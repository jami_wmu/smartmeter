import sys
from math import exp, log
from random import random, randint,sample
import matplotlib.pyplot as plt
import numpy
import pandas as pd
from matplotlib.dates import drange
from scipy import stats
import os as o

from src.utility import constant
from src.utility.common_functions import read_data


class HistoricDataProcessor:
    def __init__(self):
        # self.tainingAttackWindow = {}
        # self.historicRatioMeanDev = {}
        # self.historicalSafeMarginAndRatioFrame = {}
        # self.trainingDataSet = {}  # dictionary to keep the training set data
        # self.testingDataSet = {}  # dictionary to keep the testing set data
        # self.uniqueCompromisedMetersTraining = None  # compromised training meters
        # self.uniqueCompromisedMetersTesting = None  # compromised testing meters
        # self.additiveChangedData = {}
        # self.ratioDistributionArray = {}
        self.historicalSafeMargin = {}
        self.historicalRatio = {}
        self.historicalRatioAfterAttack = {}

    def process(self, trainingDataYearly):
        file_data_copied = {}
        for key in trainingDataYearly.keys():
            file_data_copied[key] = trainingDataYearly[key].copy()
        trainingDataAfterBoxCox = self.box_cox_transformation(trainingDataYearly)
        # print("Temp: ", temp)
        self.calculateAMAndHMTrainingData(trainingDataAfterBoxCox)
        self.calculateSignleRatioDistribution()
        additiveChangedData = self.additiveChangeOnHistoricDataOnSomeRandomMeters(file_data_copied)
        afterAttackBoxCox = self.box_cox_transformation(additiveChangedData)
        self.calculateAMAndHMTrainingUnderAttack(afterAttackBoxCox)
        self.calculateSignleRatioDistributionAfterAttack()
        plt.show()

    def additiveChangeOnHistoricDataOnSomeRandomMeters(self, trainingDataAfterBoxCox):
        additiveAttackOnTraining ={}
        # print(trainingDataAfterBoxCox)
        for key in trainingDataAfterBoxCox.keys():
            additiveAttackOnTraining[key] = trainingDataAfterBoxCox[key].copy()
        additiveChangedData = additiveAttackOnTraining['2014']
        allMeters = additiveChangedData.dataid.unique()
        print("Number of meteres",len(allMeters))
        numberofitems = (len(allMeters) * constant.RO_MAL)
        print("number of items to sample",numberofitems)
        compromisedMeters = sample(allMeters.tolist(), int(numberofitems))
        print("Number of compromised meteres",len(compromisedMeters))

        # print(uniqueCompromisedMetersTraining)
        # dataIdmask = additiveChangedData['dataid'].isin(uniqueCompromisedMetersTraining)
        # dataIdmaskFrames = additiveChangedData[dataIdmask]
        mask = ((additiveChangedData.localminute >= '2014-01-01') & (additiveChangedData.localminute <= '2014-06-01')&
                additiveChangedData['dataid'].isin(compromisedMeters))
        additiveChangedData.loc[mask, 'use'] = randint(0, 200)
        return additiveAttackOnTraining

    def calculateAMAndHMTrainingData(self, data):#the below code contains the slicing of the data by month day and hour wise
        # i have calculated ratio means for each of the time frame or window like hour,day,monthly but havent stored it yer
        # print(" Started Calculating the arithmatic and harmonic mean for hourly data")
        for key in data.keys():
            dateRatioArray = []
            yearlyData = data[key] #yearly data
            monthwisegroupData = yearlyData.groupby(pd.Grouper(key='localminute', freq='1M')) #monthwise grouping the data
            for key, monthlydata in monthwisegroupData: # iterate each month data
                if len(monthlydata)>0: #check if there is data for the month
                    daywisegroupData = monthlydata.groupby(pd.Grouper(key='localminute', freq='1D')) #group the montly data by day
                    for key, dailyData in daywisegroupData: #iterate each day of the month
                        if len(daywisegroupData)>0: #check if the day wise data count is greater than zero
                            hourwiseGroupData = dailyData.groupby(pd.Grouper(key='localminute', freq='1H')) # group the daily data into hourlwise
                            hourwiseSumAM = 0
                            hourwiseSumHM = 0
                            for key, hourlyData in hourwiseGroupData: #iterate each hourly data of the day
                                if len(hourlyData)>0: #check if the hour has data count greater than zero
                                    am_sum = 0
                                    hm_sum = 0
                                    for row in hourlyData.itertuples():
                                        if(getattr(row, "use")>0):
                                            am_sum += getattr(row, "use") #calculate arithmatic sum
                                            hm_sum += 1 / getattr(row, "use") #calculate harmonic sum
                                    N = len(hourlyData.dataid.unique())
                                    # print("Unique Meters",N)
                                    hourlyAttackHM = N / hm_sum #harmonic mean hourly
                                    hourlyAttackAM = am_sum / N#arithmatic mean hourly
                                    # print("AMsum: ", hourlyAttackAM, " HMSum: ", hourlyAttackHM)
                                    hourwiseSumAM +=hourlyAttackAM
                                    hourwiseSumHM +=hourlyAttackHM
                            year = int(key.year)
                            month = key.month
                            day = key.day
                            dateString = str(year)+"-"+str(month)+"-"+str(day)
                            daywiseRatioMean = hourwiseSumHM / hourwiseSumAM # use the ratio means of 24 hours of a day to calculate day wise ratio means
                            # print("daywiseRatioMean: ",daywiseRatioMean)
                            day = (month-1) * 30 + day
                            temp = {'year': year, 'month':month,'day':day, 'ratio'+str(key.year): daywiseRatioMean}
                            # print(temp)
                            dateRatioArray.append(temp)
            dateRatioFrame = pd.DataFrame(dateRatioArray)
            # print(dateRatioFrame.head(2))
            self.historicalRatio[key.year] = dateRatioFrame.copy()
            # print(dateRatioFrame.head(2))
            filename = key.strftime("%Y")
            # f = open(filename, "a")
            # f.write(dateRatioFrame)
            # dateRatioFrame.to_csv(r'../../resource/'+filename, index=None, header=True)
            plot = dateRatioFrame.plot(kind="line", x="day", y="ratio"+str(key.year), color="blue",
                                       xlim=(constant.START, constant.END),
                                       ylim =(constant.BOTTOM, constant.TOP),
                                       title =str(key.year)+ ' ratio plot vs day')

    def calculateAMAndHMTrainingUnderAttack(self, data):#the below code contains the slicing of the data by month day and hour wise
        # i have calculated ratio means for each of the time frame or window like hour,day,monthly but havent stored it yer
        print(" Started Calculating the arithmatic and harmonic mean for hourly data")
        for key in data.keys():
            dateRatioArray = []
            yearlyData = data[key] #yearly data
            monthwisegroupData = yearlyData.groupby(pd.Grouper(key='localminute', freq='1M')) #monthwise grouping the data
            for key, monthlydata in monthwisegroupData: # iterate each month data
                if len(monthlydata)>0: #check if there is data for the month
                    daywisegroupData = monthlydata.groupby(pd.Grouper(key='localminute', freq='1D')) #group the montly data by day
                    for key, dailyData in daywisegroupData: #iterate each day of the month
                        if len(daywisegroupData)>0: #check if the day wise data count is greater than zero
                            hourwiseGroupData = dailyData.groupby(pd.Grouper(key='localminute', freq='1H')) # group the daily data into hourlwise
                            hourwiseSumAM = 0
                            hourwiseSumHM = 0
                            for key, hourlyData in hourwiseGroupData: #iterate each hourly data of the day
                                if len(hourlyData)>0: #check if the hour has data count greater than zero
                                    am_sum = 0
                                    hm_sum = 0
                                    for row in hourlyData.itertuples():
                                        if(getattr(row, "use")>0):
                                            am_sum += getattr(row, "use") #calculate arithmatic sum
                                            hm_sum += 1 / getattr(row, "use") #calculate harmonic sum
                                    N = len(hourlyData.dataid.unique())
                                    # print("Unique Meters",N)
                                    hourlyAttackHM = N / hm_sum #harmonic mean hourly
                                    hourlyAttackAM = am_sum / N#arithmatic mean hourly
                                    # print("AMsum: ", hourlyAttackAM, " HMSum: ", hourlyAttackHM)
                                    hourwiseSumAM +=hourlyAttackAM
                                    hourwiseSumHM +=hourlyAttackHM
                            year = int(key.year)
                            month = key.month
                            day = key.day
                            dateString = str(year)+"-"+str(month)+"-"+str(day)
                            daywiseRatioMean = hourwiseSumHM / hourwiseSumAM # use the ratio means of 24 hours of a day to calculate day wise ratio means
                            day = (month-1) * 30 + day
                            temp = {'year': year, 'month':month,'day':day, 'ratio'+str(key.year): daywiseRatioMean}
                            dateRatioArray.append(temp)
            dateRatioFrame = pd.DataFrame(dateRatioArray)
            self.historicalRatioAfterAttack[key.year] = dateRatioFrame.copy()
            plot = dateRatioFrame.plot(kind="line", x="day", y="ratio"+str(key.year), color="blue",
                                       xlim=(constant.START, constant.END),
                                       ylim =(constant.BOTTOM, constant.TOP),
                                       title =str(key.year)+ ' ratio plot vs day')
    def box_cox_transformation(self,yearlyOrgData):
        for key in yearlyOrgData.keys():
            # self.historicTrainingSet[key].loc[self.historicTrainingSet[key].isin(self.uniqueCompromisedMetersTraining),'use'] +=random.uniform(0, 1)
            yearlyOrgData[key].use = stats.boxcox(yearlyOrgData[key].use, lmbda=constant.LAMBDA, alpha= None)
            # temp = self.historicTrainingSet[key].loc[self.historicTrainingSet[key]['use'] <= 0]
            # print("Negetive data: ", temp)
        return yearlyOrgData

    def calculateSafeMargin(self, dateRatioFrame,med_abs_dev):
        # calculate the safe margin out of the dataframe here
        # print("dateRatio: ",dateRatioFrame)
        safeMarginThresholdHigh = []
        safeMarginThresholdlow = []
        thresholdParam = .7*med_abs_dev
        # print(len(dateRatioFrame))
        for index, row in dateRatioFrame.iterrows():
            # print(row['date'], row['ratio'])
            temp1 = {'month':row.month,'day': row.day, 'marginHigh': row.combineRatio+thresholdParam}
            temp2 = {'month':row.month,'day': row.day,  'marginLow': row.combineRatio-thresholdParam}
            safeMarginThresholdHigh.append(temp1)
            safeMarginThresholdlow.append(temp2)

        safeMarginHighFrame = pd.DataFrame(safeMarginThresholdHigh)
        safeMarginLowFrame = pd.DataFrame(safeMarginThresholdlow)
        DF = pd.merge(safeMarginHighFrame, safeMarginLowFrame, on=['day','month'])
        # DF.plot(x="day", y=["marginHigh", "marginLow"], kind="line" ,xlim=(Constant.yearStartDay,Constant.yearEndDay),ylim=(Constant.ratioLimitLow,Constant.ratioLimitHihg))
        # print(DF)
        # plt.show()
        return DF

    def calculateNablaT(self, safeMarginAndRatioFrame):
        nablaT2014 = []
        nablaT2015 = []
        for index, row in safeMarginAndRatioFrame.iterrows():
            # print(row['date'], row['ratio'])
            if (row.ratio2014>row.marginHigh):
                temp3 = {'month':row.month,'day': row.day,  'nabla2014': row.ratio2014 -row.marginHigh}
                nablaT2014.append(temp3)
            if (row.ratio2015 > row.marginHigh):
                temp4 = {'month':row.month,'day': row.day, 'nabla2015': row.ratio2015 - row.marginHigh}
                nablaT2015.append(temp4)
            if (row.ratio2014<row.marginLow):
                temp5 = {'month':row.month,'day': row.day,  'nabla2014': row.ratio2014 - row.marginLow}
                nablaT2014.append(temp5)
            if (row.ratio2015<row.marginLow):
                temp6 = {'month':row.month,'day': row.day,  'nabla2015': row.ratio2015 - row.marginLow}
                nablaT2015.append(temp6)
            if (row.ratio2014 <= row.marginHigh and row.ratio2014 >= row.marginLow):
                temp1 = {'month':row.month,'day': row.day, 'nabla2014': 0}
                nablaT2014.append(temp1)
            if (row.ratio2015 <= row.marginHigh and row.ratio2015 >= row.marginLow):
                temp2 = {'month':row.month,'day': row.day, 'nabla2015': 0}
                nablaT2015.append(temp2)
        nablaTFrame2014 = pd.DataFrame(nablaT2014)
        nablaTFrame2015 = pd.DataFrame(nablaT2015)
        # print(nablaTFrame2014.head(2))
        # print(nablaTFrame2015.head(2))
        combineNablaT = pd.merge(nablaTFrame2014,nablaTFrame2015,on=['day','month'])
        # print(combineNablaT.head(2))
        return combineNablaT


    def calculateRUCFromNablaT(self, nablaTFrame):#fix the dataframe structure
        # print(nablaTFrame)
        rucArray = [] #we have fs days = 7 means see 7 days ahead
        for index, row in nablaTFrame.iterrows():
            if(row.day<=constant.SLIDING_FRAME):
                temp = {'month':row.month,'day': row.day,  'ruc2014':row.nabla2014, 'ruc2015':row.nabla2015}
                rucArray.append(temp)
                continue
            tempFSFrame = nablaTFrame[(nablaTFrame.day >= (row.day - constant.SLIDING_FRAME + 1)) & (nablaTFrame.day <= row.day)]
            # print("Lenght of a frame: ",len(tempFSFrame))
            #here the number of frames should be 7 I am getting 1364  and 1024
            nablaSum2014 = 0
            nablaSum2015 = 0
            for index, row1 in tempFSFrame.iterrows():
                nablaSum2014+=row1.nabla2014
                nablaSum2015+=row1.nabla2015
            temp1 = {'month':row.month,'day': row.day,  'ruc2014':nablaSum2014 , 'ruc2015':nablaSum2015}
            rucArray.append(temp1)
        rucFrame = pd.DataFrame(rucArray)
        return rucFrame

    def calculateSignleRatioDistribution(self):
        combinedRatioFrame = pd.merge(self.historicalRatio[2014], self.historicalRatio[2015], on=['day','month'])
        combinedRatioFrame['combineRatio'] = (combinedRatioFrame['ratio2014'] + combinedRatioFrame['ratio2015'])/2
        # print(combinedRatioFrame)
        standardDeviation = combinedRatioFrame.loc[:,"combineRatio"].std()  # standard deviation of the original data
        mean = combinedRatioFrame.loc[:, "combineRatio"].mean()  # mean of the original data
        med_abs_dev = stats.median_absolute_deviation(combinedRatioFrame.loc[:, "combineRatio"])
        print("mean: ", mean, " std: ", standardDeviation, " med_abs_dev: ", med_abs_dev)
        self.historicalSafeMargin = self.calculateSafeMargin(combinedRatioFrame,standardDeviation)
        ratioFrameWithSafeMargin = pd.merge(combinedRatioFrame,self.historicalSafeMargin,on =['day','month'])
        # # print("ratioFrameWithSafeMargin: ",ratioFrameWithSafeMargin.head(2))
        combineNablaT = self.calculateNablaT(ratioFrameWithSafeMargin)
        rucFrame = self.calculateRUCFromNablaT(combineNablaT)
        rucFrame.plot(x="day", y=["ruc2014", "ruc2015"], kind="line",
                      xlim=(constant.START, constant.END),
                      ylim=(-1.00,1.00))
        # mean2014 = rucFrame.loc[:, "ruc2014"].mean()
        # mean2015 = rucFrame.loc[:, "ruc2015"].mean()
        # sd2014 = rucFrame.loc[:,"ruc2014"].std()  # standard deviation of the original data
        # sd2014 = rucFrame.loc[:,"ruc2014"].std()  # standard deviation of the original data
        # sd2015 = rucFrame.loc[:,"ruc2015"].std()  # standard deviation of the original data


        # print("ruc_mean2014: ",mean2014," ruc_mean2015: ",mean2015," sd_ruc2014: ",sd2014," sd_ruc2015: ",sd2015)
        # tMax = self.calculateTmax(rucFrame)
        # tmin = self.calculateTmin(rucFrame)
        # print("Max: ",tMax," Min: ",tmin)

    def calculateTmax(self,rucFrame):
        costSum = 0
        pSum = 0
        maxSum = sys.float_info.min
        maxThreshold = sys.float_info.min
        # canTMax14 = rucFrame.loc[:,"ruc2014"].max()
        # canTMax15 = rucFrame.loc[:, "ruc2015"].max()
        # canTMin14 = rucFrame.loc[:, "ruc2014"].min()
        # canTMin15 = rucFrame.loc[:, "ruc2015"].min()
        #
        # print("canTMax14: ",canTMax14," canTMax15: ",canTMax15)
        # print("canTMin14: ",canTMin14," canTMin15: ",canTMin15)

        for taoThreshold in numpy.arange(0.0000, 0.1000, 0.0025):
            for row in rucFrame.itertuples():
                if( getattr(row, "mruc2014")>=0):
                    if( getattr(row, "mruc2014") < taoThreshold):
                        costSum +=abs(taoThreshold -  getattr(row, "mruc2014"))/2
                    else:
                        pSum += abs(taoThreshold -  getattr(row, "mruc2014"))*2

                if(getattr(row, "mruc2015") >= 0):
                    if (getattr(row, "mruc2015") < taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "mruc2015"))/2
                    else:
                        pSum += abs(taoThreshold - getattr(row, "mruc2015")) * 2

            taoSumDiff = abs(costSum - pSum)
            if(maxSum>taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold = taoThreshold
        return maxThreshold

    def calculateTmin(self, rucFrame):
        costSum = 0
        pSum = 0
        minSum = sys.float_info.max
        minThreshold = sys.float_info.max
        for taoThreshold in numpy.arange(-1.000,-0.0025,  0.0025):
            for row in rucFrame.itertuples():
                if (getattr(row, "mruc2014") < 0):
                    if (getattr(row, "mruc2014") > taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "mruc2014")) / 2
                    else:
                        pSum += abs(taoThreshold - getattr(row, "mruc2014")) * 2

                if (getattr(row, "mruc2015") < 0):
                    if (getattr(row, "mruc2015") > taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "mruc2015")) / 2
                    else:
                        pSum += abs(taoThreshold - getattr(row, "mruc2015")) * 2

            taoSumDiff = abs(costSum - pSum)
            if (minSum > taoSumDiff):
                minSum = taoSumDiff
                minThreshold = taoThreshold

        return minThreshold

    def calculateSignleRatioDistributionAfterAttack(self):
        combinedRatioFrame = pd.merge(self.historicalRatioAfterAttack[2014], self.historicalRatioAfterAttack[2015], on=['day', 'month'])
        combinedRatioFrame['combineRatio'] = (combinedRatioFrame['ratio2014'] + combinedRatioFrame['ratio2015']) / 2
        print(combinedRatioFrame)
        standardDeviation = combinedRatioFrame.loc[:, "combineRatio"].std()  # standard deviation of the original data
        mean = combinedRatioFrame.loc[:, "combineRatio"].mean()  # mean of the original data
        med_abs_dev = stats.median_absolute_deviation(combinedRatioFrame.loc[:, "combineRatio"])
        print("mean: ", mean, " std: ", standardDeviation, " med_abs_dev: ", med_abs_dev)
        ratioFrameWithSafeMargin = pd.merge(combinedRatioFrame, self.historicalSafeMargin, on=['day', 'month'])
        # # print("ratioFrameWithSafeMargin: ",ratioFrameWithSafeMargin.head(2))
        combineNablaT = self.calculateNablaT(ratioFrameWithSafeMargin)
        rucFrame = self.calculateRUCFromNablaT(combineNablaT)
        rucFrame.plot(x="day", y=["ruc2014", "ruc2015"], kind="line",
                      ylim=(-1.00, 1.00))

        mrucFrame = self.inversePowerLaw(combineNablaT)
        weightedRuc = self.weightForRuc(mrucFrame)
        modifiedRuc = self.newModifiedRUC(mrucFrame,weightedRuc)
        modifiedRuc.plot(x="day", y=["mruc2014", "mruc2015"], kind="line",
                         xlim=(constant.START, constant.END),
                         ylim=(-1.00, 1.00))
        # mean2014 = rucFrame.loc[:, "ruc2014"].mean()
        # mean2015 = rucFrame.loc[:, "ruc2015"].mean()
        # sd2014 = rucFrame.loc[:, "ruc2014"].std()  # standard deviation of the original data
        # sd2015 = rucFrame.loc[:, "ruc2015"].std()  # standard deviation of the original data

        # print("ruc_mean2014: ", mean2014, " ruc_mean2015: ", mean2015, " sd_ruc2014: ", sd2014, " sd_ruc2015: ", sd2015)
        tMax = self.calculateTmax(modifiedRuc)
        tmin = self.calculateTmin(modifiedRuc)
        print("Max: ", tMax, " Min: ", tmin)

    #calculate ruc by using inverse power law
    def inversePowerLaw(self,nablaTFrame):
        # print(nablaTFrame)
        rucArray = []  # we have fs days = 7 means see 7 days ahead
        for index, row in nablaTFrame.iterrows():
            if (row.day <= constant.SLIDING_FRAME):
                temp = {'month': row.month, 'day': row.day, 'ruc2014': row.nabla2014, 'ruc2015': row.nabla2015}
                rucArray.append(temp)
                continue
            tempFSFrame = nablaTFrame[
                (nablaTFrame.day >= (row.day - constant.SLIDING_FRAME + 1)) & (nablaTFrame.day <= row.day)]
            # print("Lenght of a frame: ",len(tempFSFrame))
            # here the number of frames should be 7 I am getting 1364  and 1024
            nablaSum2014 = 0
            nablaSum2015 = 0
            for index, row1 in tempFSFrame.iterrows():
                # row1.nabla2014*(1/pow(Constant.SLIDING_FRAME,Constant.itaParam))*pow((row1.day -(row.day - Constant.SLIDING_FRAME)),Constant.itaParam)
                # nablaSum2014 += row1.nabla2014
                nablaSum2014 += row1.nabla2014 * (1 / pow(constant.SLIDING_FRAME, constant.ITA)) * \
                                pow((row1.day - (row.day - constant.SLIDING_FRAME)), constant.ITA)
                nablaSum2015 += row1.nabla2015 * (1 / pow(constant.SLIDING_FRAME, constant.ITA)) * \
                                pow((row1.day - (row.day - constant.SLIDING_FRAME)), constant.ITA)
            temp1 = {'month': row.month, 'day': row.day, 'ruc2014': nablaSum2014, 'ruc2015': nablaSum2015}
            rucArray.append(temp1)
        rucFrame = pd.DataFrame(rucArray)
        return rucFrame

    #this is the weight for the ruc calculated by inverse power law
    def weightForRuc(self,rucByIPL):
        weightRucs= []
        for index, row in rucByIPL.iterrows():
            # print("row.ruc2014: ",row.ruc2014)
            # logRuc2014 = log(abs(row.ruc2014)) *(-1)
            #here now we are calculating for one alpha later implementation might need for all alpha
            if(row.ruc2014 == 0):
                expon2014 = 0
            else:
                expon2014 = ((-1) * constant.BETA) * pow(((-1) * log(abs(row.ruc2014))), constant.ALPHA)
            # print("exponent:",expon2014)
            w2014 = exp(expon2014)
            if(row.ruc2015 == 0):
                expon2015 = 0
            else:
                expon2015 = ((-1) * constant.BETA) * pow(((-1) * log(abs(row.ruc2015))), constant.ALPHA)

            w2015 = exp(expon2015)
            temp1 = {'month': row.month, 'day': row.day, 'w2014': w2014, 'w2015': w2015}
            weightRucs.append(temp1)
        wFrame = pd.DataFrame(weightRucs)
        return wFrame

    #below calculate modified robust ruc by the weights and ruc by inverse power law
    def newModifiedRUC (self,ruc,weightRuc):
        # print(nablaTFrame)
        weightAndRuc = pd.merge(ruc,weightRuc, on=['month','day'])
        rucArray = []  # we have fs days = 7 means see 7 days ahead
        for index, row in weightAndRuc.iterrows():
            if (row.day <= constant.SLIDING_FRAME):
                temp = {'month': row.month, 'day': row.day,
                        'ruc2014': row.ruc2014, 'ruc2015': row.ruc2015,
                        'w2014': row.w2014, 'w2015': row.w2015}
                rucArray.append(temp)
                continue
            tempFSFrame = weightAndRuc[
                (weightAndRuc.day >= (row.day - constant.SLIDING_FRAME + 1)) & (weightAndRuc.day <= row.day)]
            # print("Lenght of a frame: ",len(tempFSFrame))
            # here the number of frames should be 7 I am getting 1364  and 1024
            modifiedRuc2014 = 0
            modifiedRuc2015 = 0
            for index, row1 in tempFSFrame.iterrows():
                modifiedRuc2014 += row1.ruc2014 * row1.w2014
                modifiedRuc2015 += row1.ruc2015 * row1.w2015
            temp1 = {'month': row.month, 'day': row.day,
                     'mruc2014': modifiedRuc2014, 'mruc2015': modifiedRuc2015,
                     }
            rucArray.append(temp1)
        rucFrame = pd.DataFrame(rucArray)
        return rucFrame
    def readData(self):
        # region read
        file_data = {}
        for key in constant.TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = constant.TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader fun
        return file_data
if __name__ == "__main__":
    historicProcess = HistoricDataProcessor()  # create data reader object
    trainingData = historicProcess.readData()
    historicProcess.process(trainingData)
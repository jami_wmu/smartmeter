import pandas as pd
import matplotlib.pyplot as plt
import numpy

SMALL_SIZE = 8
MEDIUM_SIZE = 14
BIGGER_SIZE = 30

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)

LR_l2 = pd.read_csv("l2_LR_Loss_t_66.csv")
QR_l2 = pd.read_csv("l2_QR_Loss_t_16.csv")
LR_l1 = pd.read_csv("l1_LR_Loss_t_73.csv")
QR_l1 = pd.read_csv("l1_QR_Loss_t_33.csv")
x = numpy.arange(len(LR_l2))

plt.plot(x, QR_l1['loss'], color='r', label='QR l1', marker ='X',markevery = 10) # r - red colour
plt.plot(x, QR_l2['loss'], color='b', label='QR l2',linestyle='-.' ,marker ='D',markevery = 10) # r - red colour
plt.axvline(x=33, color='g',label='QR l1 FGAV', linestyle='--')
plt.axvline(x=16, color='b', label='QR l2 FGAV',linestyle='-.')
plt.xlabel("Number of RUC")
plt.ylabel("Loss")

plt.legend()
plt.show()
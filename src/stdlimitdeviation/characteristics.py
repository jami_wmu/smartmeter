import json
import math
import matplotlib.pyplot as plt
import pandas as pd
from src.utility.constant import *
from mpl_toolkits.mplot3d import Axes3D

with open("result_2014_l_a_a_m_4_6_new.txt") as source:
    json_source = source.read()
    result_2014_large_a_attack_with_mon_4_6_List = json.loads('[{}]'.format(json_source))
result_2014_large_a_attack_with_mon_4_6_frame = pd.json_normalize(result_2014_large_a_attack_with_mon_4_6_List)

with open("result_2014_s_a_a_m_4_6.txt") as source:
    json_source = source.read()
    result_2014_small_a_attack_with_mon_4_6_List = json.loads('[{}]'.format(json_source))
result_2014_small_a_attack_with_mon_4_6_frame = pd.json_normalize(result_2014_small_a_attack_with_mon_4_6_List)

distance_kappa_2sr_2014_4_to_6_l_a_a = []
for row1 in result_2014_large_a_attack_with_mon_4_6_frame.itertuples():
    lower_distance = abs(-0.017499999999999998 - row1.lower_limit)
    lower_distance_log = 0
    if(lower_distance !=0):
        lower_distance_log = (-1)*math.log(lower_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'lower_distance': lower_distance,
                'lower_distance_log':lower_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': LARGE_ADDITIVE}
        distance_kappa_2sr_2014_4_to_6_l_a_a.append(temp)
distance_kappa_2sr_2014_4_to_6_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_l_a_a)
resultant_frame_4_6_l_a_a = distance_kappa_2sr_2014_4_to_6_l_a_a_frame[distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance ==
                                           distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance.max()]
print("resultant_frame_4_6_l_a_a")
print(resultant_frame_4_6_l_a_a[['lower_distance','ro_mal','del_avg']])
distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.loc[
    distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance_log!=0]
fig = plt.figure()
ax = fig.gca(projection="3d")
z =  distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.lower_distance_log
x = distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.del_avg
y = distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.ro_mal
ax.plot_trisurf(x, y, z, label="LAA M 4-6",edgecolors='grey')
ax.set_xlabel('del_avg')
ax.set_ylabel('ro_mal')
ax.set_zlabel('Lower distance in log scale')
# ax.legend()

fig_org = plt.figure()
ax_org = fig_org.gca(projection="3d")
z_org =  distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance
x_org = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.del_avg
y_org = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.ro_mal
ax_org.plot_trisurf(x_org, y_org, z_org, label="LAA M 4-6",edgecolors='grey')
ax_org.set_xlabel('del_avg')
ax_org.set_ylabel('ro_mal')
ax_org.set_zlabel('Lower distance')
# ax.legend()

plt.show()

# gph = plt.figure(figsize=(15,8)).gca(projection='3d')
# #plotting red dotted lines with tiny markers
# gph.plot(distance_kappa_2sr_2014_4_to_6_l_a_a_frame.del_avg,
#          distance_kappa_2sr_2014_4_to_6_l_a_a_frame.ro_mal,
#          distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance_log,"r.--")
# #and on top of it goes a scatter plot with different markersizes
# gph.scatter(distance_kappa_2sr_2014_4_to_6_l_a_a_frame.del_avg,
#          distance_kappa_2sr_2014_4_to_6_l_a_a_frame.ro_mal,
#          distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance_log,
#          color = "r",  alpha = 1)
# gph.set_xlabel('del_avg')
# gph.set_ylabel('ro_mal')
# gph.set_zlabel('lower distance')
# plt.show()
# fig_impact = distance_kappa_2sr_2014_4_to_6_d_a_frame.plot(x="del_avg", y="impact_of_undetected",kind="line", label="M 4-6 D&CA")

distance_kappa_2sr_2014_4_to_6_s_a_a = []
for row1 in result_2014_small_a_attack_with_mon_4_6_frame.itertuples():
    upper_distance = abs(0.0075 - row1.upper_limit)
    upper_distance_log = 0
    if (upper_distance != 0):
        upper_distance_log = (-1)*math.log(upper_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                'upper_distance_log':upper_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': LARGE_ADDITIVE}
        distance_kappa_2sr_2014_4_to_6_s_a_a.append(temp)
distance_kappa_2sr_2014_4_to_6_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_s_a_a)
resultant_frame_4_6_s_a_a = distance_kappa_2sr_2014_4_to_6_s_a_a_frame[distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance ==
                                           distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance.max()]

distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.loc[
    distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance_log!=0]
print("resultant_frame_4_6_s_a_a")
print(resultant_frame_4_6_s_a_a[['upper_distance','ro_mal','del_avg']])
fig1 = plt.figure()
ax1 = fig1.gca(projection="3d")
z1 =  distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.upper_distance_log
x1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.del_avg
y1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.ro_mal
ax1.plot_trisurf(x1, y1, z1, label="SAA M 4-6",edgecolors='grey')
ax1.set_xlabel('del_avg')
ax1.set_ylabel('ro_mal')
ax1.set_zlabel('Upper distance in log scale')
# ax1.legend()

fig_org1 = plt.figure()
ax_org1 = fig_org1.gca(projection="3d")
z_org1 =  distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance
x_org1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.del_avg
y_org1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.ro_mal
ax_org1.plot_trisurf(x_org1, y_org1, z_org1, label="SAA M 4-6",edgecolors='grey')
ax_org1.set_xlabel('del_avg')
ax_org1.set_ylabel('ro_mal')
ax_org1.set_zlabel('Upper distance')
# ax_org1.legend()

plt.show()
# gph1 = plt.figure(figsize=(15,8)).gca(projection='3d')
# #plotting red dotted lines with tiny markers
#
# gph1.plot(distance_kappa_2sr_2014_4_to_6_s_a_a_frame.del_avg,
#          distance_kappa_2sr_2014_4_to_6_s_a_a_frame.ro_mal,
#          distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance_log,"r.--",label="SAA M 4-6")
# #and on top of it goes a scatter plot with different markersizes
# gph1.scatter(distance_kappa_2sr_2014_4_to_6_s_a_a_frame.del_avg,
#          distance_kappa_2sr_2014_4_to_6_s_a_a_frame.ro_mal,
#          distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance_log,
#          color = "r",  alpha = 1)
# gph1.set_xlabel('del_avg')
# gph1.set_ylabel('ro_mal')
# gph1.set_zlabel('Upper distance in log scale')
# plt.show()
df_output_laa = distance_kappa_2sr_2014_4_to_6_l_a_a_frame [['lower_distance','ro_mal','del_avg']]
df_output_saa = distance_kappa_2sr_2014_4_to_6_s_a_a_frame[['upper_distance','ro_mal','del_avg']]
# distance_array_frame.to_csv(r'output_large_deductive.csv')
df_output_laa.to_csv(r'output_laa.csv')
df_output_saa.to_csv(r'output_saa.csv')
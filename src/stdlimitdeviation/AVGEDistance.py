import json
import math
import matplotlib.pyplot as plt
import pandas as pd
from src.utility.constant import *
from mpl_toolkits.mplot3d import Axes3D
#region SAA
#region ReadFiles SAA
with open("result_2014_s_a_a_m_7_9_iterations_5.txt") as source:
    json_source = source.read()
    result_2014_s_a_attack_with_mon_7_9_iterations_5_List = json.loads('[{}]'.format(json_source))
result_2014_s_a_attack_with_mon_7_9_iterations_5_frame = pd.json_normalize(result_2014_s_a_attack_with_mon_7_9_iterations_5_List)

with open("result_2014_s_a_a_m_7_9_iterations_15.txt") as source:
    json_source = source.read()
    result_2014_s_a_a_m_7_9_iterations_15_List = json.loads('[{}]'.format(json_source))
result_2014_s_a_a_m_7_9_iterations_15_frame = pd.json_normalize(result_2014_s_a_a_m_7_9_iterations_15_List)

with open("result_2014_s_a_a_m_4_6_iterations_10.txt") as source:
    json_source = source.read()
    result_2014_s_a_attack_with_mon_4_6_iterations_10_List = json.loads('[{}]'.format(json_source))
result_2014_s_a_attack_with_mon_4_6_iterations_10_frame = pd.json_normalize(result_2014_s_a_attack_with_mon_4_6_iterations_10_List)
#endregion
#region Distance M 7-9 Iteration 5 and 15
distance_kappa_2sr_2014_7_to_9_s_a_a = []
for row1 in result_2014_s_a_attack_with_mon_7_9_iterations_5_frame.itertuples():
    upper_distance = abs(0.0075 - row1.upper_limit)
    upper_distance_log = 0
    if (upper_distance != 0):
        upper_distance_log = (-1) * math.log(upper_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                'upper_threshold': row1.upper_limit,
                'upper_distance_log':upper_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': SMALL_ADDITIVE,
                'iteration':row1.iteration}
        distance_kappa_2sr_2014_7_to_9_s_a_a.append(temp)

for row1 in result_2014_s_a_a_m_7_9_iterations_15_frame.itertuples():
    upper_distance = abs(0.0075 - row1.upper_limit)
    upper_distance_log = 0
    if (upper_distance != 0):
        upper_distance_log = (-1) * math.log(upper_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                'upper_threshold': row1.upper_limit,
                'upper_distance_log':upper_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': SMALL_ADDITIVE,
                'iteration':row1.iteration}
        distance_kappa_2sr_2014_7_to_9_s_a_a.append(temp)
#endregion
#region MAX U Euclidian and Plot M 7-9
distance_kappa_2sr_2014_7_to_9_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_s_a_a)
resultant_frame_7_9_s_a_a = distance_kappa_2sr_2014_7_to_9_s_a_a_frame[distance_kappa_2sr_2014_7_to_9_s_a_a_frame.upper_distance ==
                                           distance_kappa_2sr_2014_7_to_9_s_a_a_frame.upper_distance.max()]
print("resultant_frame_7_9_s_a_a")
print(resultant_frame_7_9_s_a_a[['upper_distance','ro_mal','del_avg']])
distance_kappa_2sr_2014_7_to_9_s_a_a_frame_no_zero = distance_kappa_2sr_2014_7_to_9_s_a_a_frame.loc[
    distance_kappa_2sr_2014_7_to_9_s_a_a_frame.upper_distance_log!=0]

grouped_df = distance_kappa_2sr_2014_7_to_9_s_a_a_frame.groupby("iteration")
print("Grouped Max for Month 7-9 SAA")
for key,group in grouped_df:
    # print(group)
    maximums = group[group.upper_distance ==  group.upper_distance.max()]
    print(maximums[['upper_distance','ro_mal','del_avg','iteration']])

fig = plt.figure()
ax = fig.gca(projection="3d")
z =  distance_kappa_2sr_2014_7_to_9_s_a_a_frame_no_zero.upper_distance_log
x = distance_kappa_2sr_2014_7_to_9_s_a_a_frame_no_zero.del_avg
y = distance_kappa_2sr_2014_7_to_9_s_a_a_frame_no_zero.ro_mal
ax.plot_trisurf(x, y, z, label="SAA M 7-9",edgecolors='grey')
ax.set_xlabel('del_avg')
ax.set_ylabel('ro_mal')
ax.set_zlabel('Upper distance in log scale M 7-9')
# ax.legend()

fig_org = plt.figure()
ax_org = fig_org.gca(projection="3d")
z_org =  distance_kappa_2sr_2014_7_to_9_s_a_a_frame.upper_distance
x_org = distance_kappa_2sr_2014_7_to_9_s_a_a_frame.del_avg
y_org = distance_kappa_2sr_2014_7_to_9_s_a_a_frame.ro_mal
ax_org.plot_trisurf(x_org, y_org, z_org, label="SAA M 7-9",edgecolors='grey')
ax_org.set_xlabel('del_avg')
ax_org.set_ylabel('ro_mal')
ax_org.set_zlabel('Upper distance M 7-9')
# ax.legend()
plt.show()
#endregion 4-6
#region Distance M 4-6
distance_kappa_2sr_2014_4_to_6_s_a_a = []
for row1 in result_2014_s_a_attack_with_mon_4_6_iterations_10_frame.itertuples():
    upper_distance = abs(0.0075 - row1.upper_limit)
    upper_distance_log = 0
    if (upper_distance != 0):
        upper_distance_log = (-1)*math.log(upper_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                'upper_threshold': row1.upper_limit,
                'upper_distance_log':upper_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': SMALL_ADDITIVE,
                'iteration':row1.iteration}
        distance_kappa_2sr_2014_4_to_6_s_a_a.append(temp)
#endregion
#region Max U Euclidian and plot M 4-6
distance_kappa_2sr_2014_4_to_6_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_s_a_a)
resultant_frame_4_6_s_a_a = distance_kappa_2sr_2014_4_to_6_s_a_a_frame[distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance ==
                                           distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance.max()]

distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.loc[
    distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance_log!=0]

grouped_df = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.groupby("iteration")
print("Grouped Max for Month 4-6 SAA")
for key,group in grouped_df:
    # print(group)
    maximums = group[group.upper_distance ==  group.upper_distance.max()]
    print(maximums[['upper_distance','ro_mal','del_avg','iteration']])

print("resultant_frame_4_6_s_a_a")
print(resultant_frame_4_6_s_a_a[['upper_distance','ro_mal','del_avg']])
fig1 = plt.figure()
ax1 = fig1.gca(projection="3d")
z1 =  distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.upper_distance_log
x1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.del_avg
y1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame_no_zero.ro_mal
ax1.plot_trisurf(x1, y1, z1, label="SAA M 4-6",edgecolors='grey')
ax1.set_xlabel('del_avg')
ax1.set_ylabel('ro_mal')
ax1.set_zlabel('Upper distance in log scale M 4-6')
# ax1.legend()

fig_org1 = plt.figure()
ax_org1 = fig_org1.gca(projection="3d")
z_org1 =  distance_kappa_2sr_2014_4_to_6_s_a_a_frame.upper_distance
x_org1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.del_avg
y_org1 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame.ro_mal
ax_org1.plot_trisurf(x_org1, y_org1, z_org1, label="SAA M 4-6",edgecolors='grey')
ax_org1.set_xlabel('del_avg')
ax_org1.set_ylabel('ro_mal')
ax_org1.set_zlabel('Upper distance M 4-6')
# ax_org1.legend()
plt.show()
#endregion
#region Output Files Upper Euclidian
df_output_saa_m_7_9 = distance_kappa_2sr_2014_7_to_9_s_a_a_frame [['upper_distance','ro_mal','del_avg','iteration']]
df_output_saa_m_4_6 = distance_kappa_2sr_2014_4_to_6_s_a_a_frame[['upper_distance','ro_mal','del_avg','iteration']]
# distance_array_frame.to_csv(r'output_large_deductive.csv')
# df_output_saa_m_7_9.to_csv(r'output_saa_7_9.csv')
# df_output_saa_m_4_6.to_csv(r'output_saa_4_6.csv')
#endregion
#endregion
#region LAA
#region read LAA Files
with open("result_2014_l_a_a_m_7_9_iterations_5.txt") as source:
    json_source = source.read()
    result_2014_l_a_attack_with_mon_7_9_iterations_5_List = json.loads('[{}]'.format(json_source))
result_2014_l_a_attack_with_mon_7_9_iterations_5_frame = pd.json_normalize(result_2014_l_a_attack_with_mon_7_9_iterations_5_List)

with open("result_2014_l_a_a_m_4_6_iterations_5.txt") as source:
    json_source = source.read()
    result_2014_l_a_attack_with_mon_4_6_iterations_5_List = json.loads('[{}]'.format(json_source))
result_2014_l_a_attack_with_mon_4_6_iterations_5_frame = pd.json_normalize(result_2014_l_a_attack_with_mon_4_6_iterations_5_List)

with open("result_2014_l_a_a_ROMAL_70_80_m_4_6_iterations_10.txt") as source:
    json_source = source.read()
    result_2014_l_a_ROMAL_70_80_attack_with_mon_4_6_iterations_10_List = json.loads('[{}]'.format(json_source))
result_2014_l_a_ROMAL_70_80_attack_with_mon_4_6_iterations_10_frame = pd.json_normalize(result_2014_l_a_ROMAL_70_80_attack_with_mon_4_6_iterations_10_List)
#endregion
#region Distance M7-9
distance_kappa_2sr_2014_7_to_9_l_a_a = []
for row1 in result_2014_l_a_attack_with_mon_7_9_iterations_5_frame.itertuples():
    lower_distance = abs(-0.017499999999999998 - row1.lower_limit)
    lower_distance_log = 0
    if (lower_distance != 0):
        lower_distance_log = (-1) * math.log(lower_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'lower_distance': lower_distance,
                'lower_threshold': row1.lower_limit,
                'lower_distance_log':lower_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': LARGE_ADDITIVE,
                'iteration':row1.iteration}
        distance_kappa_2sr_2014_7_to_9_l_a_a.append(temp)

distance_kappa_2sr_2014_7_to_9_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_l_a_a)
resultant_frame_7_9_l_a_a = distance_kappa_2sr_2014_7_to_9_l_a_a_frame[distance_kappa_2sr_2014_7_to_9_l_a_a_frame.lower_distance ==
                                           distance_kappa_2sr_2014_7_to_9_l_a_a_frame.lower_distance.max()]

grouped_df = distance_kappa_2sr_2014_7_to_9_l_a_a_frame.groupby("iteration")
print("Grouped Max for Month 7-9 LAA")
for key,group in grouped_df:
    # print(group)
    maximums = group[group.lower_distance ==  group.lower_distance.max()]
    print(maximums[['lower_distance','ro_mal','del_avg','iteration']])

print("resultant_frame_7_9_l_a_a")
print(resultant_frame_7_9_l_a_a[['lower_distance','ro_mal','del_avg']])
distance_kappa_2sr_2014_7_to_9_l_a_a_frame_no_zero = distance_kappa_2sr_2014_7_to_9_l_a_a_frame.loc[
    distance_kappa_2sr_2014_7_to_9_l_a_a_frame.lower_distance_log!=0]
#endregion
#region plot M7-9
figLA = plt.figure()
axLA = figLA.gca(projection="3d")
zLA =  distance_kappa_2sr_2014_7_to_9_l_a_a_frame_no_zero.lower_distance_log
xLA = distance_kappa_2sr_2014_7_to_9_l_a_a_frame_no_zero.del_avg
yLA = distance_kappa_2sr_2014_7_to_9_l_a_a_frame_no_zero.ro_mal
axLA.plot_trisurf(xLA, yLA, zLA, label="LAA M 7-9",edgecolors='grey')
axLA.set_xlabel('del_avg')
axLA.set_ylabel('ro_mal')
axLA.set_zlabel('Lower distance in log scale M 7-9')
# ax.legend()

fig_orgLA = plt.figure()
ax_orgLA = fig_orgLA.gca(projection="3d")
z_orgLA =  distance_kappa_2sr_2014_7_to_9_l_a_a_frame.lower_distance
x_orgLA = distance_kappa_2sr_2014_7_to_9_l_a_a_frame.del_avg
y_orgLA = distance_kappa_2sr_2014_7_to_9_l_a_a_frame.ro_mal
ax_orgLA.plot_trisurf(x_orgLA, y_orgLA, z_orgLA, label="LAA M 7-9",edgecolors='grey')
ax_orgLA.set_xlabel('del_avg')
ax_orgLA.set_ylabel('ro_mal')
ax_orgLA.set_zlabel('Lower distance M 7-9')
# ax.legend()
plt.show()
#endregion
#region Distance All Romal M 4-6
frames = [result_2014_l_a_attack_with_mon_4_6_iterations_5_frame,
          result_2014_l_a_ROMAL_70_80_attack_with_mon_4_6_iterations_10_frame]
concatenated_frame = pd.concat(frames)
distance_kappa_concatenated_array= []
for row1 in concatenated_frame.itertuples():
    lower_distance = abs(-0.017499999999999998 - row1.lower_limit)
    lower_distance_log = 0
    if (lower_distance != 0):
        lower_distance_log = (-1) * math.log(lower_distance)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': 2.00, 'lower_distance': lower_distance,
                'lower_threshold':row1.lower_limit,
                'lower_distance_log':lower_distance_log,
                'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected': impact_of_undetected_attack,
                'type': LARGE_ADDITIVE,
                'iteration':row1.iteration}
        distance_kappa_concatenated_array.append(temp)
distance_kappa_concatenated_frame = pd.DataFrame(distance_kappa_concatenated_array)
resultant_distance_kappa_concatenated_frame = distance_kappa_concatenated_frame[distance_kappa_concatenated_frame.lower_distance ==
                                           distance_kappa_concatenated_frame.lower_distance.max()]

grouped_df = distance_kappa_concatenated_frame.groupby("iteration")
print("Grouped Max All ROMAL M4-6")
for key,group in grouped_df:
    # print(group)
    maximums = group[group.lower_distance ==  group.lower_distance.max()]
    print(maximums[['lower_distance','ro_mal','del_avg','iteration']])


distance_kappa_concatenated_frame_no_zero = distance_kappa_concatenated_frame.loc[
    distance_kappa_concatenated_frame.lower_distance_log!=0]
#endregion
#region plot M4-6 All ROMAL
figLA = plt.figure()
axLA = figLA.gca(projection="3d")
zLA =  distance_kappa_concatenated_frame_no_zero.lower_distance_log
xLA = distance_kappa_concatenated_frame_no_zero.del_avg
yLA = distance_kappa_concatenated_frame_no_zero.ro_mal
axLA.plot_trisurf(xLA, yLA, zLA, label="LAA M 4-6",edgecolors='grey')
axLA.set_xlabel('del_avg')
axLA.set_ylabel('ro_mal')
axLA.set_zlabel('Lower distance in log scale M 4-6')
fig_orgLA = plt.figure()
ax_orgLA = fig_orgLA.gca(projection="3d")
z_orgLA =  distance_kappa_concatenated_frame.lower_distance
x_orgLA = distance_kappa_concatenated_frame.del_avg
y_orgLA = distance_kappa_concatenated_frame.ro_mal
ax_orgLA.plot_trisurf(x_orgLA, y_orgLA, z_orgLA, label="LAA M 4-6",edgecolors='grey')
ax_orgLA.set_xlabel('del_avg')
ax_orgLA.set_ylabel('ro_mal')
ax_orgLA.set_zlabel('Lower distance M 4-6')
plt.show()
#endregion
#region Distance M4-6
# distance_kappa_2sr_2014_4_to_6_l_a_a = []
# for row1 in result_2014_l_a_attack_with_mon_4_6_iterations_5_frame.itertuples():
#     lower_distance = abs(-0.017499999999999998 - row1.lower_limit)
#     lower_distance_log = 0
#     if (lower_distance != 0):
#         lower_distance_log = (-1)*math.log(lower_distance)
#         revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
#         impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
#         temp = {'kappa': 2.00, 'lower_distance': lower_distance,
#                 'lower_distance_log':lower_distance_log,
#                 'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
#                 'impact_of_undetected': impact_of_undetected_attack,
#                 'type': LARGE_ADDITIVE,
#                 'iteration':row1.iteration}
#         distance_kappa_2sr_2014_4_to_6_l_a_a.append(temp)
#
# distance_kappa_2sr_2014_4_to_6_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_l_a_a)
# resultant_frame_4_6_l_a_a = distance_kappa_2sr_2014_4_to_6_l_a_a_frame[distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance ==
#                                            distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance.max()]
#
# grouped_df = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.groupby("iteration")
# print("Grouped Max for Month 4-6 LAA")
# for key,group in grouped_df:
#     # print(group)
#     maximums = group[group.lower_distance ==  group.lower_distance.max()]
#     print(maximums[['lower_distance','ro_mal','del_avg','iteration']])
#
# distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.loc[
#     distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance!=0]
# print("resultant_frame_4_6_l_a_a")
# print(resultant_frame_4_6_l_a_a[['lower_distance','ro_mal','del_avg']])
#endregion
#region Plot M 4-6
# figL = plt.figure()
# axL = figL.gca(projection="3d")
# zL =  distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.lower_distance_log
# xL = distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.del_avg
# yL = distance_kappa_2sr_2014_4_to_6_l_a_a_frame_no_zero.ro_mal
# axL.plot_trisurf(x1, y1, z1, label="LAA M 4-6",edgecolors='grey')
# axL.set_xlabel('del_avg')
# axL.set_ylabel('ro_mal')
# axL.set_zlabel('Lower distance in log scale M 4-6')
# fig_orgL = plt.figure()
# ax_orgL = fig_orgL.gca(projection="3d")
# z_orgL =  distance_kappa_2sr_2014_4_to_6_l_a_a_frame.lower_distance
# x_orgL = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.del_avg
# y_orgL = distance_kappa_2sr_2014_4_to_6_l_a_a_frame.ro_mal
# ax_orgL.plot_trisurf(x_orgL, y_orgL, z_orgL, label="LAA M 4-6",edgecolors='grey')
# ax_orgL.set_xlabel('del_avg')
# ax_orgL.set_ylabel('ro_mal')
# ax_orgL.set_zlabel('Lower distance M 4-6')
# plt.show()
#endregion
#region  output Lower Euclidian
df_output_laa_m_7_9 = distance_kappa_2sr_2014_7_to_9_l_a_a_frame [['lower_distance','ro_mal','del_avg','iteration']]
df_output_laa_m_4_6 = distance_kappa_concatenated_frame[['lower_distance','ro_mal','del_avg','iteration']]
# distance_array_frame.to_csv(r'output_large_deductive.csv')
# df_output_laa_m_7_9.to_csv(r'output_laa_7_9.csv')
# df_output_laa_m_4_6.to_csv(r'output_laa_4_6.csv')

frames_saa = [distance_kappa_2sr_2014_7_to_9_s_a_a_frame,
          distance_kappa_2sr_2014_4_to_6_s_a_a_frame]
concatenated_frame_saa = pd.concat(frames_saa)

resultant_distance_kappa_concatenated_frame_saa = concatenated_frame_saa[concatenated_frame_saa.upper_distance ==
                                           concatenated_frame_saa.upper_distance.max()]

print("MAX Lower Distance for all ROMALs")
print(resultant_distance_kappa_concatenated_frame[['lower_distance','lower_threshold','ro_mal','del_avg']])
print("MAX Upper Distance for all ROMALs")
print(resultant_distance_kappa_concatenated_frame_saa[['upper_distance','upper_threshold','ro_mal','del_avg']])
#endregion
#endregion

#region trimmed data
with open("2014_s_a_a_m_4_6_iter_5_filter_6000.txt") as source:
    json_source = source.read()
    result_2014_s_a_a_m_4_6_iter_5_filter_6000_List = json.loads('[{}]'.format(json_source))
result_2014_s_a_a_m_4_6_iter_5_filter_6000_frame = pd.json_normalize(result_2014_s_a_a_m_4_6_iter_5_filter_6000_List)

distance_2014_s_a_a_m_4_6_iter_5_filter_6000 = []
for row1 in result_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.itertuples():
    if(row1.upper_limit>0.0075):
        upper_distance = abs(0.0075 - row1.upper_limit)
        upper_distance_log = 0
        if (upper_distance != 0):
            upper_distance_log = (-1)*math.log(upper_distance)
            revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
            impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
            temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                    'upper_threshold': row1.upper_limit,
                    'upper_distance_log':upper_distance_log,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                    'impact_of_undetected': impact_of_undetected_attack,
                    'type': SMALL_ADDITIVE,
                    'iteration':row1.iteration}
            distance_2014_s_a_a_m_4_6_iter_5_filter_6000.append(temp)

if(len(distance_2014_s_a_a_m_4_6_iter_5_filter_6000)>0):
    distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame = pd.DataFrame(distance_2014_s_a_a_m_4_6_iter_5_filter_6000)
    resultant_frame_4_6_s_a_a = distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame[distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.upper_distance ==
                                               distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.upper_distance.max()]
    distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame_no_zero = distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.loc[
        distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.upper_distance_log!=0]
    grouped_df = distance_2014_s_a_a_m_4_6_iter_5_filter_6000_frame.groupby("iteration")
    print("Grouped Max for Month 4-6 SAA For FIltered Data ")
    for key,group in grouped_df:
        # print(group)
        maximums = group[group.upper_distance ==  group.upper_distance.max()]
        print(maximums[['upper_distance','ro_mal','upper_threshold','del_avg','iteration']])

    print("resultant_frame_4_6_s_a_a For Filtered Data ")
    print(resultant_frame_4_6_s_a_a[['upper_distance','upper_threshold','ro_mal','del_avg']])


with open("2014_l_a_a_m_4_6_iter_5_filter_6000.txt") as source:
    json_source = source.read()
    result_2014_l_a_a_m_4_6_iter_5_filter_6000_List = json.loads('[{}]'.format(json_source))
result_2014_l_a_a_m_4_6_iter_5_filter_6000_frame = pd.json_normalize(result_2014_l_a_a_m_4_6_iter_5_filter_6000_List)

distance_2014_l_a_a_m_4_6_iter_5_filter_6000 = []
for row1 in result_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.itertuples():
    if( row1.lower_limit < -0.017499999999999998 ):
        lower_distance = abs(-0.017499999999999998 - row1.lower_limit)
        lower_distance_log = 0
        if (lower_distance != 0):
            lower_distance_log = (-1)*math.log(lower_distance)
            revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
            impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
            temp = {'kappa': 2.00, 'lower_distance': lower_distance,
                    'lower_threshold': row1.lower_limit,
                    'lower_distance_log':lower_distance_log,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                    'impact_of_undetected': impact_of_undetected_attack,
                    'type': LARGE_ADDITIVE,
                    'iteration':row1.iteration}
            distance_2014_l_a_a_m_4_6_iter_5_filter_6000.append(temp)
if(len(distance_2014_l_a_a_m_4_6_iter_5_filter_6000)>0):
    distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame = pd.DataFrame(distance_2014_l_a_a_m_4_6_iter_5_filter_6000)
    resultant_frame_4_6_l_a_a = distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame[distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.lower_distance ==
                                               distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.lower_distance.max()]
    
    distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame_no_zero = distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.loc[
        distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.lower_distance_log!=0]
    
    grouped_df = distance_2014_l_a_a_m_4_6_iter_5_filter_6000_frame.groupby("iteration")
    print("Grouped Max for Month 4-6 SAA For FIltered Data ")
    for key,group in grouped_df:
        # print(group)
        maximums = group[group.lower_distance ==  group.lower_distance.max()]
        print(maximums[['lower_distance','lower_threshold','ro_mal','del_avg','iteration']])
    
    print("resultant_frame_4_6_l_a_a For Filtered Data ")
    print(resultant_frame_4_6_l_a_a[['lower_distance','lower_threshold','ro_mal','del_avg']])
#endregion
#region logarithminc transformation
print("Logarithmic transformation")
with open("2014_l_a_a_m_4_6_iter_5_log_trans.txt") as source:
    json_source = source.read()
    result_2014_l_a_a_m_4_6_iter_5_log_trans_List = json.loads('[{}]'.format(json_source))
result_2014_l_a_a_m_4_6_iter_5_log_trans_frame = pd.json_normalize(result_2014_l_a_a_m_4_6_iter_5_log_trans_List)

with open("2014_s_a_a_m_4_6_iter_5_log_trans.txt") as source:
    json_source = source.read()
    result_2014_s_a_a_m_4_6_iter_5_log_trans_List = json.loads('[{}]'.format(json_source))
result_2014_s_a_a_m_4_6_iter_5_log_trans_frame = pd.json_normalize(result_2014_s_a_a_m_4_6_iter_5_log_trans_List)

distance_2014_l_a_a_m_4_6_iter_5_log_trans = []
for row1 in result_2014_l_a_a_m_4_6_iter_5_log_trans_frame.itertuples():
    if(row1.lower_limit<-0.0025):
        lower_distance = abs(-0.0025 - row1.lower_limit)
        lower_distance_log = 0
        if (lower_distance != 0):
            lower_distance_log = (-1)*math.log(lower_distance)
            revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
            impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
            temp = {'kappa': 2.00, 'lower_distance': lower_distance,
                    'lower_threshold': row1.lower_limit,
                    'lower_distance_log':lower_distance_log,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                    'impact_of_undetected': impact_of_undetected_attack,
                    'type': LARGE_ADDITIVE,
                    'iteration':row1.iteration}
            distance_2014_l_a_a_m_4_6_iter_5_log_trans.append(temp)
            
if(len(distance_2014_l_a_a_m_4_6_iter_5_log_trans)>0):
    distance_2014_l_a_a_m_4_6_iter_5_log_trans_frame = pd.DataFrame(distance_2014_l_a_a_m_4_6_iter_5_log_trans)
    resultant_frame_4_6_l_a_a_log_trans = distance_2014_l_a_a_m_4_6_iter_5_log_trans_frame[distance_2014_l_a_a_m_4_6_iter_5_log_trans_frame.lower_distance ==
                                               distance_2014_l_a_a_m_4_6_iter_5_log_trans_frame.lower_distance.max()]
    
    grouped_df = distance_2014_l_a_a_m_4_6_iter_5_log_trans_frame.groupby("iteration")
    print("Grouped Max for Month 4-6 LAA For log transformed Data ")
    for key,group in grouped_df:
        # print(group)
        maximums = group[group.lower_distance ==  group.lower_distance.max()]
        print(maximums[['lower_distance','lower_threshold','ro_mal','del_avg','iteration']])
    
    print("resultant_frame_4_6_l_a_a_log_trans")
    print(resultant_frame_4_6_l_a_a_log_trans[['lower_distance','lower_threshold','ro_mal','del_avg']])

distance_2014_s_a_a_m_4_6_iter_5_log_trans = []
for row1 in result_2014_s_a_a_m_4_6_iter_5_log_trans_frame.itertuples():
    if(row1.upper_limit> 0.0175):
        upper_distance = abs( 0.0175 - row1.upper_limit)
        upper_distance_log = 0
        if (upper_distance != 0):
            upper_distance_log = (-1)*math.log(upper_distance)
            revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000  # 24= number of reports per day #70 = M number of compromised meters
            impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
            temp = {'kappa': 2.00, 'upper_distance': upper_distance,
                    'upper_threshold': row1.upper_limit,
                    'upper_distance_log':upper_distance_log,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                    'impact_of_undetected': impact_of_undetected_attack,
                    'type': SMALL_ADDITIVE,
                    'iteration':row1.iteration}
            distance_2014_s_a_a_m_4_6_iter_5_log_trans.append(temp)
if(len(distance_2014_s_a_a_m_4_6_iter_5_log_trans)>0):
    distance_2014_s_a_a_m_4_6_iter_5_log_trans_frame = pd.DataFrame(distance_2014_s_a_a_m_4_6_iter_5_log_trans)
    resultant_frame_4_6_s_a_a_log_trans = distance_2014_s_a_a_m_4_6_iter_5_log_trans_frame[distance_2014_s_a_a_m_4_6_iter_5_log_trans_frame.upper_distance ==
                                               distance_2014_s_a_a_m_4_6_iter_5_log_trans_frame.upper_distance.max()]

    grouped_df = distance_2014_s_a_a_m_4_6_iter_5_log_trans_frame.groupby("iteration")
    print("Grouped Max for Month 4-6 SAA For Log transformed Data ")
    for key,group in grouped_df:
        # print(group)
        maximums = group[group.upper_distance ==  group.upper_distance.max()]
        print(maximums[['upper_distance','upper_threshold','ro_mal','del_avg','iteration']])

    print("resultant_frame_4_6_s_a_a_log_trans")
    print(resultant_frame_4_6_s_a_a_log_trans[['upper_distance','upper_threshold','ro_mal','del_avg']])
pd.concat(result_2014_l_a_a_m_4_6_iter_5_filter_6000_frame,result_2014_s_a_a_m_4_6_iter_5_filter_6000_frame)
#endregion
import json
import math
import matplotlib.pyplot as plt
import pandas as pd

from src.utility.constant import *
#NOTE ALERT
# REMOVE THE [] from the file before start working on the data set
with open("result_actual_threshold_1.txt") as source:
    json_source = source.read()
    thresholdList = json.loads('[{}]'.format(json_source))
threshold_frame = pd.json_normalize(thresholdList)

#region small deductive
with open("result_2014_small_deductive_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_deductive_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_deductive_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_small_deductive_attack_with_std_mon_1_3_List)

with open("result_2014_small_deductive_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_deductive_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_deductive_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_small_deductive_attack_with_std_mon_4_6_List)

with open("result_2014_small_deductive_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_deductive_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_deductive_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_small_deductive_attack_with_std_mon_7_9_List)
#endregion

#region large deductive
with open("result_2014_large_deductive_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_deductive_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_deductive_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_large_deductive_attack_with_std_mon_1_3_List)

with open("result_2014_large_deductive_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_deductive_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_deductive_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_large_deductive_attack_with_std_mon_4_6_List)

with open("result_2014_large_deductive_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_deductive_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_deductive_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_large_deductive_attack_with_std_mon_7_9_List)
#endregion

#region Small Additive
with open("result_2014_small_additive_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_additive_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_additive_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_small_additive_attack_with_std_mon_1_3_List)

with open("result_2014_small_additive_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_additive_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_additive_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_small_additive_attack_with_std_mon_4_6_List)

with open("result_2014_small_additive_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_additive_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_additive_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_small_additive_attack_with_std_mon_7_9_List)
#endregion

#region large Additive
with open("result_2014_large_additive_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_additive_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_additive_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_large_additive_attack_with_std_mon_1_3_List)

with open("result_2014_large_additive_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_additive_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_additive_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_large_additive_attack_with_std_mon_4_6_List)

with open("result_2014_large_additive_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_additive_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_additive_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_large_additive_attack_with_std_mon_7_9_List)
#endregion

#region small camouflage
with open("result_2014_small_camouflage_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_List)

with open("result_2014_small_camouflage_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_List)

with open("result_2014_small_camouflage_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_List)
#endregion

#region large camouflage
with open("result_2014_large_camouflage_attack_with_std_mon_1_3.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_frame = pd.json_normalize(threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_List)

with open("result_2014_large_camouflage_attack_with_std_mon_4_6.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_frame = pd.json_normalize(threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_List)

with open("result_2014_large_camouflage_attack_with_std_mon_7_9.txt") as source:
    json_source = source.read()
    threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_List = json.loads('[{}]'.format(json_source))
threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_frame = pd.json_normalize(threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_List)
#endregion

kappa_2sr_org = threshold_frame[threshold_frame['kappa']==float(2.0)]
#the 40% in 2014 is 1-3 number of meters 70
#region distance for SAA Case 1
kappa_2sr_2014_1_to_3_s_a_a = threshold_result_2014_small_additive_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_small_additive_attack_with_std_mon_1_3_frame['kappa'] == float(2.0) )&
    (threshold_result_2014_small_additive_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40) )]

# print(kappa_2sr_2014_1_to_3_s_a_a)
distance_kappa_2sr_2014_1_to_3_s_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_s_a_a.itertuples():
        upper_distance = abs(row.upper_limit - row1.upper_limit)
        revenue_per_day = (row1.del_avg_s * 70 * E * 24) / 1000 #24= number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24 #24 cause caclculating the impact per hour
        temp = {'kappa': row.kappa, 'upper_distance': upper_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s, 
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_ADDITIVE}
        distance_kappa_2sr_2014_1_to_3_s_a_a.append(temp)
distance_kappa_2sr_2014_1_to_3_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_s_a_a)
# print(distance_kappa_2sr_2014_1_to_3_s_a_a_frame)
fig = distance_kappa_2sr_2014_1_to_3_s_a_a_frame.plot(x="del_avg", y="upper_distance",kind="line", label="M 1-3 SAA")
fig_impact = distance_kappa_2sr_2014_1_to_3_s_a_a_frame.plot(x="del_avg", y="impact_of_undetected",kind="line", label="M 1-3 SAA")

# image.savefig("del_avg_s_a_a"+str(delavg)+"_1_3"+".png")
pass

kappa_2sr_2014_4_to_6_s_a_a = threshold_result_2014_small_additive_attack_with_std_mon_4_6_frame.loc[
    (threshold_result_2014_small_additive_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_additive_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_s_a_a)
distance_kappa_2sr_2014_4_to_6_s_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_s_a_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        upper_distance = abs(row.upper_limit - row1.upper_limit)
        temp = {'kappa': row.kappa, 'upper_distance': upper_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s, 
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_ADDITIVE}
        distance_kappa_2sr_2014_4_to_6_s_a_a.append(temp)
distance_kappa_2sr_2014_4_to_6_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_s_a_a)
distance_kappa_2sr_2014_4_to_6_s_a_a_frame.plot(x="del_avg", y="upper_distance",
                    kind="line", label= "M 4-6 SAA",ax=fig)
distance_kappa_2sr_2014_4_to_6_s_a_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 SAA",ax=fig_impact)

# for delavg in DEL_AVG_ARRAY:
#     fig = distance_kappa_2sr_2014_4_to_6_s_a_a_frame[
#         distance_kappa_2sr_2014_4_to_6_s_a_a_frame['del_avg'] == delavg].plot(x="del_avg", y="upper_distance",
#                         kind="line",
#                         title="Upper Euclidian distance For For 2Sr and 40% compromised  in 2014 month 4 to 6")
#     fig.set_xlabel("Ro_mal")
#     fig.set_ylabel("Upper Euclidian Distance")
#     image = fig.get_figure()
#     plt.show()
#     # image.savefig("del_avg_s_a_a" + str(delavg) + "_4_6" + ".png")
#
#     pass

kappa_2sr_2014_7_to_9_s_a_a = threshold_result_2014_small_additive_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_small_additive_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_additive_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_s_a_a)
distance_kappa_2sr_2014_7_to_9_s_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_s_a_a.itertuples():
        upper_distance = abs(row.upper_limit - row1.upper_limit)
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': row.kappa, 'upper_distance': upper_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s, 
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_ADDITIVE}
        distance_kappa_2sr_2014_7_to_9_s_a_a.append(temp)
distance_kappa_2sr_2014_7_to_9_s_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_s_a_a)
distance_kappa_2sr_2014_7_to_9_s_a_a_frame.plot(x="del_avg", y="upper_distance",
                    kind="line", label= "M 7-9 SAA",ax=fig)
distance_kappa_2sr_2014_7_to_9_s_a_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 SAA",ax=fig_impact)
fig_impact.set_title("Impact of undetected attack in 2014")
fig_impact.set_xlabel("Attack with Small Del avg")
fig_impact.set_ylabel("Impact of Undetected Attack")
fig_impact.legend()

fig.set_title("U.Euclidian Distance for 2Sr with 40% romal in 2014")
fig.set_xlabel("Attack with Small Del avg")
fig.set_ylabel("Upper Euclidian Distance")
fig.legend()
# image = fig.get_figure()
# for delavg in DEL_AVG_ARRAY:
#     fig = distance_kappa_2sr_2014_7_to_9_s_a_a_frame[
#         distance_kappa_2sr_2014_7_to_9_s_a_a_frame['del_avg'] == delavg].plot(x="del_avg", y="upper_distance",
#                         kind="line",
#                         title="Upper Euclidian distance For 2Sr and 40% compromised in 2014 month 7 to 9")
#     fig.set_xlabel("dev_avg")
#     fig.set_ylabel("Upper Euclidian Distance")
#     image = fig.get_figure()
#     plt.show()
#     # image.savefig("del_avg_s_a_a" + str(delavg) + "_7_9" + ".png")
#     pass
#endregion

#region distance for SDA case 2
# print(kappa_2sr_org.itertuples())
kappa_2sr_2014_1_to_3_s_d_a = threshold_result_2014_small_deductive_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_small_deductive_attack_with_std_mon_1_3_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_deductive_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_1_to_3_s_d_a)
distance_kappa_2sr_2014_1_to_3_s_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_s_d_a.itertuples():
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s, 
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_DEDUCTIVE}
        distance_kappa_2sr_2014_1_to_3_s_d_a.append(temp)
distance_kappa_2sr_2014_1_to_3_s_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_s_d_a)
fig1 = distance_kappa_2sr_2014_1_to_3_s_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 1-3 SDA")
fig_impact1 = distance_kappa_2sr_2014_1_to_3_s_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 1-3 SDA")
kappa_2sr_2014_4_to_6_s_d_a = threshold_result_2014_small_deductive_attack_with_std_mon_4_6_frame.loc[
  (threshold_result_2014_small_deductive_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
  (threshold_result_2014_small_deductive_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_s_d_a)
distance_kappa_2sr_2014_4_to_6_s_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_s_d_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_DEDUCTIVE}
        distance_kappa_2sr_2014_4_to_6_s_d_a.append(temp)
distance_kappa_2sr_2014_4_to_6_s_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_s_d_a)
distance_kappa_2sr_2014_4_to_6_s_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 4-6 SDA",ax=fig1)
distance_kappa_2sr_2014_4_to_6_s_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 SDA",ax=fig_impact1)

kappa_2sr_2014_7_to_9_s_d_a = threshold_result_2014_small_deductive_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_small_deductive_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_deductive_attack_with_std_mon_7_9_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_s_d_a)
distance_kappa_2sr_2014_7_to_9_s_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_s_d_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_DEDUCTIVE}
        distance_kappa_2sr_2014_7_to_9_s_d_a.append(temp)
distance_kappa_2sr_2014_7_to_9_s_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_s_d_a)
distance_kappa_2sr_2014_7_to_9_s_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 7-9 SDA",ax=fig1)
distance_kappa_2sr_2014_7_to_9_s_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 SDA",ax=fig_impact1)
#endregion


#region distance for SCA case 2
kappa_2sr_2014_1_to_3_s_c_a = threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_camouflage_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_1_to_3_s_c_a)
distance_kappa_2sr_2014_1_to_3_s_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_s_c_a.itertuples():
        revenue_per_day = ( row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_CAMOUFLAGE}
        distance_kappa_2sr_2014_1_to_3_s_c_a.append(temp)
distance_kappa_2sr_2014_1_to_3_s_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_s_c_a)
distance_kappa_2sr_2014_1_to_3_s_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 1-3 SCA",ax=fig1)
distance_kappa_2sr_2014_1_to_3_s_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 1-3 SCA",ax=fig_impact1)
kappa_2sr_2014_4_to_6_s_c_a = threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_frame.loc[
    (threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_camouflage_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_s_c_a)
distance_kappa_2sr_2014_4_to_6_s_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_s_c_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_CAMOUFLAGE}
        distance_kappa_2sr_2014_4_to_6_s_c_a.append(temp)
distance_kappa_2sr_2014_4_to_6_s_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_s_c_a)
distance_kappa_2sr_2014_4_to_6_s_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 4-6 SCA",ax=fig1)
distance_kappa_2sr_2014_4_to_6_s_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 SCA",ax=fig_impact1)
kappa_2sr_2014_7_to_9_s_c_a = threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_small_camouflage_attack_with_std_mon_7_9_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_s_c_a)
distance_kappa_2sr_2014_7_to_9_s_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_s_c_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': SMALL_CAMOUFLAGE}
        distance_kappa_2sr_2014_7_to_9_s_c_a.append(temp)
distance_kappa_2sr_2014_7_to_9_s_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_s_c_a)
distance_kappa_2sr_2014_7_to_9_s_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 7-9 SCA",ax=fig1)
distance_kappa_2sr_2014_7_to_9_s_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 SCA",ax=fig_impact1)

fig_impact1.set_title("Impact of undetected attack in 2014")
fig_impact1.set_xlabel("Attack with Small Del avg")
fig_impact1.set_ylabel("Impact of undetected attack")
fig_impact1.legend()
fig_impact1.set_title("L.Euclidian Distance for 2Sr with 40% romal in 2014")
fig_impact1.set_xlabel("Attack with Small Del avg")
fig_impact1.set_ylabel("Lower Euclidian Distance")
fig_impact1.legend()
# image = fig.get_figure()
plt.show()
#endregion

#region distance for LDA Case 3
kappa_2sr_2014_1_to_3_l_d_a = threshold_result_2014_large_deductive_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_large_deductive_attack_with_std_mon_1_3_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_deductive_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_1_to_3_l_d_a)
distance_kappa_2sr_2014_1_to_3_l_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_l_d_a.itertuples():
        revenue_per_day = ( row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_DEDUCTIVE}
        distance_kappa_2sr_2014_1_to_3_l_d_a.append(temp)
distance_kappa_2sr_2014_1_to_3_l_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_l_d_a)
fig2 = distance_kappa_2sr_2014_1_to_3_l_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 1-3 LDA")

fig_impact2 = distance_kappa_2sr_2014_1_to_3_l_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 1-3 LDA")
kappa_2sr_2014_4_to_6_l_d_a = threshold_result_2014_large_deductive_attack_with_std_mon_4_6_frame.loc[
    (threshold_result_2014_large_deductive_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_deductive_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_l_d_a)
# print(kappa_2sr_2014_4_to_6_l_d_a)

distance_kappa_2sr_2014_4_to_6_l_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_l_d_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_DEDUCTIVE}
        distance_kappa_2sr_2014_4_to_6_l_d_a.append(temp)
distance_kappa_2sr_2014_4_to_6_l_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_l_d_a)
distance_kappa_2sr_2014_4_to_6_l_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 4-6 LDA",ax=fig2)
distance_kappa_2sr_2014_4_to_6_l_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 LDA",ax=fig_impact2)
kappa_2sr_2014_7_to_9_l_d_a = threshold_result_2014_large_deductive_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_large_deductive_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_deductive_attack_with_std_mon_7_9_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_l_d_a)
distance_kappa_2sr_2014_7_to_9_l_d_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_l_d_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_DEDUCTIVE}
        distance_kappa_2sr_2014_7_to_9_l_d_a.append(temp)
distance_kappa_2sr_2014_7_to_9_l_d_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_l_d_a)
distance_kappa_2sr_2014_7_to_9_l_d_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 7-9 LDA",ax=fig2)
distance_kappa_2sr_2014_7_to_9_l_d_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 LDA",ax=fig_impact2)
#endregion
#region distance for LAA Case 3
kappa_2sr_2014_1_to_3_l_a_a = threshold_result_2014_large_additive_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_large_additive_attack_with_std_mon_1_3_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_additive_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_1_to_3_l_a_a)
distance_kappa_2sr_2014_1_to_3_l_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_l_a_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack, 'type': LARGE_ADDITIVE}
        distance_kappa_2sr_2014_1_to_3_l_a_a.append(temp)
distance_kappa_2sr_2014_1_to_3_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_l_a_a)
distance_kappa_2sr_2014_1_to_3_l_a_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 1-3 LAA",ax=fig2)
distance_kappa_2sr_2014_1_to_3_l_a_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 1-3 LAA",ax=fig_impact2)
kappa_2sr_2014_4_to_6_l_a_a = threshold_result_2014_large_additive_attack_with_std_mon_4_6_frame.loc[
    (threshold_result_2014_large_additive_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_additive_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_l_a_a)
distance_kappa_2sr_2014_4_to_6_l_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_l_a_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s, 
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_ADDITIVE}
        distance_kappa_2sr_2014_4_to_6_l_a_a.append(temp)
distance_kappa_2sr_2014_4_to_6_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_l_a_a)
distance_kappa_2sr_2014_4_to_6_l_a_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 4-6 LAA",ax=fig2)
distance_kappa_2sr_2014_4_to_6_l_a_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 LAA",ax=fig_impact2)
kappa_2sr_2014_7_to_9_l_a_a = threshold_result_2014_large_additive_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_large_additive_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_additive_attack_with_std_mon_7_9_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_l_a_a)
distance_kappa_2sr_2014_7_to_9_l_a_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_l_a_a.itertuples():
        revenue_per_day = ( row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_ADDITIVE}
        distance_kappa_2sr_2014_7_to_9_l_a_a.append(temp)
distance_kappa_2sr_2014_7_to_9_l_a_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_l_a_a)
distance_kappa_2sr_2014_7_to_9_l_a_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 7-9 LAA",ax=fig2)
distance_kappa_2sr_2014_7_to_9_l_a_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 LAA",ax=fig_impact2)
#endregion
#region distance for LCA Case 3
kappa_2sr_2014_1_to_3_l_c_a = threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_frame.loc[
    (threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_camouflage_attack_with_std_mon_1_3_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_1_to_3_l_c_a)
distance_kappa_2sr_2014_1_to_3_l_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_1_to_3_l_c_a.itertuples():
        revenue_per_day = ( row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_CAMOUFLAGE}
        distance_kappa_2sr_2014_1_to_3_l_c_a.append(temp)
distance_kappa_2sr_2014_1_to_3_l_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_1_to_3_l_c_a)
distance_kappa_2sr_2014_1_to_3_l_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 1-3 LCA",ax=fig2)
distance_kappa_2sr_2014_1_to_3_l_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 1-3 LCA",ax=fig_impact2)
kappa_2sr_2014_4_to_6_l_c_a = threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_frame.loc[
    (threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_camouflage_attack_with_std_mon_4_6_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_4_to_6_l_c_a)
distance_kappa_2sr_2014_4_to_6_l_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_4_to_6_l_c_a.itertuples():
        revenue_per_day = ( row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_CAMOUFLAGE}
        distance_kappa_2sr_2014_4_to_6_l_c_a.append(temp)
distance_kappa_2sr_2014_4_to_6_l_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_4_to_6_l_c_a)
distance_kappa_2sr_2014_4_to_6_l_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 4-6 LCA",ax=fig2)
distance_kappa_2sr_2014_4_to_6_l_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 4-6 LCA",ax=fig_impact2)

kappa_2sr_2014_7_to_9_l_c_a = threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_frame.loc[
    (threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_frame['kappa'] == float(2.0))&
    (threshold_result_2014_large_camouflage_attack_with_std_mon_7_9_frame['ro_mal'] == float(.40))]
# print(kappa_2sr_2014_7_to_9_l_c_a)
distance_kappa_2sr_2014_7_to_9_l_c_a = []
for row in kappa_2sr_org.itertuples():
    for row1 in kappa_2sr_2014_7_to_9_l_c_a.itertuples():
        revenue_per_day = (row1.del_avg_s * 70 * E * 1) / 1000  # 1 = number of reports per day #70 = M number of compromised meters
        impact_of_undetected_attack = revenue_per_day / 24  # 24 cause caclculating the impact per hour
        lower_distance = abs(row.lower_limit - row1.lower_limit)
        temp = {'kappa': row.kappa, 'lower_distance': lower_distance,
                    'ro_mal': row1.ro_mal, 'del_avg': row1.del_avg_s,
                'impact_of_undetected':impact_of_undetected_attack,'type': LARGE_CAMOUFLAGE}
        distance_kappa_2sr_2014_7_to_9_l_c_a.append(temp)
distance_kappa_2sr_2014_7_to_9_l_c_a_frame = pd.DataFrame(distance_kappa_2sr_2014_7_to_9_l_c_a)
distance_kappa_2sr_2014_7_to_9_l_c_a_frame.plot(x="del_avg", y="lower_distance",
                    kind="line", label= "M 7-9 LCA",ax=fig2)
distance_kappa_2sr_2014_7_to_9_l_c_a_frame.plot(x="del_avg", y="impact_of_undetected",
                    kind="line", label= "M 7-9 LCA",ax=fig_impact2)

fig_impact2.set_title("L.Euclidian Distance for 2Sr with 40% romal in 2014")
fig_impact2.set_xlabel("Attack with Large Del avg")
fig_impact2.set_ylabel("Lower Euclidian Distance")

fig2.set_title("L.Euclidian Distance for 2Sr with 40% romal in 2014")
fig2.set_xlabel("Attack with Large Del avg")
fig2.set_ylabel("Lower Euclidian Distance")
# fig2.legend()

plt.legend(loc='upper center', bbox_to_anchor=(1.20, 0.8), shadow=True, ncol=1)
plt.show()
#endregion
# distance_array_frame.to_csv(r'output_large_deductive.csv')

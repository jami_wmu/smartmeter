import json
import seaborn as sns;

from src.utility.square_res_inv_sqr import *
from src.utility.square_residuals import *

sns.set()
import pandas as pd
from src.utility.common_functions import *
from src.utility.constant import *
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
import numpy
ruc_frame = pd.read_csv('training_residual.csv')
t_min = calculateTmin_ruc(ruc_frame,TRAINING_DATA.keys())
t_max = calculateTmax_ruc(ruc_frame,TRAINING_DATA)
print("t_min: ",t_min," t_max:",t_max)
# with open("2014_l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021.txt") as source:
#     json_source = source.read()
#     l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_list = json.loads('[{}]'.format(json_source))
# l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame = pd.json_normalize(l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_list)
# with open("2014_s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021.txt") as source:
#     json_source = source.read()
#     s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_list = json.loads('[{}]'.format(json_source))
# s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame = pd.json_normalize(s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_list)
# print(s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame[s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame['upper_limit']==s_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame['upper_limit'].max()])
# print(l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame[l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame['lower_limit']==l_a_a_m_4_6_iter_1_BOX_COX_RUN_01_14_2021_frame['lower_limit'].min()])
# print(ruc_frame)
# y1 = calculate_optimal_max_threshold(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y1 = calculate_optimal_min_threshold(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_min_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_optimal_by_gradient(ruc_frame,TRAINING_DATA.keys())

#square residual
# y1 = calculate_optimal_max_threshold_sqr_res(ruc_frame, TRAINING_DATA.keys())
# y3 = calculate_max_by_gradient_sqr_res(ruc_frame, TRAINING_DATA.keys())
# y1 = calculate_optimal_min_threshold_sqr_res(ruc_frame, TRAINING_DATA.keys())
y1 = calculate_min_by_gradient(ruc_frame,TRAINING_DATA.keys())
y2 = calculate_min_by_gradient_sqr_res_inv_sqr(ruc_frame, TRAINING_DATA.keys())
y3 = calculate_min_by_gradient_sqr_res(ruc_frame, TRAINING_DATA.keys())
#y1 = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_max_by_gradient_sqr_res_inv_sqr(ruc_frame, TRAINING_DATA.keys())
# y3 = calculate_max_by_gradient_sqr_res(ruc_frame, TRAINING_DATA.keys())
x = numpy.arange(len(y2))
plt.plot(x, y1, color='b', label='l2 gradient ind sqr') # r - red colour
plt.plot(x, y2, color='r', label='l2 gradient ind sqr') # r - red colour
plt.plot(x, y3, color='g', label='l2 gradient') # g - green colour
plt.xlabel("Number of RUC ")
plt.ylabel("Loss = Abs(Sum(Cost) - Sum(Penalty)) ")
plt.title("Loss for tao-min with abs grad and points shifted")

plt.legend()
# plt.grid()
plt.show()

diff = [0 for i in range(len(y2))]
for i in range(len(y2)):
    diff[i] =  y2[i] - y3[i]
print("Difference gradient to RUC ordered")
print(diff)


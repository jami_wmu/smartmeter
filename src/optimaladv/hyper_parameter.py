import pandas as pd
import matplotlib.pyplot as plt
import numpy

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 30

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)

hyper_2 = pd.read_csv("std_hyper_2.csv")
hyper_4 = pd.read_csv("std_hyper_4.csv")
hyper_6 = pd.read_csv("std_hyper_6.csv")
hyper_8 = pd.read_csv("std_hyper_8.csv")

x = numpy.arange(len(hyper_2))

# plt.plot(x, hyper_2['std_limit'], color='r', label='hyper 2', marker ='X',markevery = 10) # r - red colour
# plt.plot(x, hyper_4['std_limit'], color='b', label='hyper 4',linestyle='-.' ,marker ='D',markevery = 10) # r - red colour
# plt.plot(x, hyper_6['std_limit'], color='g', label='hyper 6',linestyle=':' ,marker ='o',markevery = 10) # r - red colour
# plt.plot(x, hyper_8['std_limit'], color='k', label='hyper 8',linestyle='--' ,marker ='v',markevery = 10) # r - red colour

plt.plot(hyper_2['std_limit'],hyper_2['loss'], color='r', label='hyper 2', marker ='X',markevery = 10) # r - red colour
plt.plot(hyper_4['std_limit'],hyper_4['loss'], color='b', label='hyper 4',linestyle='-.' ,marker ='D',markevery = 10) # r - red colour
plt.plot(hyper_6['std_limit'], hyper_6['loss'],color='g', label='hyper 6',linestyle=':' ,marker ='o',markevery = 10) # r - red colour
plt.plot(hyper_8['std_limit'], hyper_8['loss'],color='k', label='hyper 8',linestyle='--' ,marker ='v',markevery = 10) # r - red colour

# fig = plt.figure()
# ax = fig.gca(projection='3d')

# ax.plot_trisurf(x,hyper_2['std_limit'],hyper_2['loss'])
# ax.plot_trisurf(x,hyper_4['std_limit'],hyper_4['loss'],linewidth=0.2, antialiased=True)
# ax.plot_trisurf(x,hyper_6['std_limit'],hyper_6['loss'])
# ax.plot_trisurf(x,hyper_8['std_limit'],hyper_8['loss'])
# plt.show()
# plt.xlabel("Number of RUC ")
# plt.ylabel("Standard Limit")

plt.xlabel("DegreStandard Limit ")
plt.ylabel("Loss")

plt.legend()
plt.show()
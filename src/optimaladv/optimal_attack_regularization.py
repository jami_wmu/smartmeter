import json
import seaborn as sns; sns.set()
import pandas as pd
from src.utility.common_functions import *
from src.utility.constant import *
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
import numpy
ruc_frame = pd.read_csv('training_residual.csv')
t_min = calculateTmin_ruc(ruc_frame,TRAINING_DATA.keys())
t_max = calculateTmax_ruc(ruc_frame,TRAINING_DATA)
print("t_min: ",t_min," t_max:",t_max)
y1 = calculate_optimal_max_threshold(ruc_frame,TRAINING_DATA.keys())
y2 = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y1 = calculate_optimal_min_threshold(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_min_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y2 = calculate_optimal_by_gradient(ruc_frame,TRAINING_DATA.keys())
x = numpy.arange(len(y1))

plt.plot(x, y1, color='r', label='Order by RUC') # r - red colour
plt.plot(x, y2, color='g', label='Order by Grad') # g - green colour
plt.xlabel("Number of RUC ")
plt.ylabel("Loss = Abs(Sum(Cost) - Sum(Penalty)) ")
plt.title("Loss Maximization signed gradient flipped")

plt.legend()
# plt.grid()
plt.show()

diff = [0 for i in range(len(y1))]
for i in range(len(y1)):
    diff[i] =  y2[i] - y1[i]
# print("Difference gradient to RUC ordered")
# print(diff)


from functools import reduce
import sys
import numpy
import pandas as pd
import matplotlib.pyplot as plt
# from src.utility.common_functions import *
from src.utility.constant import *


def calculateTmax_ruc(rucFrame, keys):
    difference = dict()
    maxThreshold = 0
    kappa = 0
    for findex in rucFrame.keys():
        print("Iterate for kappa:", findex)
        maxSum = sys.float_info.max
        for taoThreshold in numpy.arange(0.0000, .100, 0.0025):
            costSum = 0
            pSum = 0
            costCount = 0
            penaltyCount = 0
            for row in rucFrame[findex].itertuples():
                for key in keys:
                    if (getattr(row, "ruc" + key) > 0):  # previously It was greater equal
                        # tdsc says to iterate only non zero items
                        if (getattr(row, "ruc" + key) < taoThreshold):
                            costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                            costCount = costCount + 1
                        else:
                            pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                            penaltyCount = penaltyCount + 1
            if (pSum != 0 and costSum != 0):
                # taoSumDiff = abs(costSum - pSum+ (costCount-penaltyCount)*taoThreshold)
                taoSumDiff = abs(costSum - pSum)
                difference[taoThreshold] = taoSumDiff
                if (maxSum > taoSumDiff):
                    maxSum = taoSumDiff
                    maxThreshold = taoThreshold
                    kappa = findex
        print("kappa:",kappa," maxThreshold: ",maxThreshold," maxsum:",maxSum)

    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    fig = plt.figure()
    plt.scatter(x, y, marker='o')
    fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Cost - Penalty')
    # fig.savefig('test.jpg')
    plt.show()
    return maxThreshold,kappa


def calculateTmin_ruc(rucFrame, keys):
    minSum = sys.float_info.max
    minThreshold = 0
    difference = dict()
    kappa = 0
    for findex in rucFrame.keys():
        print("Iterate for kappa:", findex)
        minSum = sys.float_info.max
        for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
            costSum = 0
            pSum = 0
            costCount = 0
            penaltyCount = 0
            print(rucFrame[findex])
            for row in rucFrame[findex].itertuples():
                # print(row)
                for key in keys:
                    if (getattr(row, "ruc" + key) < 0):
                        if (getattr(row, "ruc" + key) > ((-1) * taoThreshold)):
                            costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                            costCount = costCount + 1
                        else:
                            pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                            penaltyCount = penaltyCount + 1
            if (costSum != 0 and pSum != 0):
                taoSumDiff = abs(costSum - pSum)
                difference[taoThreshold] = taoSumDiff
                if (minSum > taoSumDiff):
                    minSum = taoSumDiff
                    minThreshold = taoThreshold * (-1)
                    kappa = findex
        print("kappa:",kappa," minthreshold: ",minThreshold," minsum:",minSum)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    fig = plt.figure()
    plt.plot(x, y)
    fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Cost - Penalty')
    # fig.savefig('test.jpg')
    plt.show()
    return minThreshold,kappa

frame_dict = dict()
ruc_frame1 = pd.read_csv('training_residual0.025.csv')
frame_dict[.025] = ruc_frame1
ruc_frame2 = pd.read_csv('training_residual0.1.csv')
frame_dict[0.1] = ruc_frame1
ruc_frame3 = pd.read_csv('training_residual0.1875.csv')
frame_dict[0.1875] = ruc_frame1
ruc_frame4 = pd.read_csv('training_residual0.3125.csv')
frame_dict[0.3125] = ruc_frame1
ruc_frame5 = pd.read_csv('training_residual0.4375.csv')
frame_dict[0.4375] = ruc_frame1
ruc_frame6 = pd.read_csv('training_residual0.625.csv')
frame_dict[0.625] = ruc_frame1
ruc_frame7 = pd.read_csv('training_residual0.8125.csv')
frame_dict[0.8125] = ruc_frame1
ruc_frame8 = pd.read_csv('training_residual1.0.csv')
frame_dict[1.0] = ruc_frame1
ruc_frame9 = pd.read_csv('training_residual1.125.csv')
frame_dict[1.125] = ruc_frame1
ruc_frame10 = pd.read_csv('training_residual1.25.csv')
frame_dict[1.25] = ruc_frame1
ruc_frame11 = pd.read_csv('training_residual1.375.csv')
frame_dict[1.375] = ruc_frame1
ruc_frame12 = pd.read_csv('training_residual1.5.csv')
frame_dict[1.5] = ruc_frame1
ruc_frame13 = pd.read_csv('training_residual1.7.csv')
frame_dict[1.7] = ruc_frame1
ruc_frame14 = pd.read_csv('training_residual1.9.csv')
frame_dict[1.9] = ruc_frame1
ruc_frame15 = pd.read_csv('training_residual.csv')
frame_dict[2.00] = ruc_frame1
t_min,kappa = calculateTmin_ruc(frame_dict,TRAINING_DATA.keys())
t_max,kappa1 = calculateTmax_ruc(frame_dict,TRAINING_DATA)
print("t_min: ",t_min," t_max:",t_max)
print("kappa: ",kappa," kappa1:",kappa1)



# ruc_frame_array = [ruc_frame1,ruc_frame2,ruc_frame3,ruc_frame4,ruc_frame5,ruc_frame6,ruc_frame7,ruc_frame8,
#                      ruc_frame9,ruc_frame10,ruc_frame11,ruc_frame12,ruc_frame13,ruc_frame14,ruc_frame15]
# # ruc_frame = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), ruc_frame_array)
# ruc_frame = pd.concat(ruc_frame_array)
# ruc_frame_updated =ruc_frame[ ['ruc2014','ruc2015','day','month']]
# # # print(ruc_frame_updated)
# # t_min = calculateTmin_ruc(ruc_frame,TRAINING_DATA.keys())
# # t_max = calculateTmax_ruc(ruc_frame,TRAINING_DATA)
# # print("t_min: ",t_min," t_max:",t_max)
# y1 = calculate_optimal_min_threshold(ruc_frame_updated,TRAINING_DATA.keys())
# y2 = calculate_min_by_gradient(ruc_frame_updated,TRAINING_DATA.keys())
# # y1 = calculate_optimal_max_threshold(ruc_frame,TRAINING_DATA.keys())
# # y2 = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
# x = numpy.arange(len(y1))
#
# plt.plot(x, y1, color='r', label='Order by RUC') # r - red colour
# plt.plot(x, y2, color='g', label='Order by Grad') # g - green colour
# plt.xlabel("Number of RUC ")
# plt.ylabel("Loss = Abs(Sum(Cost) - Sum(Penalty)) ")
# plt.title("Loss Maximization Manual gradient")
#
# plt.legend()
# plt.grid()
# plt.show()
#
# diff = [0 for i in range(len(y1))]
# for i in range(len(y1)):
#     diff[i] =  y2[i] - y1[i]
# lower_limit = -0.090
# def function_value(input):
#     costSum = 0
#     pSum =0
#     for l in range(len(input)):
#         if (input[l] > lower_limit):
#             costSum += abs(lower_limit - input[l]) / 2
#         else:
#             pSum += abs(lower_limit - input[l]) * 2
#     return  abs(costSum - pSum)
# if __name__ == "__main__":
#     ruc_frame =    pd.read_csv('training_residual_new.csv')
#     # print(sorted_frame[key].head(5))
#     sorted_non_zero_ruc = ruc_frame.sort_values(by=["ruc2014"])[ruc_frame.sort_values(by=["ruc2014"])['ruc2014' ] < 0]
#     non_zero_ruc_array = sorted_non_zero_ruc["ruc2014"].values
#     org_output = function_value(non_zero_ruc_array)
#     # print(org_output)
#     # print("Gradient by tensorflow")
#     # inp = tf.Variable(np.array(non_zero_ruc_array))
#     #
#     # with tf.GradientTape() as tape:
#     #     preds = model.predict(inp)
#     #
#     # grads = tape.gradient(preds, inp)
#     # print(grads)
#     # print("Gradient by tensorflow ending")
#     gradient = []
#     for l in range(len(non_zero_ruc_array)):
#         non_zero_ruc_array_copy = non_zero_ruc_array.copy()
#         # print(non_zero_ruc_array_copy[l])
#         non_zero_ruc_array_copy[l] =  non_zero_ruc_array_copy[l] - float(1e-6)
#         # print(non_zero_ruc_array_copy[l])
#         changed_output = function_value(non_zero_ruc_array_copy)
#         # print(org_output)
#         gradient.append(((changed_output - org_output) / float(1e-6)))
#     print(gradient)
#     index_of_sorted_list = sorted(range(len(gradient)), key=lambda k: gradient[k], reverse=True)
#     print(index_of_sorted_list)
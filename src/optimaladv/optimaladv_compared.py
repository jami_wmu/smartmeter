import seaborn as sns
from src.utility.square_res_inv_sqr import *
from src.utility.square_residuals import *
# sns.set()
from src.utility.common_functions import *
from src.utility.constant import *
# import matplotlib as mpl
import numpy
SMALL_SIZE = 8
MEDIUM_SIZE = 18
BIGGER_SIZE = 36

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
# mpl.rcParams['agg.path.chunksize'] = 10000
ruc_frame = pd.read_csv('training_residual_new.csv')
#region tao-min
# y1,target1 = calculate_min_by_gradient(ruc_frame,TRAINING_DATA.keys())
# df = pd.DataFrame(y1,columns=['loss'])
# df.to_csv('l1_QR_Loss_t_33.csv')
ruc_frame_array = calculate_min_by_gradient(ruc_frame,TRAINING_DATA.keys())
# pd.DataFrame(ruc_frame_array).to_csv("../loss_minimization/ruc_for_l1_fgsv.csv")
# y2,target2 = calculate_min_by_gradient_no_shift(ruc_frame,TRAINING_DATA.keys())
# ruc_frame_array_1 = calculate_min_by_gradient_no_shift(ruc_frame,TRAINING_DATA.keys())
# pd.DataFrame(ruc_frame_array_1).to_csv("../loss_minimization/ruc_for_l1_fgsv.csv")
# y3,target3 = calculate_min_by_gradient_sqr_res(ruc_frame, TRAINING_DATA.keys())
# y3,target3 = calculate_min_by_RUC_order(ruc_frame, TRAINING_DATA.keys())
# df3 = pd.DataFrame(y3,columns=['loss'])
# df3.to_csv('l2_QR_Loss_t_16.csv')
# ruc_frame_array_1 = calculate_min_by_gradient_sqr_res(ruc_frame,TRAINING_DATA.keys())
# pd.DataFrame(ruc_frame_array_1).to_csv("../loss_minimization/ruc_for_l2_R_fgsm.csv")
# y4,target4 = calculate_min_by_gradient_sqr_res_no_shift(ruc_frame, TRAINING_DATA.keys())
# ruc_array = calculate_min_by_gradient_sqr_res_no_shift(ruc_frame, TRAINING_DATA.keys())
# pd.DataFrame(ruc_array).to_csv("../loss_minimization/ruc_for_l2_fgsv.csv")

# y5,target5 = calculate_min_by_gradient_sqr_res_inv_sqr(ruc_frame, TRAINING_DATA.keys()) #individual element square
# ruc_array = calculate_min_by_gradient_sqr_res_inv_sqr(ruc_frame, TRAINING_DATA.keys())
# pd.DataFrame(ruc_array).to_csv("../loss_minimization/ruc_for_l2_R_ind_fgsm.csv")
# y6 = calculate_min_by_gradient_sqr_res_inv_sqr_no_shift(ruc_frame, TRAINING_DATA.keys())
#endregion
#region tao-max
# y1,target1 = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
ruc_frame_array_max = calculate_max_by_gradient(ruc_frame,TRAINING_DATA.keys())
# y2,target2 = calculate_max_by_gradient_no_shift(ruc_frame,TRAINING_DATA.keys())
# y3,target3 = calculate_max_by_RUC_order(ruc_frame, TRAINING_DATA.keys())
# y3,target3 = calculate_max_by_gradient_sqr_res(ruc_frame, TRAINING_DATA.keys())
# y4,target4 = calculate_max_by_gradient_sqr_res_no_shift(ruc_frame, TRAINING_DATA.keys())
# y5 = calculate_max_by_gradient_sqr_res_inv_sqr(ruc_frame, TRAINING_DATA.keys())
# y6 = calculate_max_by_gradient_sqr_res_inv_sqr_no_shift(ruc_frame, TRAINING_DATA.keys())
#endregion
x = numpy.arange(len(y1))
# temp = dict()
# temp["l1"+str(target1)] = ((y1))
# temp["l1_shifted"+str(target2)]=((y2))
# temp["l2"+str(target3)]= ((y3))
# temp["l2shifted"+str(target4)]=((y4))
# temp_frame = pd.DataFrame.from_dict(temp)
# temp_frame.to_csv(r"min_plot_array.csv")
plt.plot(x, y1, color='r', label='l1-FGSV', marker ='X',markevery = 5) # r - red colour
plt.plot(x, y2, color='g', label='l1-FGAV',marker ='D',markevery = 5) # r - red colour
plt.plot(x, y3, color='b', label='RUC Order', marker = '>', markevery = 5) # g - green colour
# plt.plot(x, y4, color='g', label='l2 gradient',marker ='D', markevery = 5) # r - red colour
# plt.plot(x, y5, color='b', label='l2 gradient sqr term') # g - green colour
# plt.plot(x, y6, color='b', label='l2 gradient sqr term') # g - green colour
# plt.axvline(x=target1, color='k',label='Target FGSV', linestyle='--')
# plt.axvline(x=target2, color='b', label='Target FGAV',linestyle='--')
# plt.axvline(x=target3, color='k',label='Target l2 shifted', linestyle='--')
# plt.axvline(x=target4, color='k', label='Target l2',linestyle='--')

plt.xlabel("Number of RUC ")
plt.ylabel("Loss")
# plt.title("Compare Two Loss Function")

plt.legend()
# plt.grid()
plt.show()

# diff = [0 for i in range(len(y2))]
# for i in range(len(y2)):
#     diff[i] =  y2[i] - y3[i]
# print("Difference gradient to RUC ordered")
# print(diff)


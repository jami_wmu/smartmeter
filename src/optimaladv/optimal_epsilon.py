import pandas as pd
import matplotlib.pyplot as plt
import numpy
import math
from scipy import stats

from src.utility.constant_new import KAPPA
epsilon_margin = {}
for item in KAPPA:
    print("Kappa: ",item)
    df = pd.read_csv("../main/training_RUC/training_RUC_mad_"+str(item)+"csv")
    # df = pd.read_csv("../main/training_RUC")
    # print(df)
    df1 = pd.DataFrame()
    df2 = pd.DataFrame()
    df1['RUC'] = df['ruc2014']
    # print(df1)
    df2['RUC'] = df['ruc2015']
    # print(df2)
    df_merged = pd.concat([df1,df2])
    # print(df_merged)
    df_pos = df_merged[df_merged['RUC']> float(0)]
    # print(df_pos)
    df_neg = df_merged[df_merged['RUC']< float(0)]
    # print(df_neg)

    # # df['RUC'].hist(bins=5)
    # df['RUC_LOG'] = numpy.log(df['RUC'])
    # print(df.head(5))
    # df['RUC_LOG'].hist(bins=5)
    # mean = df['RUC_LOG'].mean()
    # mean_exp = math.exp(mean)
    mean1 = df_pos['RUC'].mean()
    std1  = df_pos['RUC'].mad()
    mean2 = df_neg['RUC'].mean()
    median1 = df_neg['RUC'].median()
    median2 = df_pos['RUC'].median()
    # print("MAX POS RUC: ",df_pos['RUC'].max()," The compared part: ",mean1+3*std1)
    print("median1: ",median1," median2: ",median2)
    std2 = df_neg['RUC'].mad()
    # print("MAX NEG RUC: ",df_neg['RUC'].min()," The compared part: ",mean2-3*std2)
    gmean_neg = stats.gmean(df_neg['RUC']*(-1))
    gmean_pos = stats.gmean(df_pos['RUC'])
    # print("Gmean1: ",gmean_neg," gmean pos: ",gmean_pos)
    print(mean1,",", std1)
    print(mean2,",",std2)
    epsilon_margin[item] = {"mean":mean1,"std":std1}
    # print("Mean RUC: ", mean1," mean RUC_LOG:",mean, "mean_exp: ",mean_exp," diff: ",(mean1-mean_exp))
    # plt.show()
epsilon_array_dict = {}
for key in epsilon_margin.keys():
    eps1 = epsilon_margin[key]['mean'] + epsilon_margin[key]['std']
    eps2 = epsilon_margin[key]['mean'] + 2*epsilon_margin[key]['std']
    eps3 = epsilon_margin[key]['mean'] + 3*epsilon_margin[key]['std']
    eps4 = epsilon_margin[key]['mean'] + 4*epsilon_margin[key]['std']
    eps5 = epsilon_margin[key]['mean'] + 5*epsilon_margin[key]['std']
    epsilon_array_dict[key] = [eps1,eps2,eps3,eps4,eps5]
print(epsilon_array_dict)
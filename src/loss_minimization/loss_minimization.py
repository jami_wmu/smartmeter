import csv
import sys
import matplotlib.pyplot as plt
import numpy
import pandas as pd

def calculateTmax_ruc(rucFrame):
    global maxThreshold
    maxSum = sys.float_info.max
    # print("minThreshold: ",minThreshold)
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            # print(row)

            if (row > 0):
                if (row >  taoThreshold):
                    # temp = taoThreshold - getattr(row, "ruc" + key)
                    # costSum += abs(pow(temp, 2)) / 2
                    costSum += abs(taoThreshold - row) / 2
                    costCount = costCount + 1
                else:
                    # temp = taoThreshold - getattr(row, "ruc" + key)
                    # pSum += abs(pow(temp, 2)) * 2
                    pSum += abs(taoThreshold - row) * 2
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            # print("Both are not zero at least once")
            # taoSumDiff = abs(costSum - pSum + (costCount-penaltyCount)*taoThreshold)
            taoSumDiff = abs(costSum - pSum)
            difference[taoThreshold] = taoSumDiff
            if (maxSum > taoSumDiff):
                # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold*(-1))
                maxSum = taoSumDiff
                maxThreshold = taoThreshold * (-1)
        # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    # w = csv.writer(open("../plot_helper/under_attack_loss_l2_fgsv.csv", "w"))
    # for key, val in difference.items():
    #     w.writerow([key, val])
    # x, y = zip(*lists)  # unpack a list of pairs into two tuples
    #
    # fig = plt.figure()
    # plt.plot(x, y)
    # # fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    # plt.xlabel('Standard Limit')
    # plt.ylabel('Loss')
    # # fig.savefig('test.jpg')
    # plt.show()
    # print(minThreshold)
    return maxThreshold,maxSum

def calculateTmin_ruc_LR(rucFrame):
    global minThreshold
    minSum = sys.float_info.max
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        for row in rucFrame:
            if (row < 0):
                # costSum += abs(taoThreshold*(-1) - row)
                costSum += pow(abs(taoThreshold*(-1) - row),2)
        taoSumDiff = abs(costSum)
        difference[taoThreshold] = taoSumDiff
        if (minSum > taoSumDiff):
            minSum = taoSumDiff
            minThreshold = taoThreshold * (-1)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    # w = csv.writer(open("../plot_helper/under_attack_loss_l2_fgsv.csv", "w"))
    # for key, val in difference.items():
    #     w.writerow([key, val])
    # x, y = zip(*lists)  # unpack a list of pairs into two tuples
    #
    # fig = plt.figure()
    # plt.plot(x, y)
    # # fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    # plt.xlabel('Standard Limit')
    # plt.ylabel('Loss')
    # # fig.savefig('test.jpg')
    # plt.show()
    # print(minThreshold)
    return minThreshold,minSum

def calculateTmin_ruc(rucFrame):
    global minThreshold
    minSum = sys.float_info.max
    # print("minThreshold: ",minThreshold)
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            # print(row)

            if (row < 0):
                if (row > ((-1) * taoThreshold)):
                    # temp = abs(taoThreshold*(-1) - row)/2
                    # costSum += pow(temp, 2)
                    costSum += abs(taoThreshold*(-1) - row) / 2
                    costCount = costCount + 1
                else:
                    # temp = abs(taoThreshold*(-1) - row)*2
                    # pSum += pow(temp, 2)
                    pSum += abs(taoThreshold*(-1) - row) * 2
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            # print("Both are not zero at least once")
            # taoSumDiff = abs(costSum - pSum + (costCount-penaltyCount)*taoThreshold)
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            if (minSum > taoSumDiff):
                # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold*(-1))
                minSum = taoSumDiff
                minThreshold = taoThreshold * (-1)
        # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    # w = csv.writer(open("../plot_helper/under_attack_loss_l1_fgsv.csv", "w"))
    # for key, val in difference.items():
    #     w.writerow([key, val])
    x, y = zip(*lists)  # unpack a list of pairs into two tuples

    # fig = plt.figure()
    # plt.plot(x, y)
    # # fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    # plt.xlabel('Standard Limit')
    # plt.ylabel('Loss')
    # fig.savefig('test.jpg')
    # plt.show()
    # print(minThreshold)
    return minThreshold,minSum
# ruc_for_l1_fgsm = pd.read_csv('ruc_for_l1_fgsv.csv', names=['ruc'])
# ruc_for_l1_R_fgsm = pd.read_csv('ruc_for_l1_R_fgsm.csv',names = ['ruc'])
# ruc_fgsm_list = ruc_for_l1_fgsm['ruc'].to_list()
# ruc_r_fgsm_list = ruc_for_l1_R_fgsm['ruc'].to_list()
# calculateTmin_ruc(ruc_fgsm_list)
# calculateTmin_ruc(ruc_r_fgsm_list)
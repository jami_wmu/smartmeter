from src.test_RUC_generator.ruc_generator_EA import Test_Threshold
from src.utility.constant import *
from tqdm import tqdm
import pandas as pd
def main():
    test_process = Test_Threshold()
    for k in tqdm(KAPPA,desc="Progress"):
        df = pd.read_csv('../main/safe_margin_kappa_varied/safe_margin_mad_'+str(k)+'.csv')
        for del_avg in DEL_AVG_ARRAY_DED_TE:
            for ro_mal in RO_MAL_ARRAY:
                test_process.testing_RUC_TE(TESTING_DATA,df,ro_mal,del_avg,'ded')


main()


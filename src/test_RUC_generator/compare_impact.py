import json
import math
import matplotlib.pyplot as plt
import pandas as pd
from src.utility.constant import *
from mpl_toolkits.mplot3d import Axes3D
with open("result_impact_of_undetected.txt") as source:
    json_source = source.read()
    result_impact_of_undetected_List = json.loads('[{}]'.format(json_source))
result_impact_of_undetected_frame = pd.json_normalize(result_impact_of_undetected_List)
grouped_df = result_impact_of_undetected_frame.groupby("attack_type")
for key,group in grouped_df:
    print("For Attack Type", key)
    group_by_ro_mal = group.groupby("ro_mal")
    for key1,group1 in group_by_ro_mal:
        print("Impact for ro_mal",key1)
        print(group1[group1.impact_of_undetected ==group1.impact_of_undetected.max()])

print("\n")
max_impact = result_impact_of_undetected_frame[result_impact_of_undetected_frame.impact_of_undetected ==
                                               result_impact_of_undetected_frame.impact_of_undetected.max()]
print(max_impact)
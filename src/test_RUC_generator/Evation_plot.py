import json
import matplotlib.pyplot as plt
import pandas as pd
from src.utility.constant import *
SMALL_SIZE = 8
MEDIUM_SIZE = 14
BIGGER_SIZE = 24

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
with open("with_noise/result_evasion_test_e_s_d_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_e_s_d_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_e_s_d_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_e_s_d_a_2016_4_6_List)
result_evasion_test_e_s_d_a_2016_4_6_frame['attack_type'] = SMALL_DEDUCTIVE
#Total Number of meters 192
#region impact of undetected
number_of_meters = 192
attack_start_day = 91
attack_end_day = 181
impact_arra_org = []
impact_arra_att = []
range_att =None
range_org =None
for row in result_evasion_test_e_s_d_a_2016_4_6_frame.itertuples():
    if(int(row.first_detected_att) == int(0)):
       range_att = attack_end_day - attack_start_day
    if int(row.first_detected_org) == int(0):
        range_org = (attack_end_day - attack_start_day)
    if int(row.first_detected_att) != int(0):
        print("executing this")
        range_att = row.first_detected_att - attack_start_day
    if int(row.first_detected_org) != int(0):
        print("executing this")
        range_org = row.first_detected_org - attack_start_day
    print(range_att," ",range_org)
    impact_org = row.del_avg * row.ro_mal * number_of_meters * E * range_org/1000
    impact_att = row.del_avg * row.ro_mal * number_of_meters * E * range_att/1000
    print("Del avg: ",row.del_avg," romal: ", row.ro_mal)
    plt.bar(["Not Attacked Std limit","Attacked Std limit"], [impact_org/24,impact_att/24], color='blue')
    plt.ylabel("Impact(USD per Day)")
    # plt.title("Compared impact of Undetected attack")
    plt.show()
    break
    # impact_arra_org.append(range_org)
    # impact_arra_att.append(range_att)
# result_evasion_test_e_s_d_a_2016_4_6_frame['impact_org'] =impact_arra_org
# result_evasion_test_e_s_d_a_2016_4_6_frame['impact_att'] =impact_arra_att
# print(result_evasion_test_e_s_d_a_2016_4_6_frame.head(5))
# fig = plt.figure()
# ax = plt.axes(projection='3d')
# ax.contour3D(result_evasion_test_e_s_d_a_2016_4_6_frame["del_avg"],
#              result_evasion_test_e_s_d_a_2016_4_6_frame['ro_mal'],
# result_evasion_test_e_s_d_a_2016_4_6_frame['impact_org'], 50, cmap='binary')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Impact')
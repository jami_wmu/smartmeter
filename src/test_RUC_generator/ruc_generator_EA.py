#calculates the residual from the safe margin
#then compares with the optimal thresholds
# finds out if any romal and delavg is detected by the defense system

# MAX Lower Distance for all ROMALs
#      lower_distance  lower_threshold  ro_mal  del_avg
# 511           0.235          -0.2525     0.4     3000
# MAX Upper Distance for all ROMALs
#       upper_distance  upper_threshold  ro_mal  del_avg
# 399           0.0575            0.065     0.6      750
# 1154          0.0575            0.065     0.6      800
import json
from functools import reduce
from datetime import datetime

from src.utility.common_functions import *
from src.utility.constant import *
import os as o
import matplotlib.pyplot as plt
class Test_Threshold:
    def __init__(self):
        pass
    def testing_RUC(self,TESTING_DATA,historical_safe_margin,ro_mal,del_TR,del_TE,type):
        file_data = {}
        file_data_filtered = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        if (float(del_TE) != float(0) and float(ro_mal) != float(0)):
            if(type == 'ded'):
                additive_attack_data, number_of_compromised_meter = perform_deductive_attack(file_data_filtered,
                                                                                         ro_mal,
                                                                                         del_TE,
                                                                                         START_DATE_TEST,
                                                                                         END_DATE_TEST)
            else:
                additive_attack_data, number_of_compromised_meter = perform_additive_attack(file_data_filtered,
                                                                                             ro_mal,
                                                                                             del_TE,
                                                                                             START_DATE_TEST,
                                                                                             END_DATE_TEST)
            for key in additive_attack_data.keys():
                additive_attack_data[key]['use'].loc[additive_attack_data[key]['use'] <= 0] = 10
            file_data_transformed = box_cox_transformation(additive_attack_data)
        else:
            # print("Executing for unperturbed case")
            file_data_transformed = box_cox_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        nabla_frame = calculate_nabla(merged_with_safe_margin, TESTING_DATA.keys())
        residual_frame = calculate_ruc(nabla_frame, TESTING_DATA.keys())
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        ruc_with_safe_margin = pd.merge(merged_with_safe_margin, residual_frame, on=['day'])
        # ruc_with_safe_margin.to_csv("test_RUC/Test_RUC_TR"+str(del_TR)+"_TE_"+str(del_TE)+
        #                             "_RO_"+str(ro_mal)+type+"_"+START_DATE_TEST+"_"+END_DATE_TEST+".csv")

        ruc_with_safe_margin.to_csv("test_RUC_RA/Test_RUC_TR" + str(del_TR) + "_TE_" + str(del_TE) +
                                    "_RO_" + str(ro_mal) + type +"M4M6.csv")

    def testing_RUC_TE(self, TESTING_DATA, historical_safe_margin, ro_mal, del_TE, type):
        file_data = {}
        file_data_filtered = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        if (float(del_TE) != float(0) and float(ro_mal) != float(0)):
            if (type == 'ded'):
                additive_attack_data, number_of_compromised_meter = perform_deductive_attack(file_data_filtered,
                                                                                             ro_mal,
                                                                                             del_TE,
                                                                                             START_DATE_TEST,
                                                                                             END_DATE_TEST)
            else:
                additive_attack_data, number_of_compromised_meter = perform_additive_attack(file_data_filtered,
                                                                                            ro_mal,
                                                                                            del_TE,
                                                                                            START_DATE_TEST,
                                                                                            END_DATE_TEST)
            for key in additive_attack_data.keys():
                additive_attack_data[key]['use'].loc[additive_attack_data[key]['use'] <= 0] = 10
            file_data_transformed = box_cox_transformation(additive_attack_data)
        else:
            print("Executing for unperturbed case")
            file_data_transformed = box_cox_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        nabla_frame = calculate_nabla(merged_with_safe_margin, TESTING_DATA.keys())
        residual_frame = calculate_ruc(nabla_frame, TESTING_DATA.keys())
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        ruc_with_safe_margin = pd.merge(merged_with_safe_margin, residual_frame, on=['day'])
        # ruc_with_safe_margin.to_csv("test_RUC/Test_RUC_TR"+str(del_TR)+"_TE_"+str(del_TE)+
        #                             "_RO_"+str(ro_mal)+type+"_"+START_DATE_TEST+"_"+END_DATE_TEST+".csv")

        ruc_with_safe_margin.to_csv("test_RUC_SA/Test_RUC_TE_" + str(del_TE) +
                                    "_RO_" + str(ro_mal) + type + "M7M9.csv")

    def testing_RUC_Benign(self,TESTING_DATA,historical_safe_margin):
        file_data = {}
        file_data_filtered = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        file_data_transformed = box_cox_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        nabla_frame = calculate_nabla(merged_with_safe_margin, TESTING_DATA.keys())
        residual_frame = calculate_ruc(nabla_frame, TESTING_DATA.keys())
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        ruc_with_safe_margin = pd.merge(merged_with_safe_margin, residual_frame, on=['day'])
        ruc_with_safe_margin.to_csv("Test_RUC_Benign.csv")

    def testing_attacked_threshold(self, TESTING_DATA,historical_safe_margin,
                                   max_threshold_org,min_threshold_org,
                                   max_threshold_att,min_threshold_att,ro_mal,del_avg):
        file_data = {}
        file_data_filtered = {}
        system_detected_unsafe =[]
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            # file_data[key] = file_data[key][(file_data[key][['use']] > 0).all(axis=1)]
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        if(float(del_avg)!=float(0) and float(ro_mal) !=float(0)):
            # print("Attack Start Day: ", 91," end day: ",181)
            additive_attack_data, number_of_compromised_meter = perform_deductive_attack(file_data_filtered,
                                                                                        ro_mal,
                                                                                        del_avg,
                                                                                        START_DATE_TEST,
                                                                                        END_DATE_TEST)
            for key in additive_attack_data.keys():
                # additive_attack_data_copied[key] = additive_attack_data[key][additive_attack_data[key]['use'] > 0]
                additive_attack_data[key]['use'].loc[additive_attack_data[key]['use'] <= 0] = 10
                # additive_attack_data_copied[key] = additive_attack_data[key]
            file_data_transformed = box_cox_transformation(additive_attack_data)
        else:
            print("Executing for unperturbed case")
            file_data_transformed = box_cox_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        # df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        # print((merged_with_safe_margin['ratio2016'].max(),' ',merged_with_safe_margin['ratio2016'].min()))
        tier1_anomaly = 0
        tier2_for_att = 0
        tier2_for_org = 0
        # max = float(-1.18973149536e+4932)
        # min = float(1.18973149536e+4932)
        # print(merged_with_safe_margin)
        nabla_frame = calculate_nabla(merged_with_safe_margin,TESTING_DATA.keys())
        # for row in nabla_frame.itertuples():
        #     print(getattr(row,"nabla2016"))
        residual_frame = calculate_ruc(nabla_frame, TESTING_DATA.keys())
        # merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day', 'month'])
        merged_with_safe_margin = pd.merge(df_merged, historical_safe_margin, on=['day'])
        # print((merged_with_safe_margin['ratio2016'].max(),' ',merged_with_safe_margin['ratio2016'].min()))
        ruc_with_safe_margin = pd.merge(merged_with_safe_margin, residual_frame, on=['day'])
        ruc_with_safe_margin.to_csv("Test_RUC_K_mad_2.0_D150_M4M6.csv")
        first_detected_org = 0
        first_detected_att = 0
        false_alarm_tier2_org = 0
        false_alarm_tier2_att = 0
        # ruc_with_safe_margin.to_csv("testing_residual_2016_deductive_attack_del50_romal_30.csv")

        for row in ruc_with_safe_margin.itertuples():
            if not(getattr(row,"ratio2016") <= getattr(row,'margin_high') and
                   getattr(row,'ratio2016') >= getattr(row,'margin_low')):
                tier1_anomaly = tier1_anomaly + 1
                if float(getattr(row,"ruc2016"))!= float(0):
                    if(float(getattr(row,"ruc2016"))> float(max_threshold_org) or float(getattr(row,"ruc2016")) < float(min_threshold_org)):
                        # print("day",getattr(row, "day"),' ',getattr(row,"ruc2016"),' ', max_threshold_org,' ', min_threshold_org)
                        if(getattr(row,"day") > int(91) and getattr(row,"day")<int(181) ):
                            if int(tier2_for_org) == int(0):
                                first_detected_org = getattr(row, "day")
                            tier2_for_org = tier2_for_org +1
                        else:
                            false_alarm_tier2_org = false_alarm_tier2_org + 1
                    if (float(getattr(row,"ruc2016")) > float(max_threshold_att) or float(getattr(row,"ruc2016")) < float(min_threshold_att)):
                        # print("ruc: ",ruc," with max t: ",self.tmax_ruc," min t: ",self.tmin_ruc)
                        # print("day",getattr(row, "day"),' ',getattr(row,"ruc2016"),' ',max_threshold_att,' ',min_threshold_att)
                        if (getattr(row,"day") > int(91) and getattr(row,"day")<int(181)):
                            if int(tier2_for_att) == int(0) :
                                first_detected_att = getattr(row, "day")
                            tier2_for_att = tier2_for_att +1
                        else:
                            false_alarm_tier2_att = false_alarm_tier2_att + 1
                        system_detected_unsafe.append(getattr(row,"day"))

        print("Tier 1 Detection: ",tier1_anomaly,"\n",
              "Tier 2 Detection for altered threshold: ",tier2_for_att,"\n",
              "Tier 2 Detection for Org Threshold: ",tier2_for_org,
              "First Detected org: ",first_detected_org,
              "First Detected atta: ",first_detected_att)
        # return max, min, tier2_for_org,tier2_for_att, first_detected_org,first_detected_att
        return tier2_for_org,tier2_for_att, first_detected_org,first_detected_att,false_alarm_tier2_org,false_alarm_tier2_att
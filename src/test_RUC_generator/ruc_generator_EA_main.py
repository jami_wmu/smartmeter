#First Calculate the original safemargin from the training set
#then use the safe margin to test the new data year 2016
# from src.main.existing_defese import ExistingDefense
from src.test_RUC_generator.ruc_generator_EA import Test_Threshold
# from src.main.poisoning_on_existing_defense import PoisoningOnExistingDefense
from src.utility.constant import *
from tqdm import tqdm

#original Limit
#{'kappa': 2.0, 'upper_limit': 0.0075, 'lower_limit': -0.017499999999999998}
# MAX Lower Distance for all ROMALs
#      lower_distance  lower_threshold  ro_mal  del_avg
# 511           0.235          -0.2525     0.4     3000
# MAX Upper Distance for all ROMALs
#       upper_distance  upper_threshold  ro_mal  del_avg
# 399           0.0575            0.065     0.6      750
# 1154          0.0575            0.065     0.6      800
import pandas as pd
import json
def main1():
    test_process = Test_Threshold()
    #region RUC Evasion
    # df = pd.read_csv('../main/poisoning/safe_margin_ded/safe_margin_del150_romal_0.3_type_ded.csv')
    df = pd.read_csv('safe_margin_TDSC.csv')
    # test_process.testing_RUC_Benign(TESTING_DATA,df)
    # test_process.testing_RUC(TESTING_DATA, df,0.3, 150,100,'ded')
    # print("Generating SA ADD")
    # for del_te in DEL_AVG_ARRAY_ADD_TE:
    #     for ro_mal in RO_MAL_ARRAY:
    #         test_process.testing_RUC_TE(TESTING_DATA,df,ro_mal,del_te,'add')
    # print("Generating SA Ded")
    #
    # for del_te in tqdm(DEL_AVG_ARRAY_DED_TE,desc="Te Progress: "):
    #     for ro_mal in RO_MAL_ARRAY:
    #         test_process.testing_RUC_TE(TESTING_DATA, df, ro_mal, del_te, 'ded')
    #endregion
    # region sensor level poisoning and evasion
    # print("Generating RA /ADD")
    #
    # for del_TR in DEL_AVG_ARRAY_ADD:
    #     for romal in RO_MAL_ARRAY:
    #         df = pd.read_csv('../main/poisoning/safe_margin_add/safe_margin_del'+str(del_TR)+
    #                          '_romal_'+str(romal)+'_type_add.csv')
    #         for del_TE in DEL_AVG_ARRAY_ADD_TE:
    #             # we could have varied romal as well but we did not so the test and the training
    #             # compromised meters are same which is not actually a correct way
    #             test_process.testing_RUC(TESTING_DATA, df,romal, del_TR,del_TE,'add')
    print("Generating RA ded")
    for del_TR in  tqdm(DEL_AVG_ARRAY_DED,desc="TR Progress: "):
        for romal in RO_MAL_ARRAY:
            df = pd.read_csv('../main/poisoning/safe_margin_ded/safe_margin_del'+str(del_TR)+
                             '_romal_'+str(romal)+'_type_ded.csv')
            for del_TE in DEL_AVG_ARRAY_DED_TE:
                test_process.testing_RUC(TESTING_DATA, df,romal, del_TR,del_TE,'ded')
    # endregion


    #region Not Nedded
    # for davg in DEL_AVG_ARRAY_DED_TR:
    #     for ro in RO_MAL_ARRAY:
    #         df = pd.read_csv('../main/safe_margin_all_RO_DEL_RA/safe_margin_del'+
    #                          str(davg)+'_romal_'+str(ro)+'.csv')
    #         for delavg in DEL_AVG_ARRAY_DED_TE:
    #             test_process.testing_RUC(TESTING_DATA, df,ro, delavg,davg)
    #end region
main1()


#this file does two thing calculate impact of undetected attack during testing
#check how much evasion is possible from the two limits (Taumax and Taumin) we calculated from training
import json
import math
import matplotlib.pyplot as plt
import pandas as pd
from src.utility.constant import *
from mpl_toolkits.mplot3d import Axes3D
#region month 4-6
with open("result_evasion_test_s_a_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_s_a_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_a_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_s_a_a_2016_4_6_List)
result_evasion_test_s_a_a_2016_4_6_frame['attack_type'] = SMALL_ADDITIVE
with open("result_evasion_test_s_a_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_a_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_a_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_s_a_a_2016_4_6_romal_1_List)
result_evasion_test_s_a_a_2016_4_6_romal_1_frame['attack_type'] = SMALL_ADDITIVE
result_evasion_test_s_a_a_2016_4_6_all = [result_evasion_test_s_a_a_2016_4_6_frame,result_evasion_test_s_a_a_2016_4_6_romal_1_frame]
result_evasion_test_s_a_a_2016_4_6_all_frames = pd.concat(result_evasion_test_s_a_a_2016_4_6_all)
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_a_a_2016_4_6_all_frames.tier2_for_att
# x = result_evasion_test_s_a_a_2016_4_6_all_frames.del_avg
# y = result_evasion_test_s_a_a_2016_4_6_all_frames.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_a_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_a_a_2016_4_6_all_frames.tier2_for_org
# x = result_evasion_test_s_a_a_2016_4_6_all_frames.del_avg
# y = result_evasion_test_s_a_a_2016_4_6_all_frames.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_a_a org')
# plt.show()
with open("result_evasion_test_l_a_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_l_a_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_a_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_l_a_a_2016_4_6_List)
result_evasion_test_l_a_a_2016_4_6_frame['attack_type'] = LARGE_ADDITIVE
with open("result_evasion_test_l_a_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_l_a_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_a_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_l_a_a_2016_4_6_romal_1_List)
result_evasion_test_l_a_a_2016_4_6_romal_1_frame['attack_type'] = LARGE_ADDITIVE
result_evasion_test_l_a_a_2016_4_6_all = [result_evasion_test_l_a_a_2016_4_6_frame,result_evasion_test_l_a_a_2016_4_6_romal_1_frame]
result_evasion_test_l_a_a_2016_4_6_all_frames = pd.concat(result_evasion_test_l_a_a_2016_4_6_all)
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_a_a_2016_4_6_all_frames.tier2_for_att
# x = result_evasion_test_l_a_a_2016_4_6_all_frames.del_avg
# y = result_evasion_test_l_a_a_2016_4_6_all_frames.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_a_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_a_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_l_a_a_2016_4_6_frame.del_avg
# y = result_evasion_test_l_a_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_a_a org')
# plt.show()
with open("result_evasion_test_s_d_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_s_d_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_d_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_s_d_a_2016_4_6_List)
result_evasion_test_s_d_a_2016_4_6_frame['attack_type'] = SMALL_DEDUCTIVE
with open("result_evasion_test_s_d_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_d_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_d_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_s_d_a_2016_4_6_romal_1_List)
result_evasion_test_s_d_a_2016_4_6_romal_1_frame['attack_type'] = SMALL_DEDUCTIVE
result_evasion_test_s_d_a_2016_4_6_all = [result_evasion_test_s_d_a_2016_4_6_frame,result_evasion_test_s_d_a_2016_4_6_romal_1_frame]
result_evasion_test_s_d_a_2016_4_6_all_frames = pd.concat(result_evasion_test_s_d_a_2016_4_6_all)
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_d_a_2016_4_6_all_frames.tier2_for_att
# x = result_evasion_test_s_d_a_2016_4_6_all_frames.del_avg
# y = result_evasion_test_s_d_a_2016_4_6_all_frames.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_d_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_d_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_s_d_a_2016_4_6_frame.del_avg
# y = result_evasion_test_s_d_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_d_a org')
# plt.show()
with open("result_evasion_test_l_d_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_l_d_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_d_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_l_d_a_2016_4_6_List)
result_evasion_test_l_d_a_2016_4_6_frame['attack_type'] = LARGE_DEDUCTIVE
with open("result_evasion_test_l_d_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_l_d_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_d_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_l_d_a_2016_4_6_romal_1_List)
result_evasion_test_l_d_a_2016_4_6_romal_1_frame['attack_type'] = LARGE_DEDUCTIVE

result_evasion_test_l_d_a_2016_4_6_all = [result_evasion_test_l_d_a_2016_4_6_frame,result_evasion_test_l_d_a_2016_4_6_romal_1_frame]
result_evasion_test_l_d_a_2016_4_6_all_frames = pd.concat(result_evasion_test_l_d_a_2016_4_6_all)
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_d_a_2016_4_6_frame.tier2_for_att
# x = result_evasion_test_l_d_a_2016_4_6_frame.del_avg
# y = result_evasion_test_l_d_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level  l_d_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_d_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_l_d_a_2016_4_6_frame.del_avg
# y = result_evasion_test_l_d_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_d_a org')
# plt.show()
with open("result_evasion_test_s_c_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_s_c_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_c_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_s_c_a_2016_4_6_List)
result_evasion_test_s_c_a_2016_4_6_frame['attack_type'] = SMALL_CAMOUFLAGE
with open("result_evasion_test_s_c_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_c_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_c_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_s_c_a_2016_4_6_romal_1_List)
result_evasion_test_s_c_a_2016_4_6_romal_1_frame['attack_type'] = SMALL_CAMOUFLAGE
result_evasion_test_s_c_a_2016_4_6_all = [result_evasion_test_s_c_a_2016_4_6_frame,result_evasion_test_s_c_a_2016_4_6_romal_1_frame]
result_evasion_test_s_c_a_2016_4_6_all_frames = pd.concat(result_evasion_test_s_c_a_2016_4_6_all)
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_c_a_2016_4_6_all_frames.tier2_for_att
# x = result_evasion_test_s_c_a_2016_4_6_all_frames.del_avg
# y = result_evasion_test_s_c_a_2016_4_6_all_frames.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_c_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_c_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_s_c_a_2016_4_6_frame.del_avg
# y = result_evasion_test_s_c_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_c_a org')
# plt.show()
with open("result_evasion_test_l_c_a_2016_4_6.txt") as source:
    json_source = source.read()
    result_evasion_test_l_c_a_2016_4_6_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_c_a_2016_4_6_frame = pd.json_normalize(result_evasion_test_l_c_a_2016_4_6_List)
result_evasion_test_l_c_a_2016_4_6_frame['attack_type'] = LARGE_CAMOUFLAGE
with open("result_evasion_test_l_c_a_2016_4_6_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_l_c_a_2016_4_6_romal_1_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_c_a_2016_4_6_romal_1_frame = pd.json_normalize(result_evasion_test_l_c_a_2016_4_6_romal_1_List)
result_evasion_test_l_c_a_2016_4_6_romal_1_frame['attack_type'] = LARGE_CAMOUFLAGE
result_evasion_test_l_c_a_2016_4_6_all = [result_evasion_test_l_c_a_2016_4_6_frame,result_evasion_test_l_c_a_2016_4_6_romal_1_frame]
result_evasion_test_l_c_a_2016_4_6_all_frames = pd.concat(result_evasion_test_s_c_a_2016_4_6_all)
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_c_a_2016_4_6_frame.tier2_for_att
# x = result_evasion_test_l_c_a_2016_4_6_frame.del_avg
# y = result_evasion_test_l_c_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_c_a')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_c_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_l_c_a_2016_4_6_frame.del_avg
# y = result_evasion_test_l_c_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_c_a org')
# plt.show()
frames_saa = [result_evasion_test_s_c_a_2016_4_6_all_frames,result_evasion_test_s_d_a_2016_4_6_all_frames,
              result_evasion_test_s_a_a_2016_4_6_all_frames,result_evasion_test_l_a_a_2016_4_6_all_frames,
              result_evasion_test_l_c_a_2016_4_6_all_frames,result_evasion_test_l_d_a_2016_4_6_all_frames]
concatenated_frame = pd.concat(frames_saa)
#I am considering 10 count is a normal but it depends on the situation
tier_2_evasion_limit = concatenated_frame[concatenated_frame.tier2_for_att == 0] #10 allowed threshold
tier_2_evasion_limit_org = concatenated_frame[concatenated_frame.tier2_for_org == 0] # 10 allowed threshold
print("Tier 2 can not detect when thresholds are altered through poison ")
#Number of meteres 192
num_of_meters = 192
result_impact_of_undetected = []
for row in tier_2_evasion_limit.itertuples():
    revenue_per_day = (row.del_avg * num_of_meters *row.ro_mal* E * 24) / 1000
    ipt_of_und_attk = revenue_per_day / 24
    temp = {"del_avg":row.del_avg,"ro_mal":row.ro_mal,"impact_of_undetected":ipt_of_und_attk,"attack_type":row.attack_type}
    result_impact_of_undetected.append(temp)
with open('result_impact_of_undetected.txt', 'w') as outfile:
    json.dump(result_impact_of_undetected, outfile)
grouped_df = tier_2_evasion_limit.groupby("attack_type")
for key,group in grouped_df:
    print("For Attack Type", key)
    print(group[['ro_mal','del_avg','tier2_for_att','attack_type']])
#endregion
#region month 7-9
with open("result_evasion_test_s_a_a_2016_7_9_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_a_a_2016_7_9_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_a_a_2016_7_9_frame = pd.json_normalize(result_evasion_test_s_a_a_2016_7_9_List)
result_evasion_test_s_a_a_2016_7_9_frame['attack_type'] = SMALL_ADDITIVE
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_a_a_2016_7_9_frame.tier2_for_att
# x = result_evasion_test_s_a_a_2016_7_9_frame.del_avg
# y = result_evasion_test_s_a_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_a_a m 7-9')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_a_a_2016_7_9_frame.tier2_for_org
# x = result_evasion_test_s_a_a_2016_7_9_frame.del_avg
# y = result_evasion_test_s_a_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_a_a org m 7-9')
# plt.show()
with open("result_evasion_test_l_a_a_2016_7_9_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_l_a_a_2016_7_9_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_a_a_2016_7_9_frame = pd.json_normalize(result_evasion_test_l_a_a_2016_7_9_List)
result_evasion_test_l_a_a_2016_7_9_frame['attack_type'] = LARGE_ADDITIVE
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_a_a_2016_7_9_frame.tier2_for_att
# x = result_evasion_test_l_a_a_2016_7_9_frame.del_avg
# y = result_evasion_test_l_a_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_a_a m 7-9')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_l_a_a_2016_7_9_frame.tier2_for_org
# x = result_evasion_test_l_a_a_2016_7_9_frame.del_avg
# y = result_evasion_test_l_a_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level l_a_a org m 7-9')
# plt.show()
with open("result_evasion_test_s_d_a_2016_7_9_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_d_a_2016_7_9_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_d_a_2016_7_9_frame = pd.json_normalize(result_evasion_test_s_d_a_2016_7_9_List)
result_evasion_test_s_d_a_2016_7_9_frame['attack_type'] = SMALL_DEDUCTIVE
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_d_a_2016_7_9_frame.tier2_for_att
# x = result_evasion_test_s_d_a_2016_7_9_frame.del_avg
# y = result_evasion_test_s_d_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_d_a m 7-9')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_d_a_2016_7_9_frame.tier2_for_org
# x = result_evasion_test_s_d_a_2016_7_9_frame.del_avg
# y = result_evasion_test_s_d_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_d_a org m 7-9')
# plt.show()
with open("result_evasion_test_s_c_a_2016_7_9_romal_1.txt") as source:
    json_source = source.read()
    result_evasion_test_s_c_a_2016_7_9_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_c_a_2016_7_9_frame = pd.json_normalize(result_evasion_test_s_c_a_2016_7_9_List)
result_evasion_test_s_c_a_2016_7_9_frame['attack_type'] = SMALL_CAMOUFLAGE

frames_saa = [result_evasion_test_s_a_a_2016_7_9_frame,result_evasion_test_l_a_a_2016_7_9_frame,
              result_evasion_test_s_d_a_2016_7_9_frame,result_evasion_test_s_c_a_2016_7_9_frame]
concatenated_frame = pd.concat(frames_saa)
#I am considering 10 count is a normal but it depends on the situation
tier_2_evasion_limit = concatenated_frame[concatenated_frame.tier2_for_att == 0] #10 allowed threshold
tier_2_evasion_limit_org = concatenated_frame[concatenated_frame.tier2_for_org == 0] # 10 allowed threshold
print("Tier 2 can not detect when thresholds are altered through poison ")
#Number of meteres 192
result_impact_of_undetected = []
for row in tier_2_evasion_limit.itertuples():
    revenue_per_day = (row.del_avg * num_of_meters *row.ro_mal* E * 24) / 1000
    ipt_of_und_attk = revenue_per_day / 24
    temp = {"del_avg":row.del_avg,"ro_mal":row.ro_mal,"impact_of_undetected":ipt_of_und_attk,"attack_type":row.attack_type}
    result_impact_of_undetected.append(temp)
with open('result_impact_of_undetected_m_7_9.txt', 'w') as outfile:
    json.dump(result_impact_of_undetected, outfile)
grouped_df = tier_2_evasion_limit.groupby("attack_type")
for key,group in grouped_df:
    print("For Attack Type", key)
    print(group[['ro_mal','del_avg','tier2_for_att','attack_type']])
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_c_a_2016_7_9_frame.tier2_for_att
# x = result_evasion_test_s_c_a_2016_7_9_frame.del_avg
# y = result_evasion_test_s_c_a_2016_7_9_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_c_a m 7-9')
# plt.show()
#
# fig = plt.figure()
# ax = fig.gca(projection="3d")
# z =  result_evasion_test_s_c_a_2016_4_6_frame.tier2_for_org
# x = result_evasion_test_s_c_a_2016_4_6_frame.del_avg
# y = result_evasion_test_s_c_a_2016_4_6_frame.ro_mal
# ax.plot_trisurf(x, y, z,edgecolors='grey')
# ax.set_xlabel('del_avg')
# ax.set_ylabel('ro_mal')
# ax.set_zlabel('Detection Level s_c_a org m 7-9')
# plt.show()
#endregion
#region Result After trimming the consumption above 6000
with open("result_evasion_test_s_a_a_2016_4_6_on_filtered_data.txt") as source:
    json_source = source.read()
    result_evasion_test_s_a_a_2016_4_6_on_filtered_data_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_a_a_2016_4_6_on_filtered_data_frame = pd.json_normalize(result_evasion_test_s_a_a_2016_4_6_on_filtered_data_List)
result_evasion_test_s_a_a_2016_4_6_on_filtered_data_frame['attack_type'] = SMALL_ADDITIVE
with open("result_evasion_test_l_a_a_2016_4_6_on_filtered_data.txt") as source:
    json_source = source.read()
    result_evasion_test_l_a_a_2016_4_6_on_filtered_data_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_a_a_2016_4_6_on_filtered_data_frame = pd.json_normalize(result_evasion_test_l_a_a_2016_4_6_on_filtered_data_List)
result_evasion_test_l_a_a_2016_4_6_on_filtered_data_frame['attack_type'] = LARGE_ADDITIVE
with open("result_evasion_test_s_d_a_2016_4_6_on_filtered_data.txt") as source:
    json_source = source.read()
    result_evasion_test_s_d_a_2016_4_6_on_filtered_data_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_d_a_2016_4_6_on_filtered_data_frame = pd.json_normalize(result_evasion_test_s_d_a_2016_4_6_on_filtered_data_List)
result_evasion_test_s_d_a_2016_4_6_on_filtered_data_frame['attack_type'] = SMALL_DEDUCTIVE

with open("result_evasion_test_s_c_a_2016_4_6_on_filtered_data.txt") as source:
    json_source = source.read()
    result_evasion_test_s_c_a_2016_4_6_on_filtered_data_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_c_a_2016_4_6_on_filtered_data_frame = pd.json_normalize(result_evasion_test_s_c_a_2016_4_6_on_filtered_data_List)
result_evasion_test_s_c_a_2016_4_6_on_filtered_data_frame['attack_type'] = SMALL_CAMOUFLAGE


frames_saa_4_6 = [result_evasion_test_s_a_a_2016_4_6_on_filtered_data_frame,result_evasion_test_l_a_a_2016_4_6_on_filtered_data_frame,
              result_evasion_test_s_d_a_2016_4_6_on_filtered_data_frame,result_evasion_test_s_c_a_2016_4_6_on_filtered_data_frame]
concatenated_frame_4_6 = pd.concat(frames_saa_4_6)
#I am considering 10 count is a normal but it depends on the situation
tier_2_evasion_limit_4_6 = concatenated_frame_4_6[concatenated_frame_4_6.tier2_for_att == 0] #10 allowed threshold
tier_2_evasion_limit_org_4_6 = concatenated_frame_4_6[concatenated_frame_4_6.tier2_for_org == 0] # 10 allowed threshold
print("Tier 2 on Filtered Data can not detect when thresholds are altered through poison ")

result_impact_of_undetected_4_6 = []
for row in tier_2_evasion_limit_4_6.itertuples():
    revenue_per_day = (row.del_avg * num_of_meters *row.ro_mal* E * 24) / 1000
    ipt_of_und_attk = revenue_per_day / 24
    temp = {"del_avg":row.del_avg,"ro_mal":row.ro_mal,"impact_of_undetected":ipt_of_und_attk,"attack_type":row.attack_type}
    result_impact_of_undetected_4_6.append(temp)
with open('result_impact_of_undetected_m_4_6.txt', 'w') as outfile:
    json.dump(result_impact_of_undetected_4_6, outfile)

grouped_df = tier_2_evasion_limit_4_6.groupby("attack_type")
for key,group in grouped_df:
    print("For Attack Type ", key," in m 4-6")
    grouped_by_romal = group.groupby("ro_mal")
    for key1, group1 in grouped_by_romal:
        print("For romal: ",key1," max delavg:", group1['del_avg'].max() )
    # print(group[['ro_mal','del_avg','tier2_for_att','attack_type']])
#endregion

#region Result for log transformation
with open("result_evasion_test_s_a_a_2016_4_6_on_log_transformed.txt") as source:
    json_source = source.read()
    result_evasion_test_s_a_a_2016_4_6_on_log_transformed_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_a_a_2016_4_6_on_log_transformed_frame = pd.json_normalize(result_evasion_test_s_a_a_2016_4_6_on_log_transformed_List)
result_evasion_test_s_a_a_2016_4_6_on_log_transformed_frame['attack_type'] = SMALL_ADDITIVE
with open("result_evasion_test_l_a_a_2016_4_6_on_log_transformed.txt") as source:
    json_source = source.read()
    result_evasion_test_l_a_a_2016_4_6_on_log_transformed_List = json.loads('[{}]'.format(json_source))
result_evasion_test_l_a_a_2016_4_6_on_log_transformed_frame = pd.json_normalize(result_evasion_test_l_a_a_2016_4_6_on_log_transformed_List)
result_evasion_test_l_a_a_2016_4_6_on_log_transformed_frame['attack_type'] = LARGE_ADDITIVE
with open("result_evasion_test_s_d_a_2016_4_6_on_log_transformed.txt") as source:
    json_source = source.read()
    result_evasion_test_s_d_a_2016_4_6_on_log_transformed_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_d_a_2016_4_6_on_log_transformed_frame = pd.json_normalize(result_evasion_test_s_d_a_2016_4_6_on_log_transformed_List)
result_evasion_test_s_d_a_2016_4_6_on_log_transformed_frame['attack_type'] = SMALL_DEDUCTIVE

with open("result_evasion_test_s_c_a_2016_4_6_on_log_transformed.txt") as source:
    json_source = source.read()
    result_evasion_test_s_c_a_2016_4_6_on_log_transformed_List = json.loads('[{}]'.format(json_source))
result_evasion_test_s_c_a_2016_4_6_on_log_transformed_frame = pd.json_normalize(result_evasion_test_s_c_a_2016_4_6_on_log_transformed_List)
result_evasion_test_s_c_a_2016_4_6_on_log_transformed_frame['attack_type'] = SMALL_CAMOUFLAGE


frames_saa_4_6 = [result_evasion_test_s_a_a_2016_4_6_on_log_transformed_frame,result_evasion_test_l_a_a_2016_4_6_on_log_transformed_frame,
              result_evasion_test_s_d_a_2016_4_6_on_log_transformed_frame,result_evasion_test_s_c_a_2016_4_6_on_log_transformed_frame]
concatenated_frame_4_6 = pd.concat(frames_saa_4_6)
#I am considering 10 count is a normal but it depends on the situation
tier_2_evasion_limit_4_6 = concatenated_frame_4_6[concatenated_frame_4_6.tier2_for_att <5] #10 allowed threshold
tier_2_evasion_limit_org_4_6 = concatenated_frame_4_6[concatenated_frame_4_6.tier2_for_org <5] # 10 allowed threshold
print("Tier 2 on log transformation can not detect when thresholds are altered through poison ")

result_impact_of_undetected_4_6 = []
for row in tier_2_evasion_limit_4_6.itertuples():
    revenue_per_day = (row.del_avg * num_of_meters *row.ro_mal* E * 24) / 1000
    ipt_of_und_attk = revenue_per_day / 24
    temp = {"del_avg":row.del_avg,"ro_mal":row.ro_mal,"impact_of_undetected":ipt_of_und_attk,"attack_type":row.attack_type}
    result_impact_of_undetected_4_6.append(temp)
with open('result_impact_of_undetected_m_4_6.txt', 'w') as outfile:
    json.dump(result_impact_of_undetected_4_6, outfile)

grouped_df = tier_2_evasion_limit_4_6.groupby("attack_type")
for key,group in grouped_df:
    print("For Attack Type ", key," in m 4-6")
    grouped_by_romal = group.groupby("ro_mal")
    for key1, group1 in grouped_by_romal:
        print("For romal: ",key1," max delavg:", group1['del_avg'].max() )
    # print(group[['ro_mal','del_avg','tier2_for_att','attack_type']])
#endregion
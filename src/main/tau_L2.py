
from src.utility.common_functions_new import *
import numpy as np
tau_result_array = []

for del_avg in DEL_AVG_ARRAY_DED:
    for ro in RO_MAL_ARRAY:  # need to remove the ro from the code if want to use other training residual
        ruc_frame = pd.read_csv('poisoning/deductive_RUC/training_RUC_del_' + str(del_avg) +
                                '_romal_' + str(ro) + '_type_ded.csv')
        tau_max = calculateTmax_ruc_new_L2(ruc_frame,TRAINING_DATA.keys())  # non attacked standard limit max margin
        tau_min = calculateTmin_ruc_new_L2(ruc_frame,TRAINING_DATA.keys())  # non attacked standard limit min margin
        object_c = {"del_avg_tr": del_avg, 'ro_mal': ro, 'type': 'ded',
                    'tau_max': tau_max, 'tau_min': tau_min}
        tau_result_array.append(object_c)
tau_result_frame = pd.DataFrame(tau_result_array)
tau_result_frame.to_csv('RA_tau_L2_ded.csv')

tau_result_array = []
for del_avg in DEL_AVG_ARRAY_ADD:
    for ro in RO_MAL_ARRAY:  # need to remove the ro from the code if want to use other training residual
        ruc_frame = pd.read_csv('poisoning/additive_RUC/training_RUC_del_' + str(del_avg) +
                                '_romal_' + str(ro) + '_type_add.csv')
        tau_max = calculateTmax_ruc_new_L2(ruc_frame,TRAINING_DATA.keys())  # non attacked standard limit max margin
        tau_min = calculateTmin_ruc_new_L2(ruc_frame,TRAINING_DATA.keys())  # non attacked standard limit min margin
        object_c = {"del_avg_tr": del_avg, 'ro_mal': ro, 'type': 'add',
                    'tau_max': tau_max, 'tau_min': tau_min}
        tau_result_array.append(object_c)
tau_result_frame = pd.DataFrame(tau_result_array)
tau_result_frame.to_csv('RA_tau_L2_add.csv')
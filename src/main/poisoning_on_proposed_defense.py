import json
from functools import reduce
from datetime import datetime

from src.utility.common_functions import *
from src.utility.constant import *
import os as o
import matplotlib.pyplot as plt
class PoisoningOnProposedDefense:
    def __init__(self):
        self.historical_safe_margin = None
        self.tmax_mruc = None
        self.tmin_mruc = None
        self.number_of_compromised_meter = 0
        self.system_detected_unsafe = [] #track the days where false alarm happened

    def training(self, TRAINING_DATA,threshold,romal,delavg):
        #region read
        file_data = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
        additive_attack_data,self.number_of_compromised_meter = perform_additive_attack(file_data,romal,delavg)
        additive_attack_data_copied = {}
        for key in additive_attack_data.keys():
            additive_attack_data_copied[key] = additive_attack_data[key][additive_attack_data[key]['use'] >= 0]
        additive_attack_data_transformed = box_cox_transformation(additive_attack_data_copied)
        hm_am_ratio_frame_attacked = calculateAMAndHM(additive_attack_data_transformed)
        df_merged_attacked = reduce(lambda left, right: pd.merge(left, right,
                                                      on=['day', 'month']), hm_am_ratio_frame_attacked)
        df_merged_attacked['mean_ratio'] = df_merged_attacked[['ratio2014', 'ratio2015']].mean(axis=1)  # take mean of two ratio per day
        mean_absolute_deviation = df_merged_attacked['mean_ratio'].mad()
        df_merged_copied = df_merged_attacked.copy()
        safe_margin = calculate_safe_margin(df_merged_copied, mean_absolute_deviation,threshold)
        self.historical_safe_margin = safe_margin[['day', 'month', 'margin_low', 'margin_high']].copy()
        nabla_frame = calculate_nabla(df_merged_attacked,self.historical_safe_margin,TRAINING_DATA.keys())
        mrucFrame = inversePowerLaw(nabla_frame,TRAINING_DATA.keys())
        weightedRuc = weightForRuc(mrucFrame,TRAINING_DATA.keys())
        modifiedRuc = newModifiedRUC(mrucFrame, weightedRuc,TRAINING_DATA.keys())
        fig = modifiedRuc.plot(x="day", y=["mruc2014", "mruc2015"], kind="line",
                               xlim=(START, END),
                               ylim=(-.25,.25),
                               title = "Attacked on new defense With kappa "+threshold+" roaml "+romal+' and del avg: '+delavg)
        fig.set_xlabel("Days in Year")
        fig.set_ylabel("Residual Under Curve")
        image = fig.get_figure()
        image.savefig("ruc_kappa_" + threshold + "_romal_" + romal + "_del_" + delavg + ".png")
        self.tmax_mruc = calculateTmax_mruc(modifiedRuc, TRAINING_DATA.keys())
        self.tmin_mruc = calculateTmin_mruc(modifiedRuc, TRAINING_DATA.keys())
        print("tmax_mruc: ", self.tmax_mruc, " tmin_mruc: ", self.tmin_mruc )
        plt.show()

    def testing(self, TESTING_DATA):
        file_data = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
        file_data_transformed = box_cox_transformation(file_data) #when no attack is applied
        # file_data_transformed = box_cox_transformation(additive_attack_data_copied)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged,self.historical_safe_margin,on=['day','month'])
        tier1_anomaly = 0
        tier2_anomaly_mruc = 0
        for row in merged_with_safe_margin.itertuples():
            if not(getattr(row,"ratio2016")<=getattr(row,'margin_high') and
                   getattr(row,'ratio2016')>= getattr(row,'margin_low')):
                tier1_anomaly = tier1_anomaly + 1
                if (getattr(row,"ratio2016")> getattr(row,'margin_high')):
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_high')
                else:
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_low')
                # print ("ruc: ",ruc)
                if not ruc ==0:
                    if not (ruc <= self.tmax_mruc and ruc >= self.tmin_mruc):
                        tier2_anomaly_mruc = tier2_anomaly_mruc + 1
                        self.system_detected_unsafe.append(getattr(row,"day"))


        print("Number of detection of attack by tier 1: ",tier1_anomaly)
        print("Number of detection of attack by tier 2 mruc: ",tier2_anomaly_mruc)
        prev = 0
        detection_time_sum = 0
        number_of_fa = 0
        detection_time_avg = 0
        if not (len(self.system_detected_unsafe) == 0):
            for row in self.system_detected_unsafe:
                if row<60 or row > 273: # 60 means the start of the attack
                    number_of_fa = number_of_fa + 1
                    if(prev == 0):
                        prev = row
                    else:
                        print("days:", prev, " ",row)
                        detection_time_sum += row - prev
                        prev = row
            print("Total time sum: ", detection_time_sum," number of false alarms: ", number_of_fa)
            if not(number_of_fa == 0):
                detection_time_avg = detection_time_sum/number_of_fa
                print("detection time avg: ",detection_time_avg)
        reports_per_day = tier2_anomaly_mruc/DAYS_INA_YEAR
        revenue_per_day = (DEL_AVG *  self.number_of_compromised_meter * E * reports_per_day)/1000
        impact_of_undetected_attack = revenue_per_day/24
        print("impact of undetected attack: ", impact_of_undetected_attack)
        return detection_time_avg,impact_of_undetected_attack


def main():
    result=[]
    for delavg in DEL_AVG_ARRAY:
        for romal in RO_MAL_ARRAY:
            for threshold in KAPPA:
                main = PoisoningOnProposedDefense()
                main.training(TRAINING_DATA, threshold,romal,delavg)
                effective_time_fa, impact_of_undetected_attack = main.testing(TESTING_DATA)
                temp = {"del_avg":delavg,
                        "ro_mal":romal,
                        "kappa":threshold,
                        "upper_limit": main.tmax_mruc,
                        "lower_limit": main.tmin_mruc,
                        "effective_time_fa": effective_time_fa,
                        "impact_of_undetected_attack": impact_of_undetected_attack}
                result.append(temp)
    # now = datetime.now()
    # dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    with open('result_small_additive_attack.txt', 'w') as outfile:
        json.dump(result, outfile)
# main()

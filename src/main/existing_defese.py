import json
from functools import reduce
# import pylab as py
# from scipy.stats import norm
# import numpy as np
# import scipy.stats as stats
# import seaborn as sns; sns.set()
from src.utility.common_functions import *
from src.utility.constant import *
import os as o
import matplotlib.pyplot as plt
class ExistingDefense:
    def __init__(self):
        self.historical_safe_margin = None
        self.tMax_ruc = None
        self.tmin_ruc = None
        self.number_of_compromised_meter = 0
        self.system_detected_unsafe = [] #track the days where false alarm happened
        # self.residual_Frame = None

    def training(self, TRAINING_DATA,threshold):
        #region read
        file_data = {}
        file_data_filtered = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        #endregion
        file_data_transformed = box_cox_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df1 = hm_am_ratio_frame[0]
        df2 = hm_am_ratio_frame[1]

        # df_merged = reduce(lambda left, right: pd.merge(left, right,on=['day']), hm_am_ratio_frame)
        df_merged = pd.merge(df1,df2,on=['month','day'])
        # df_merged.to_csv('ratio_2.csv')
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1) #take mean of two ratio per day
        # print(df_merged)
        all_ratio = pd.concat([df_merged['ratio2014'],df_merged['ratio2015']])
        # print(all_ratio)
        std_from_mean_ratio = df_merged['mean_ratio'].std()
        std = all_ratio.std() #This std deviation is correct one to use however
        mad = all_ratio.mad() #This std deviation is correct one to use however
        # keeping it here unused as old implementation did not used this
        print(" std_from_mean_ratio: ",std_from_mean_ratio," std: ",std," mad: ",mad)
        # df_merged_copied = df_merged.copy()
        safe_margin = calculate_safe_margin(df_merged,std,threshold)
        self.historical_safe_margin = safe_margin[['day','margin_low','margin_high']].copy()
        # self.historical_safe_margin.to_csv(r'safe_margin_kappa_varied'+str(threshold)+'.csv')
        nabla_frame_no_attack = calculate_nabla(df_merged, TRAINING_DATA.keys())
        # print(nabla_frame_no_attack[nabla_frame_no_attack['nabla2014']!=0])
        # print(nabla_frame_no_attack[nabla_frame_no_attack['nabla2015']!=0])
        residual_frame_no_attack = calculate_ruc(nabla_frame_no_attack, TRAINING_DATA.keys())
        # print(residual_frame_no_attack['ruc2014'].max(),' ',residual_frame_no_attack['ruc2015'].max())
        # residual_frame_no_attack.to_csv(r'training_residual_mad.csv')
        self.tmax_ruc = calculateTmax_ruc(residual_frame_no_attack, TRAINING_DATA.keys())# non attacked standard limit max margin
        self.tmin_ruc = calculateTmin_ruc(residual_frame_no_attack, TRAINING_DATA.keys()) # non attacked standard limit min margin
        print(self.tmin_ruc, self.tmax_ruc)
        plt.show()

    def testing(self, TESTING_DATA):
        file_data = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
        file_data_transformed = box_cox_transformation(file_data) #when no attack is applied
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged,self.historical_safe_margin,on=['day','month'])
        tier1_anomaly = 0
        tier2_anomaly_ruc = 0
        for row in merged_with_safe_margin.itertuples():
            if not(getattr(row,"ratio2016")<=getattr(row,'margin_high') and
                   getattr(row,'ratio2016')>= getattr(row,'margin_low')):
                tier1_anomaly = tier1_anomaly + 1
                if (getattr(row,"ratio2016")> getattr(row,'margin_high')):
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_high')
                else:
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_low')
                # print ("ruc: ",ruc)
                if not ruc ==0:
                    if not (ruc<= self.tmax_ruc and ruc>= self.tmin_ruc):
                        # print("ruc: ",ruc," with max t: ",self.tmax_ruc," min t: ",self.tmin_ruc)
                        tier2_anomaly_ruc = tier2_anomaly_ruc +1
                        self.system_detected_unsafe.append(getattr(row,"day"))
        print("Number of detection of attack by tier 1: ",tier1_anomaly,
              " number of detection of attack by tier 2 ruc: ",tier2_anomaly_ruc)
        prev = 0
        detection_time_sum = 0
        detection_time_avg = 0
        if not (len(self.system_detected_unsafe) == 0):
            for row in self.system_detected_unsafe:
                if(prev == 0):
                    prev = row
                else:
                    # print("days:", prev, " ",row)
                    detection_time_sum += row - prev
                    prev = row
            print("Total time sum: ", detection_time_sum," number of false alarms: ", len(self.system_detected_unsafe))
            detection_time_avg = detection_time_sum/len(self.system_detected_unsafe)
            print("detection time avg: ",detection_time_avg)
        reports_per_day = tier2_anomaly_ruc/DAYS_INA_YEAR
        revenue_per_day = (DEL_AVG *  self.number_of_compromised_meter * E * reports_per_day)/1000
        impact_of_undetected_attack = revenue_per_day/24
        print("impact of undetected attack: ", impact_of_undetected_attack)
        return detection_time_avg,impact_of_undetected_attack
def main():
    result = []
    main = ExistingDefense()
    main.training(TRAINING_DATA, 2.00)
    # effective_time_fa, impact_of_undetected_attack = main.testing_on_posoning(TESTING_DATA)
    temp = {
        "kappa": 2.00,
        "upper_limit": main.tmax_ruc,
        "lower_limit": main.tmin_ruc,
    }
    result.append(temp)
    print(temp)
    # for kappa in KAPPA:
    #     main.training(TRAINING_DATA,kappa)
    # # effective_time_fa, impact_of_undetected_attack = main.testing_on_posoning(TESTING_DATA)
    #     temp = {
    #             "kappa": kappa,
    #             "upper_limit":main.tmax_ruc,
    #             "lower_limit":main.tmin_ruc,
    #             }
    #     result.append(temp)
    #     print(temp)
    # now = datetime.now()
    # dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    # with open('standard_limit_with_diff_kappa', 'w') as outfile:
    #     json.dump(result, outfile)
# main()

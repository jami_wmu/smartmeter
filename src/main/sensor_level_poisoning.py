from src.utility.common_functions_new import *
from src.utility.constant_new import *
import os as o
class SensorLevel:
    def __init__(self):
        self.historical_safe_margin = None
        self.tMax_ruc = None
        self.tmin_ruc = None

    def training(self, TRAINING_DATA,threshold,ro_mal,del_avg,type,start_time,end_time):
        file_data = {}
        file_data_filtered = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data_new(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        if(type=='ded'):
            additive_attack_data,N_compromised = perform_deductive_attack(file_data_filtered,ro_mal,del_avg,start_time,end_time)
        else:
            additive_attack_data, N_compromised = perform_additive_attack(file_data_filtered, ro_mal, del_avg, start_time, end_time)

        for key in additive_attack_data.keys():
            additive_attack_data[key]['use'].loc[additive_attack_data[key]['use'] <= 0] = 10
        file_data_transformed = box_cox_transformation_new(additive_attack_data)
        hm_am_ratio_frame = calculateAMAndHM_new(file_data_transformed)
        df1 = hm_am_ratio_frame[0]
        df2 = hm_am_ratio_frame[1]
        df_merged = pd.merge(df1,df2,on=['month','day'])
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1) #take mean of two ratio per day
        all_ratio = pd.concat([df_merged['ratio2014'],df_merged['ratio2015']])
        # all_ratio.to_csv('ratio_ro30_del150.csv')
        # std = all_ratio.std() #This std deviation is correct one to use however
        mad = all_ratio.mad() #This std deviation is correct one to use however
        # print(" std_from_mean_ratio: ",std_from_mean_ratio," std: ",std," mad: ",mad)
        safe_margin = calculate_safe_margin_new(df_merged, mad, threshold)
        self.historical_safe_margin = safe_margin[['day','margin_low','margin_high']].copy()
        if(type == 'ded'):
            self.historical_safe_margin.to_csv('safe_margin_del'+str(del_avg)+"_romal_"+str(ro_mal)+"_type_"+type+'.csv')
        else:
            self.historical_safe_margin.to_csv('safe_margin_del'+str(del_avg)+"_romal_"+str(ro_mal)+"_type_"+type+'.csv')

        nabla_frame_no_attack = calculate_nabla_new(df_merged, TRAINING_DATA.keys())
        # nabla_frame_no_attack.to_csv('nabla_a.csv')
        # print(nabla_frame_no_attack[( nabla_frame_no_attack['nabla2014'] != 0 )  ])
        residual_frame_no_attack = calculate_ruc_new(nabla_frame_no_attack, TRAINING_DATA.keys())

        if (type == 'ded'):
            residual_frame_no_attack.to_csv(
                'poisoning/deductive_RUC/training_RUC_del_' + str(del_avg) + "_romal_" + str(ro_mal) + "_type_" + type + '.csv')
        else:
            residual_frame_no_attack.to_csv(
                'poisoning/additive_RUC/training_RUC_del_' + str(del_avg) + "_romal_" + str(ro_mal) + "_type_" + type + '.csv')

        self.tmax_ruc = calculateTmax_ruc_new(residual_frame_no_attack, TRAINING_DATA.keys())# non attacked standard limit max margin
        self.tmin_ruc = calculateTmin_ruc_new(residual_frame_no_attack, TRAINING_DATA.keys()) # non attacked standard limit min margin
        print(self.tmin_ruc, self.tmax_ruc)
def main():
    result = []
    main = SensorLevel()
    # main.training(TRAINING_DATA, 2.0, .30, 150,'ded')
    i = 0
    for del_avg in DEL_AVG_ARRAY_DED:
        for ro in RO_MAL_ARRAY:
            start_time = START_DATE_ARR[i % 6]
            end_time = END_DATE_ARR[i % 6]
            main.training(TRAINING_DATA, 2.0,ro,del_avg,'ded',start_time,end_time)
            temp = {
                "del_avg": del_avg,
                "ro_mal":ro,
                "tau_max": main.tmax_ruc,
                "tau_min": main.tmin_ruc,
            }
            result.append(temp)
            i = i + 1

    print(result)
    standard_limit = pd.DataFrame(result)
    standard_limit.to_csv('poisoning/deductive_RUC/deductive_posoning_tau_11_08_21.csv')

    result = []
    i=0
    for del_avg in DEL_AVG_ARRAY_ADD:
        for ro in RO_MAL_ARRAY:
            start_time = START_DATE_ARR[i%6]
            end_time = END_DATE_ARR[i%6]
            main.training(TRAINING_DATA, 2.0,ro,del_avg,'add',start_time,end_time)
            temp = {
                "del_avg": del_avg,
                "ro_mal":ro,
                "tau_max": main.tmax_ruc,
                "tau_min": main.tmin_ruc,
            }
            result.append(temp)
            i = i + 1

    print(result)
    standard_limit = pd.DataFrame(result)
    standard_limit.to_csv('poisoning/additive_RUC/additive_posoning_tau_11_08_21.csv')
# main()

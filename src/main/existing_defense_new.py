from src.utility.common_functions_new import *
from src.utility.constant_new import *
import os as o
import matplotlib.pyplot as plt
class ExistingDefense:
    def __init__(self):
        self.historical_safe_margin = None
        self.tMax_ruc = None
        self.tmin_ruc = None

    def training(self, TRAINING_DATA,threshold):
        file_data = {}
        file_data_filtered = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data_new(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <= 6000]
        file_data_transformed = box_cox_transformation_new(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM_new(file_data_transformed)
        df1 = hm_am_ratio_frame[0]
        df2 = hm_am_ratio_frame[1]
        df_merged = pd.merge(df1,df2,on=['month','day'])
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1) #take mean of two ratio per day
        # plt.plot(df_merged[df_merged['day']<80]['ratio2014'],label="2014")
        # plt.plot(df_merged[df_merged['day']<80]['ratio2015'],label="2015")
        # plt.plot(df_merged[df_merged['day']<80]['mean_ratio'])
        plt.show()
        all_ratio = pd.concat([df_merged['ratio2014'],df_merged['ratio2015']])
        # all_ratio.to_csv('ratio_benign.csv')
        # std_from_mean_ratio = df_merged['mean_ratio'].std()
        # std = all_ratio.std() #This std deviation is correct one to use however
        mad = all_ratio.mad() #This std deviation is correct one to use however
        # print(" std_from_mean_ratio: ",std_from_mean_ratio," std: ",std," mad: ",mad)
        safe_margin = calculate_safe_margin_new(df_merged, mad, threshold)
        self.historical_safe_margin = safe_margin[['day','margin_low','margin_high']].copy()
        self.historical_safe_margin.to_csv('safe_margin_mad_'+str(threshold)+'.csv')
        nabla_frame_no_attack = calculate_nabla_new(df_merged, TRAINING_DATA.keys())
        # print(nabla_frame_no_attack[nabla_frame_no_attack['nabla2014']!=0])
        # print(nabla_frame_no_attack[nabla_frame_no_attack['nabla2015']!=0])

        # nabla_frame_no_attack.to_csv('nabla.csv')
        residual_frame_no_attack = calculate_ruc_new(nabla_frame_no_attack, TRAINING_DATA.keys())
        # print(residual_frame_no_attack[residual_frame_no_attack['ruc2014'] != 0])
        # print(residual_frame_no_attack[residual_frame_no_attack['ruc2015'] != 0])
        residual_frame_no_attack.to_csv('training_RUC_mad_'+str(threshold)+'csv')
        self.tmax_ruc = calculateTmax_ruc_new_L2(residual_frame_no_attack, TRAINING_DATA.keys())# non attacked standard limit max margin
        self.tmin_ruc = calculateTmin_ruc_new_L2(residual_frame_no_attack, TRAINING_DATA.keys()) # non attacked standard limit min margin
        print(self.tmin_ruc, self.tmax_ruc)

def main():
    result = []
    main = ExistingDefense()
    # main.training(TRAINING_DATA, 2.0)
    for k in KAPPA:
        main.training(TRAINING_DATA, k)
        # effective_time_fa, impact_of_undetected_attack = main.testing_on_posoning(TESTING_DATA)
        temp = {
            "kappa": k,
            "upper_limit": main.tmax_ruc,
            "lower_limit": main.tmin_ruc,
        }
        result.append(temp)
    # print(temp)
    result_frame = pd.DataFrame(result)
    result_frame.to_csv('standard_limit_12_09_21.csv')
    print(result)
main()

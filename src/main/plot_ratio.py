import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
SMALL_SIZE = 12
MEDIUM_SIZE = 18
BIGGER_SIZE = 26
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    #

ratio_attack = pd.read_csv('others/ratio_ro30_del150.csv')
ratio_benign = pd.read_csv('others/ratio_benign.csv')
plot_ratio_attack = ratio_attack[ratio_attack.index< int(250)]
plot_ratio_benign = ratio_benign[ratio_benign.index < int(250)]
print(len(plot_ratio_attack))
arr = np.arange(len(plot_ratio_attack))
plt.figure()
# plt.rcParams["figure.figsize"] = (12, 8)
matplotlib.rcParams['savefig.dpi'] = 200

# plt.tight_layout()

plt.plot(arr,plot_ratio_attack['ratio'],label = 'Poisoned',linestyle ='--',marker = '<',markevery = 10)
plt.plot(arr,plot_ratio_benign['ratio'],label = 'Benign',linestyle ='-.',marker = '>',markevery = 10)
plt.ylabel('Ratio')
plt.xlabel('Days')
# loc = matplotlib.ticker.LinearLocator(numticks=8)
# plt.gca().xaxis.set_major_locator(loc)
plt.legend()
plt.savefig('ro30_del150_ratio.png',bbox_inches="tight")

plt.show()

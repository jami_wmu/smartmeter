#REMARK : Low additive_RUC attack increases the ratio because of inherent right skewness
# however if we remove all datapoints will there still be rightskewness as we are removing all higher values

import json
from functools import reduce
from datetime import datetime
import seaborn as sns; sns.set()
from src.utility.common_functions import *
from src.utility.constant import *
import os as o
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
class PoisoningOnExistingDefense:
    def __init__(self):
        self.historical_safe_margin = None
        self.tMax_ruc = None
        self.tmin_ruc = None
        self.number_of_compromised_meter = 0
        self.system_detected_unsafe = []
        # self.residual_Frame = None

    def training_on_posoning(self,TRAINING_DATA,threshold,romal,delavg_s,del_avg_m):
        file_data = {}
        file_data_filtered = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
            file_data_filtered[key] = file_data[key][file_data[key]['use'] <=6000]
        #First Small Scale Additive Attack to influence the uppler limit
        file_data_copied = {}
        for key in file_data_filtered.keys():
            file_data_copied[key] = file_data_filtered[key].copy()
        #region original Safe margin
        file_data_transformed = box_cox_transformation(file_data_filtered)
        # file_data_transformed = log_transformation(file_data_filtered)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        # hm_am_ratio_frame = calculateAMAndHM(winsorized_hm_am_ratio_frame)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame)
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1)  # take mean of two ratio per day
        standard_deviation = df_merged['mean_ratio'].std()
        df_merged_copied = df_merged.copy()
        safe_margin = calculate_safe_margin(df_merged_copied, standard_deviation, threshold)
        self.historical_safe_margin = safe_margin[['day', 'month', 'margin_low', 'margin_high']].copy()
        #endregion
        additive_attack_data,self.number_of_compromised_meter = perform_additive_attack(file_data_copied,romal,delavg_s,START_DATE,END_DATE)
        additive_attack_data_copied = {}
        for key in additive_attack_data.keys():
            additive_attack_data[key]['use'].loc[additive_attack_data[key]['use'] <= 0] = 10
            additive_attack_data_copied[key] = additive_attack_data[key]
        additive_attack_data_transformed = box_cox_transformation(additive_attack_data_copied)
        hm_am_ratio_frame_attacked = calculateAMAndHM(additive_attack_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame_attacked)
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1)  # take mean of two ratio per day
        df_merge_safe_margin = pd.merge(df_merged,self.historical_safe_margin,on='day')
        nabla_frame = calculate_nabla(df_merge_safe_margin, TRAINING_DATA.keys())
        print(nabla_frame)
        residual_frame = calculate_ruc(nabla_frame, TRAINING_DATA.keys())
        print(residual_frame['ruc2014'].max(),' ',residual_frame['ruc2015'].max())
        self.tmax_ruc = calculateTmax_ruc(residual_frame, TRAINING_DATA.keys())  # commented on 08/27/2020
        self.tmin_ruc = calculateTmin_ruc(residual_frame, TRAINING_DATA.keys())
        print("After attack tmax_ruc: ", self.tmax_ruc, " tmin_ruc: ", self.tmin_ruc)
        plt.show()

def main():
    main = PoisoningOnExistingDefense()
    main.training_on_posoning(TRAINING_DATA, 2.00, .4, 1000,1000)

# main()
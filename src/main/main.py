#This is the intial coding file we had
from functools import reduce

from src.utility.common_functions import *
from src.utility.constant import *
import os as o
import matplotlib.pyplot as plt
class Main:
    def __init__(self):
        self.historical_safe_margin = None
        self.tMax_ruc = None
        self.tmin_ruc = None
        self.tmax_mruc = None
        self.tmin_mruc = None
        self.number_of_compromised_meter = 0
        self.system_detected_unsafe = [] #track the days where false alarm happened
        pass

    def training(self, TRAINING_DATA):
        #region read
        file_data = {}
        for key in TRAINING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TRAINING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object
        #endregion
        #region No attack Stat
        # print(file_data)
        file_data_copied = {}
        for key in file_data.keys():
            file_data_copied[key] = file_data[key].copy()
        #region barplot
        # plt.hist(file_data['2014'].use, density=True)  # `density=False` would make counts
        # plt.ylabel('Use')
        # plt.xlabel('Date')
        # plt.show()
        #endregion
        file_data_transformed = box_cox_transformation(file_data_copied)
        # region barplot
        # plt.hist(file_data_transformed['2014'].use, density=True)  # `density=False` would make counts
        # plt.ylabel('Use Trans')
        # plt.xlabel('Date')
        # plt.show()
        # endregion
        # hm_am_ratio_frame = calculate_AM_HM_ratio(file_data_transformed) # it is a different function
        #winsorize the transformed data
        # winsorized_hm_am_ratio_frame = winsorize_data (file_data_transformed)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        # hm_am_ratio_frame = calculateAMAndHM(winsorized_hm_am_ratio_frame)
        df_merged = reduce(lambda left, right: pd.merge(left, right,on=['day','month']), hm_am_ratio_frame)
        #following line should not be hard coded column
        #if we can calculate the mean in a loop that would have been much better
        df_merged['mean_ratio'] = df_merged[['ratio2014', 'ratio2015']].mean(axis=1) #take mean of two ratio per day
        mean_absolute_deviation = df_merged['mean_ratio'].mad()
        # standard_deviation = df_merged['mean_ratio'].std()
        # print(df_merged)
       # print("mad: ",mean_absolute_deviation," std: ",standard_deviation)
        df_merged_copied = df_merged.copy()
        safe_margin = calculate_safe_margin(df_merged_copied,mean_absolute_deviation,1)
        # self.historical_safe_margin = safe_margin_kappa_varied[['day','month','margin_low','margin_high']].copy()
        historical_safe_margin = safe_margin[['day','month','margin_low','margin_high']].copy()
        # nabla_frame_no_attack = calculate_nabla(df_merged, self.historical_safe_margin, TRAINING_DATA.keys())
        nabla_frame_no_attack = calculate_nabla(df_merged, historical_safe_margin, TRAINING_DATA.keys())
        residual_frame_no_attack = calculate_ruc(nabla_frame_no_attack, TRAINING_DATA.keys())
        residual_frame_no_attack.plot(x="day", y=['ruc2014', 'ruc2015'],
                                      kind="line",
                                      xlim=(START, END),
                                      ylim=(-.25, .25),
                                      title = "No Attack")
        # self.tmax_ruc = calculateTmax_ruc(residual_frame_no_attack, TRAINING_DATA.keys())# non attacked standard limit max margin
        # self.tmin_ruc = calculateTmin_ruc(residual_frame_no_attack, TRAINING_DATA.keys()) # non attacked standard limit min margin
        tmax_ruc = calculateTmax_ruc(residual_frame_no_attack, TRAINING_DATA.keys())# non attacked standard limit max margin
        tmin_ruc = calculateTmin_ruc(residual_frame_no_attack, TRAINING_DATA.keys()) # non attacked standard limit min margin

        print("tmax_ruc: ",tmax_ruc," tmin_ruc: ",tmin_ruc) #uncomment it later
        # print("tmax_ruc: ",self.tmax_ruc," tmin_ruc: ",self.tmin_ruc) # commented on 08/27/2020
        # print( self.historical_safe_margin.head(2))

        #endregion
        #region under attack
        file_data_for_adversary = {} #keeping original data by copying it into a new var
        for key in file_data.keys():
            file_data_for_adversary[key] = file_data[key].copy()
        additive_attack_data,self.number_of_compromised_meter = perform_deductive_attack(file_data_for_adversary,.4,200,START_DATE_M,END_DATE_M)
        additive_attack_data_copied = {}
        for key in additive_attack_data.keys():
            additive_attack_data_copied[key] = additive_attack_data[key][additive_attack_data[key]['use'] > 0]

        additive_attack_data_transformed = box_cox_transformation(additive_attack_data_copied)
        # winsorized_hm_am_ratio_frame_attacked = winsorize_data (additive_attack_data_transformed)

        # hm_am_ratio_frame_attacked = calculate_AM_HM_ratio(additive_attack_data_transformed)
        hm_am_ratio_frame_attacked = calculateAMAndHM(additive_attack_data_transformed)
        # hm_am_ratio_frame_attacked = calculateAMAndHM(winsorized_hm_am_ratio_frame_attacked)
        df_merged_attacked = reduce(lambda left, right: pd.merge(left, right,
                                                      on=['day', 'month']), hm_am_ratio_frame_attacked)

        temp1 = pd.DataFrame(columns = ["ratio2014_normal"])
        temp2 = pd.DataFrame(columns = ["ratio2014_attacked"])
        temp1['ratio2014_normal'] = df_merged['ratio2014']
        temp2['ratio2014_attacked'] =  df_merged_attacked['ratio2014']
        ax = temp1.plot( xlim=(START, END),
                            ylim = (.8,1.00),title =
                         "ratio plot for month 3 to 6 in year 2014 with delavg 500 romal 40%")
        ax.set_xlabel("Days in a Year")
        ax.set_ylabel("Ratio of HM/AM per day")

        temp2.plot(ax=ax)
        # following line should not be hard coded column
        # if we can calculate the mean in a loop that would have been much better
        df_merged_attacked['mean_ratio'] = df_merged_attacked[['ratio2014', 'ratio2015']].mean(axis=1)  # take mean of two ratio per day

        mean_absolute_deviation = df_merged_attacked['mean_ratio'].mad()
        # standard_deviation = df_merged['mean_ratio'].std()
        # print(df_merged)
        # print("mad: ",mean_absolute_deviation," std: ",standard_deviation)
        df_merged_copied = df_merged_attacked.copy()
        safe_margin = calculate_safe_margin(df_merged_copied, mean_absolute_deviation,1)
        temp3 = pd.DataFrame(columns=["margin_high"])
        temp4 = pd.DataFrame(columns=["margin_low"])
        temp3['margin_high'] = safe_margin['margin_high']
        temp4['margin_low'] = safe_margin['margin_low']
        ax1 = temp3.plot(xlim=(START, END),
                        ylim=(.8, 1.00), title=
                        "safe Margin")
        ax1.set_xlabel("Days in a Year")
        ax1.set_ylabel("Ratio of HM/AM per day")

        temp4.plot(ax=ax1)

        self.historical_safe_margin = safe_margin[['day', 'month', 'margin_low', 'margin_high']].copy()
        nabla_frame = calculate_nabla(df_merged_attacked,self.historical_safe_margin,TRAINING_DATA.keys())
        residual_frame = calculate_ruc(nabla_frame,TRAINING_DATA.keys())
        residual_frame.plot(x="day",y=['ruc2014','ruc2015'],
                            kind = "line",
                            xlim=(START, END),
                            ylim = (-.25,.25),
                            title = "After attack")
        self.tmax_ruc = calculateTmax_ruc(residual_frame, TRAINING_DATA.keys())#commented on 08/27/2020
        self.tmin_ruc = calculateTmin_ruc(residual_frame, TRAINING_DATA.keys())
        # tmax_ruc = calculateTmax_ruc(residual_frame, TRAINING_DATA.keys())
        # tmin_ruc = calculateTmin_ruc(residual_frame, TRAINING_DATA.keys())
        print("After attack tmax_ruc: ",self.tmax_ruc," tmin_ruc: ",self.tmin_ruc)
        # print("After attack tmax_ruc: ",tmax_ruc," tmin_ruc: ",tmin_ruc)
        #
        # #region IPL& Prelec
        # mrucFrame = inversePowerLaw(nabla_frame,TRAINING_DATA.keys())
        # weightedRuc = weightForRuc(mrucFrame,TRAINING_DATA.keys())
        # modifiedRuc = newModifiedRUC(mrucFrame, weightedRuc,TRAINING_DATA.keys())
        # modifiedRuc.plot(x="day", y=["mruc2014", "mruc2015"], kind="line",
        #                  xlim=(START, END),
        #                  ylim=(-.25,.25),
        #                  title = "Prelec")
        # plt.show()
        # self.tmax_mruc = calculateTmax_mruc(modifiedRuc, TRAINING_DATA.keys())
        # self.tmin_mruc = calculateTmin_mruc(modifiedRuc, TRAINING_DATA.keys())
        # print("tmax_mruc: ", self.tmax_mruc, " tmin_mruc: ", self.tmin_mruc )
        # #endregion
        # # deductive_attack_data = perform_deductive_attack(file_data_for_adversary)
        # #endregion
        # # df_merged.plot(x="day",y=['ratio2014','ratio2015'], kind = "line", ylim = (BOTTOM,TOP))
        # # df_merged_attacked.plot(x="day",y=['ratio2014','ratio2015'], kind = "line", ylim = (BOTTOM,TOP))
        plt.show()

    def testing(self, TESTING_DATA):
        file_data = {}
        for key in TESTING_DATA.keys():  # for each file in the dictionary
            file_relative_path = TESTING_DATA.get(key)  # get the file path by year as key
            file_path = o.path.abspath(file_relative_path)  # relative path of the data file
            file_data[key] = read_data(file_path, key)  # call the reader function of datareader object

        # additive_attack_data = perform_additive_attack_on_test(file_data) #uncomment if you want attack on test data
        # additive_attack_data_copied = {}
        # for key in additive_attack_data.keys():
        #     additive_attack_data_copied[key] = additive_attack_data[key][additive_attack_data[key]['use'] >= 0]
        #     #uncomment if you want attack on test data
        file_data_transformed = box_cox_transformation(file_data) #when no attack is applied
        # file_data_transformed = box_cox_transformation(additive_attack_data_copied)
        hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
        df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day', 'month']), hm_am_ratio_frame)
        merged_with_safe_margin = pd.merge(df_merged,self.historical_safe_margin,on=['day','month'])
        tier1_anomaly = 0
        tier2_anomaly_ruc = 0
        tier2_anomaly_mruc = 0

        # print("merged_with_safe_margin: ",merged_with_safe_margin)
        # START_DATE = '2014-01-01'
        # END_DATE = '2014-06-01' # day count = 152
        for row in merged_with_safe_margin.itertuples():
            if not(getattr(row,"ratio2016")<=getattr(row,'margin_high') and
                   getattr(row,'ratio2016')>= getattr(row,'margin_low')):
                tier1_anomaly = tier1_anomaly + 1
                if (getattr(row,"ratio2016")> getattr(row,'margin_high')):
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_high')
                else:
                    ruc = getattr(row,"ratio2016") - getattr(row,'margin_low')
                # print ("ruc: ",ruc)
                if not ruc ==0:
                    if not (ruc<= self.tmax_ruc and ruc>= self.tmin_ruc):
                        # print("ruc: ",ruc," with max t: ",self.tmax_ruc," min t: ",self.tmin_ruc)
                        tier2_anomaly_ruc = tier2_anomaly_ruc +1
                        self.system_detected_unsafe.append(getattr(row,"day"))
                    if not (ruc <= self.tmax_mruc and ruc >= self.tmin_mruc):
                        tier2_anomaly_mruc = tier2_anomaly_mruc + 1

        print("Number of detection of attack by tier 1: ",tier1_anomaly,
              " number of detection of attack by tier 2 ruc: ",tier2_anomaly_ruc)
        print("Number of detection of attack by tier 2 mruc: ",tier2_anomaly_mruc)
        # print ("Number of false alarm tier 1 having .5 MAD: ", abs(152-tier1_anomaly))
        # print ("Number of false alarm tier 2 ruc", abs(152-tier2_anomaly_ruc))
        # print ("Number of false alarm tier 2 mruc: ", abs(152-tier2_anomaly_mruc))
        prev = 0
        detection_time_sum = 0
        if not (len(self.system_detected_unsafe) == 0):
            for row in self.system_detected_unsafe:
                if(prev == 0):
                    prev = row
                else:
                    print("days:", prev, " ",row)
                    detection_time_sum += row - prev
                    prev = row
            print("Total time sum: ", detection_time_sum," number of false alarms: ", len(self.system_detected_unsafe))
            detection_time_avg = detection_time_sum/len(self.system_detected_unsafe)
            print("detection time avg: ",detection_time_avg)
        reports_per_day = tier2_anomaly_ruc/DAYS_INA_YEAR
        revenue_per_day = (DEL_AVG *  self.number_of_compromised_meter * E * reports_per_day)/1000
        impact_of_undetected_attack = revenue_per_day/24
        print("impact of undetected attack: ", impact_of_undetected_attack)

# if __name__ == '__main__':
#     main = Main()  # this containing the old codes we now should have seperate files
#     main.training(TRAINING_DATA)
#     # main.testing(TESTING_DATA)

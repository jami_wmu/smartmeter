import sys
from random import randint
import pandas as pd
from scipy import stats
from src.utility.constant_new import *
import numpy
import random
random.seed(42)

def box_cox_transformation_new(data):
    for key in data.keys():
        data[key].use = stats.boxcox(data[key].use, lmbda=LAMBDA, alpha=None)
    return data

def read_data_new(filePath, year):
    file = pd.read_csv(filePath)  # read file from the abs dir
    file['localminute'] = pd.to_datetime(file['localminute'].str.slice(0, 19))
    file['use'] = file['use'] * 1000
    file = file[(file[['use']] > 50).all(axis=1)]
    # mask_less_50 = (file['use'] <= 50)
    # df = file.loc[mask_less_50]
    # # replacing all values less than 50 by a random number 10-50
    # file.loc[mask_less_50, 'use'] = df['use'].apply(lambda x: randint(20, 50))
    file = file[file['localminute'].dt.year == int(year)]
    # print("length file: ", len(file))
    return file

def calculateAMAndHM_new(data):
    frame_array = []
    for key in data.keys():
        dateRatioArray = []
        day = 1
        yearlyData = data[key]  # yearly data
        monthwisegroupData = yearlyData.groupby(pd.Grouper(key='localminute', freq='1M'))  # monthwise grouping the data
        for key, monthlydata in monthwisegroupData:  # iterate each month data
            if len(monthlydata) > 0:  # check if there is data for the month
                daywisegroupData = monthlydata.groupby(
                    pd.Grouper(key='localminute', freq='1D'))  # group the montly data by day
                for key, dailyData in daywisegroupData:  # iterate each day of the month
                    if len(daywisegroupData) > 0:  # check if the day wise data count is greater than zero
                        hourwiseGroupData = dailyData.groupby(
                            pd.Grouper(key='localminute', freq='1H'))  # group the daily data into hourlwise
                        hourwiseSumAM = 0
                        hourwiseSumHM = 0
                        for key, hourlyData in hourwiseGroupData:  # iterate each hourly data of the day
                            if len(hourlyData) > 0:  # check if the hour has data count greater than zero
                                am_sum = 0
                                hm_sum = 0
                                for row in hourlyData.itertuples():
                                    if (getattr(row, "use") > 0):
                                        am_sum += getattr(row, "use")  # calculate arithmatic sum
                                        hm_sum += 1 / getattr(row, "use")  # calculate harmonic sum
                                N = len(hourlyData.dataid.unique())
                                hourlyAttackHM = N / hm_sum  # harmonic mean hourly
                                hourlyAttackAM = am_sum / N  # arithmatic mean hourly
                                hourwiseSumAM += hourlyAttackAM
                                hourwiseSumHM += hourlyAttackHM
                        year = int(key.year)
                        month = key.month
                        daywiseRatioMean = hourwiseSumHM / hourwiseSumAM  # use the ratio means of 24 hours of a day to calculate day wise ratio means
                        temp = {'year': year, 'month': month, 'day': day, 'ratio' + str(key.year): daywiseRatioMean}
                        day = day + 1
                        # print(temp)
                        dateRatioArray.append(temp)
        dateRatioFrame = pd.DataFrame(dateRatioArray)
        # print(dateRatioFrame.head(2))
        frame_array.append(dateRatioFrame)
        # plot = dateRatioFrame.plot(kind="line", x="day", y="ratio"+str(key.year), color="blue",
        #                            xlim=(START, END),
        #                            ylim =(.85, 1.00),
        #                            title =str(key.year)+ ' ratio plot vs day')

    return frame_array


def calculate_safe_margin_new(data_frame, mad, threshold):
    data_frame['margin_high'] = data_frame['mean_ratio'] + threshold * mad
    data_frame['margin_low'] = data_frame['mean_ratio'] - threshold * mad
    return data_frame

def calculate_nabla_new(ratio_frame, keys):
    nabla = []
    for row in ratio_frame.itertuples():
        temp = {}
        # print(row)
        for key in keys:
            if (getattr(row, 'ratio' + key) > getattr(row, 'margin_high')):
                temp.update({'nabla' + key: (getattr(row, 'ratio' + key) - getattr(row, 'margin_high'))})
            elif (getattr(row, 'ratio' + key) < getattr(row, 'margin_low')):
                temp.update({'nabla' + key: (getattr(row, 'ratio' + key) - getattr(row, 'margin_low'))})
            else:
                temp.update({'nabla' + key: 0})
        temp.update({'day': row.day})
        nabla.append(temp)
    nabla_frame = pd.DataFrame(nabla)
    return nabla_frame

def calculate_ruc_new(nabla_frame, keys):
    ruc_arry = []
    for row in nabla_frame.itertuples():
        temp = {}
        if (row.day <= SLIDING_FRAME):
            for key in keys:
                temp.update({'ruc' + key: getattr(row, 'nabla' + key)})
            temp.update({'day': row.day})
            ruc_arry.append(temp)
        else:
            for key in keys:
                sliding_frame_rows = nabla_frame.loc[(nabla_frame['day'] <= row.day)
                                                     & (nabla_frame['day'] > (row.day - SLIDING_FRAME))]
                ruc = 0
                count = 0
                for row1 in sliding_frame_rows.itertuples():
                    ruc = ruc + getattr(row1, 'nabla' + key)
                    count = count +1
                temp.update({'ruc' + key: ruc})
            temp.update({'day': row.day})
            ruc_arry.append(temp)
    ruc_frame = pd.DataFrame(ruc_arry)
    return ruc_frame


def calculateTmax_ruc_new(rucFrame, keys):
    maxSum = sys.float_info.max
    maxRuc2014 = rucFrame['ruc2014'].max()
    maxRuc2015 = rucFrame['ruc2015'].max()
    # print(maxRuc2014,maxRuc2015)
    max_candidate = max(maxRuc2015, maxRuc2014)
    global maxThreshold
    # print("maxThreshold: ", max_candidate)
    for taoThreshold in numpy.arange(0.0000, max_candidate, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        # print("executing")
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) > 0):  # previously It was greater equal
                    if (getattr(row, "ruc" + key) < taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                        costCount = costCount + 1
                    else:
                        pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            taoSumDiff = abs(costSum - pSum)
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold = taoThreshold
    # print(maxThreshold)
    return maxThreshold


def calculateTmin_ruc_new(rucFrame, keys):
    minSum = sys.float_info.max
    minRuc2014 = rucFrame['ruc2014'].min()
    minRuc2015 = rucFrame['ruc2015'].min()
    # print(minRuc2014,minRuc2015)
    min_candidate = min(minRuc2015, minRuc2014)
    global minThreshold1
    # min_candidate = min_candidate*(-1)
    print("Min candidate: ", min_candidate)
    for taoThreshold in numpy.arange(min_candidate, 0, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        # print("tau",taoThreshold)
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) < 0):  # previously It was greater equal
                    if (getattr(row, "ruc" + key) > taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                        costCount = costCount + 1
                    else:
                        pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            taoSumDiff = abs(costSum - pSum)
            if (minSum > taoSumDiff):
                minSum = taoSumDiff
                minThreshold1 = taoThreshold
    # print(minThreshold1)
    return minThreshold1

def calculateTmax_ruc_new_L2(rucFrame, keys):
    maxSum = sys.float_info.max
    maxRuc2014 = rucFrame['ruc2014'].max()
    maxRuc2015 = rucFrame['ruc2015'].max()
    max_candidate = max(maxRuc2015, maxRuc2014)
    global maxThreshold
    # print("maxThreshold: ", max_candidate)
    for taoThreshold in numpy.arange(0.0000, max_candidate, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        # print("executing")
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) > 0):  # previously It was greater equal
                    if (getattr(row, "ruc" + key) < taoThreshold):
                        temp = abs(taoThreshold - getattr(row, "ruc" + key))/2
                        costSum += pow(temp,2)
                        costCount = costCount + 1
                    else:
                        temp = abs(taoThreshold - getattr(row, "ruc" + key))*2
                        pSum += pow(temp,2)
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            taoSumDiff = abs(costSum - pSum)
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold = taoThreshold
    # print(maxThreshold)
    return maxThreshold


def calculateTmin_ruc_new_L2(rucFrame, keys):
    minSum = sys.float_info.max
    minRuc2014 = rucFrame['ruc2014'].min()
    minRuc2015 = rucFrame['ruc2015'].min()
    min_candidate = min(minRuc2015, minRuc2014)
    global minThreshold1
    # min_candidate = min_candidate*(-1)
    # print("maxThreshold: ", min_candidate)
    for taoThreshold in numpy.arange(min_candidate, 0, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        # print("tau",taoThreshold)
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) < 0):  # previously It was greater equal
                    if (getattr(row, "ruc" + key) > taoThreshold):
                        temp = abs(taoThreshold - getattr(row, "ruc" + key))/2
                        costSum += pow(temp,2)
                        costCount = costCount + 1
                    else:
                        temp = abs(taoThreshold - getattr(row, "ruc" + key))*2
                        pSum += pow(temp,2)
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            taoSumDiff = abs(costSum - pSum)
            if (minSum > taoSumDiff):
                minSum = taoSumDiff
                minThreshold1 = taoThreshold
    # print(minThreshold1)
    return minThreshold1

def perform_additive_attack(file_data, romal, delavg, START_DATE, END_DATE):
    file_data_copied = {}
    for key in file_data.keys():
        file_data_copied[key] = file_data[key].copy()
    attack_window_data = file_data_copied[str(ATTACK_YEAR)]
    allMeters = attack_window_data.dataid.unique()
    numberofitems = (len(allMeters) * romal)
    compromisedMeters = random.sample(allMeters.tolist(), int(numberofitems))
    mask = (attack_window_data['localminute'] >= START_DATE) & \
           (attack_window_data['localminute'] <= END_DATE) & \
           (attack_window_data['dataid'].isin(compromisedMeters))
    df = attack_window_data.loc[mask]
    s = delavg - 50
    e = delavg + 50
    attack_window_data.loc[mask, 'use'] = df['use'].apply(lambda x: x + randint(s, e))
    return file_data_copied, numberofitems

def perform_deductive_attack(file_data, romal, delavg, START_DATE, END_DATE):
    file_data_copied = {}
    for key in file_data.keys():
        file_data_copied[key] = file_data[key].copy()
    attack_window_data = file_data_copied[str(ATTACK_YEAR)]
    allMeters = attack_window_data.dataid.unique()
    print("Number of meteres", len(allMeters))
    numberofitems = (len(allMeters) * romal)
    print("number of items to sample", numberofitems)
    compromisedMeters = random.sample(allMeters.tolist(), int(numberofitems))
    print("Number of compromised meteres", len(compromisedMeters))
    mask = (attack_window_data['localminute'] >= START_DATE) & \
           (attack_window_data['localminute'] <= END_DATE) & \
           (attack_window_data['dataid'].isin(compromisedMeters))
    df = attack_window_data.loc[mask]
    # s = delavg - 50
    # e = delavg + 50
    attack_window_data.loc[mask, 'use'] = df['use'].apply(lambda x: x - delavg)
    return file_data_copied, numberofitems

def calculateTmax_ruc_QR_l1(rucFrame):
    global maxThreshold1
    max_candidate = max(rucFrame)
    maxSum = sys.float_info.max
    difference = dict()
    for taoThreshold in numpy.arange(0.0000, max_candidate, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            if (row > 0):
                if (row <  taoThreshold):
                    costSum += abs(taoThreshold - row) / 2
                    costCount = costCount + 1
                else:
                    pSum += abs(taoThreshold - row) * 2
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold1 = taoThreshold
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    return maxThreshold1, maxSum

def calculateTmin_ruc_QR_l1(rucFrame):
    global minThreshold
    minSum = sys.float_info.max
    min_candidate = min(rucFrame)
    print("Min Candidate: ",min_candidate)
    difference = dict()
    for taoThreshold in numpy.arange(min_candidate, 0.00, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            if (row < 0):
                # print(row)
                if (row > ( taoThreshold)):
                    # temp = abs(taoThreshold*(-1) - row)/2
                    # costSum += pow(temp, 2)
                    costSum += abs(taoThreshold - row) / 2
                    costCount = costCount + 1
                else:
                    # temp = abs(taoThreshold*(-1) - row)*2
                    # pSum += pow(temp, 2)
                    pSum += abs(taoThreshold - row) * 2
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            if (minSum > taoSumDiff):
                # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold*(-1))
                minSum = taoSumDiff
                minThreshold = taoThreshold
        # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    return minThreshold,minSum

def calculateTmax_ruc_QR_L2(rucFrame):
    global maxThreshold1
    max_candidate = max(rucFrame)
    maxSum = sys.float_info.max
    for taoThreshold in numpy.arange(0.0000, max_candidate, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            if (row > 0):
                if (row <  taoThreshold):
                    temp = abs(taoThreshold - row) / 2
                    costSum += pow(temp, 2)
                    costCount = costCount + 1
                else:
                    temp = abs(taoThreshold - row) * 2
                    pSum += pow(temp, 2)
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            taoSumDiff = abs(costSum - pSum)
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold1 = taoThreshold
    return maxThreshold1, maxSum

def calculateTmin_ruc_QR_L2(rucFrame):
    global minThreshold
    minSum = sys.float_info.max
    min_candidate = min(rucFrame)
    for taoThreshold in numpy.arange(min_candidate, 0.00, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame:
            if (row < 0):
                # print(row)
                if (row > ( taoThreshold)):
                    temp = abs(taoThreshold - row) / 2
                    costSum += pow(temp, 2)
                    costCount = costCount + 1
                else:
                    temp = abs(taoThreshold - row) * 2
                    pSum += pow(temp, 2)
                    penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            if (minSum > taoSumDiff):
                # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold*(-1))
                minSum = taoSumDiff
                minThreshold = taoThreshold
    return minThreshold,minSum


#observation if you winsorize something it will increase the mean ratio
#we might avoid winsorizing the data
#*********************
#parameters to evaluate the performance
#time to detection: after attack introduction when it gets detected
#*************************
TRAINING_DATA = {
    "2014":"../../resource/2014.csv",
    "2015":"../../resource/2015.csv",
}
TESTING_DATA = {
    "2016": "../../resource/2016.csv",
}
KAPPA = [0.25,0.50,0.75,1.0,1.25,1.5,1.75,2.00,2.25,2.5]

DEL_AVG_ARRAY_ADD = [200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000]
DEL_AVG_ARRAY_ADD_TE = [100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150
                        ,1200,1250,1300,1350,1400,1450,1500]


DEL_AVG_ARRAY_DED = [50,70,100,130,150,175,200,225,250,275,300]
DEL_AVG_ARRAY_DED_TE = [10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,175,180,190,200,210,220,225,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,390,400]

DEL_AVG_ARRAY_DED_TR = [100,130,150,175,200,225,250,275,300]
# DEL_AVG_ARRAY_DED_TE = [10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,175,180,190,200,210,220,225,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,390,400]
RO_MAL = .90 #fraction of attacked meters
RO_MAL_ARRAY = [0.20,0.30,0.40,0.50,0.60,0.70]
RO_MAL_ARRAY_N = [.70,.80]
DEL_AVG = 1500 #amount of posoning in the attacked time frame
START = 1
END = 365
E = 0.12 #avg per unit electricity cost
DAYS_INA_YEAR = 365
ITA = 1
ALPHA = .7#[0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1]
BETA = .8#0.25
LAMBDA = 0.25
BOTTOM = 0.70
TOP = 1.00
THREASHOLD = .5
SMALL_ADDITIVE = 0
LARGE_ADDITIVE = 1
LARGE_DEDUCTIVE = 2
SMALL_DEDUCTIVE = 4
LARGE_CAMOUFLAGE = 3
SMALL_CAMOUFLAGE = 5
ATTACK_YEAR_TEST = 2016
ATTACK_YEAR = 2016
START_DATE = '2014-04-01' #day number 91
END_DATE = '2014-06-30' # day number 181
START_DATE_TEST = '2016-07-01' # this will not be used for poisoning attack cases
END_DATE_TEST = '2016-09-30'

# START_DATE_TEST = '2016-10-01' # this will not be used for poisoning attack cases
# END_DATE_TEST = '2016-12-30'


SLIDING_FRAME = 7
RESIDUAL_BOTTOM = -1
RESIDUAL_TOP = 1
# DEL_AVG_ARRAY_DUAL_ATTACK = [{'del_avg_s':50,'del_avg_m':450},
#                  {'del_avg_s':100,'del_avg_m':500},
#                  {'del_avg_s':150,'del_avg_m':550},
#                  {'del_avg_s':200,'del_avg_m':600},
#                  {'del_avg_s':250,'del_avg_m':650},
#                  {'del_avg_s':300,'del_avg_m':700},
#                  {'del_avg_s':350,'del_avg_m':750},
#                  {'del_avg_s':400,'del_avg_m':800}] #small scale attack
# DEL_AVG_ARRAY = [50,100,150,200,250,300,350,400,430,450,470,490,510,530,550]
MONTH_DAY_MAP = {
    1:31,
    2:28,
    3:31,
    4:30,
    5:31,
    6:30,
    7:31,
    8:31,
    9:30,
    10:31,
    11:30,
    12:31
}
"""
      =======================================
         Normal Year          Leap Year
        --------------      ---------------
        Jan    1 -  31       Jan    1 -  31           
        Feb   32 -  59       Feb   32 -  60           
        Mar   60 -  90       Mar   61 -  91           
        Apr   91 - 120       Apr   92 - 121           
        May  121 - 151       May  122 - 152            
        Jun  152 - 181       Jun  153 - 182            
        Jul  182 - 212       Jul  183 - 213            
        Aug  213 - 243       Aug  214 - 244             
        Sep  244 - 273       Sep  245 - 274            
        Oct  274 - 304       Oct  275 - 305            
        Nov  305 - 334       Nov  306 - 335            
        Dec  335 - 365       Dec  336 - 366 
"""

from src.utility.common_functions import *
# t_min:  -0.1075  t_max: 0.0625
upper_limit = 0.0625
lower_limit = -0.1075
def calculateTmax_ruc(rucFrame, keys):
    maxSum = sys.float_info.max
    maxRuc2014 = rucFrame['ruc2014'].max()
    maxRuc2015 = rucFrame['ruc2015'].max()
    maxThreshold = max(maxRuc2015, maxRuc2014)
    difference = dict()
    for taoThreshold in numpy.arange(0.0000, .100, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) > 0):  # previously It was greater equal
                    if (getattr(row, "ruc" + key) < taoThreshold):
                        temp = taoThreshold - getattr(row, "ruc" + key)
                        costSum += abs(pow(temp,2)) / 2
                        costCount = costCount + 1
                    else:
                        temp = taoThreshold - getattr(row, "ruc" + key)
                        pSum += abs(pow(temp,2)) * 2
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            # taoSumDiff = abs(costSum - pSum)
            taoSumDiff = pow((costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold)
            # print("Cost Sum: ", costSum," psum: ",pSum," maxSum: ",maxSum," taoDifference: ",taoSumDiff)
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold = taoThreshold
        # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    fig = plt.figure()
    plt.scatter(x, y, marker='o')
    fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Cost - Penalty')
    # fig.savefig('test.jpg')
    plt.show()
    return maxThreshold

def calculateTmin_ruc(rucFrame, keys):
    minSum = sys.float_info.max
    minRuc2014 = rucFrame['ruc2014'].min()
    minRuc2015 = rucFrame['ruc2015'].min()
    minThreshold = min(minRuc2015, minRuc2014)
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) < 0):
                    if (getattr(row, "ruc" + key) > ((-1) * taoThreshold)):
                        temp = taoThreshold - getattr(row, "ruc" + key)
                        costSum += abs(pow(temp, 2)) / 2
                        costCount = costCount + 1
                    else:
                        temp = taoThreshold - getattr(row, "ruc" + key)
                        pSum += abs(pow(temp, 2)) * 2
                        penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            # taoSumDiff = abs(costSum - pSum + (costCount-penaltyCount)*taoThreshold)
            # taoSumDiff = abs(costSum - pSum)
            taoSumDiff = pow((costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            if (minSum > taoSumDiff):
                # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold*(-1))
                minSum = taoSumDiff
                minThreshold = taoThreshold * (-1)
        # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    fig = plt.figure()
    plt.plot(x, y)
    fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Cost - Penalty')
    # fig.savefig('test.jpg')
    plt.show()
    return minThreshold

def calculate_max_by_gradient_sqr_res_inv_sqr_no_shift(rucFrame, keys):
    target_t_min = 0.0675
    difference = dict()
    sorted_frame = rucFrame.sort_values(by=["ruc2015"],ascending=False)
    sorted_non_zero_ruc = sorted_frame[sorted_frame['ruc2015'] > 0]
    non_zero_ruc_array = sorted_non_zero_ruc["ruc2015"].values
    print(non_zero_ruc_array)
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(non_zero_ruc_array))]
    for l in range(len(non_zero_ruc_array)):
        if (non_zero_ruc_array[l] < upper_limit):
            temp = lower_limit - non_zero_ruc_array[l]
            costSum += abs(pow(temp, 2)) / 2
            cost_count = cost_count + 1
        else:
            temp = lower_limit - non_zero_ruc_array[l]
            pSum += abs(pow(temp, 2)) * 2
            penalty_count = penalty_count + 1
        # costFunction[l] =  abs(costSum-pSum + (cost_count - penalty_count)* upper_limit)
        # costFunction[l] =  abs(costSum-pSum)
        costFunction[l] =  pow((costSum - pSum), 2)
    print("org Cost count:",cost_count," penalty count: ",penalty_count)
    # print(costFunction)
    index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,non_zero_ruc_array) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_max_by_manual(non_zero_ruc_array)  # Gradient by manual process
    non_zero_ruc_array_copied = non_zero_ruc_array.copy()
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]< 0):
            sign = (-1)*1
        else:sign = 1
        print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  + 0.03
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]+0.03*sign
        # print(non_zero_ruc_array_copied)
        # tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # if(tmin<target_t_min):
        #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
        #     break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                temp = lower_limit - non_zero_ruc_array_copied[l]
                costSum += abs(pow(temp, 2)) / 2
                cost_count = cost_count + 1
            else:
                temp = lower_limit - non_zero_ruc_array_copied[l]
                pSum += abs(pow(temp, 2)) * 2
                penalty_count = penalty_count + 1
        # taoSumDiff = abs(pSum - costSum + (cost_count - penalty_count)* upper_limit)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        taoSumDiff = pow((costSum - pSum), 2)
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        print("Cost count:", cost_count, " penalty count: ", penalty_count)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    return y


def calculate_max_by_gradient_sqr_res_inv_sqr(rucFrame, keys):
    target_t_min = 0.0675
    difference = dict()
    sorted_frame = rucFrame.sort_values(by=["ruc2015"],ascending=False)
    sorted_non_zero_ruc = sorted_frame[sorted_frame['ruc2015'] > 0]
    non_zero_ruc_array = sorted_non_zero_ruc["ruc2015"].values
    print(non_zero_ruc_array)
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(non_zero_ruc_array))]
    for l in range(len(non_zero_ruc_array)):
        if (non_zero_ruc_array[l] < upper_limit):
            temp = lower_limit - non_zero_ruc_array[l]
            costSum += abs(pow(temp, 2)) / 2
            cost_count = cost_count + 1
        else:
            temp = lower_limit - non_zero_ruc_array[l]
            pSum += abs(pow(temp, 2)) * 2
            penalty_count = penalty_count + 1
        # costFunction[l] =  abs(costSum-pSum + (cost_count - penalty_count)* upper_limit)
        # costFunction[l] =  abs(costSum-pSum)
        costFunction[l] =  pow((costSum - pSum), 2)
    print("org Cost count:",cost_count," penalty count: ",penalty_count)
    # print(costFunction)
    index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,non_zero_ruc_array) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_max_by_manual(non_zero_ruc_array)  # Gradient by manual process
    non_zero_ruc_array_copied = non_zero_ruc_array.copy()
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]< 0):
            sign = (-1)*1
        else:sign = 1
        print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  + 0.03
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]+0.03*sign
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + \
        #                                                      (abs(non_zero_ruc_array_copied[index_of_sorted_list[
        #                                                          i]] - upper_limit) + 0.03) * sign
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + \
                                                             (abs(non_zero_ruc_array_copied[index_of_sorted_list[
                                                                 i]] - upper_limit) + 0.03)
        # print(non_zero_ruc_array_copied)
        # tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # if(tmin<target_t_min):
        #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
        #     break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                temp = lower_limit - non_zero_ruc_array_copied[l]
                costSum += abs(pow(temp, 2)) / 2
                cost_count = cost_count + 1
            else:
                temp = lower_limit - non_zero_ruc_array_copied[l]
                pSum += abs(pow(temp, 2)) * 2
                penalty_count = penalty_count + 1
        # taoSumDiff = abs(pSum - costSum + (cost_count - penalty_count)* upper_limit)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        taoSumDiff = pow((costSum - pSum), 2)
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        print("Cost count:", cost_count, " penalty count: ", penalty_count)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    return y

def calculate_optimal_max_threshold_sqr_res_inv_sqr(rucFrame, keys):
    target_t_min = 0.0675
    difference = dict()
    sorted_frame = rucFrame.sort_values(by=["ruc2015"],ascending=False)
    sorted_non_zero_ruc = sorted_frame[sorted_frame['ruc2015'] > 0]
    non_zero_ruc_array = sorted_non_zero_ruc[["ruc2015"]].values
    non_zero_ruc_array_copied = non_zero_ruc_array.copy()
    for i in range(len(non_zero_ruc_array_copied)):
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        non_zero_ruc_array_copied[i] = non_zero_ruc_array_copied[i] + 0.03
        # tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # if(tmin<target_t_min):
        #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
        #     break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                temp = lower_limit - non_zero_ruc_array_copied[l]
                costSum += abs(pow(temp, 2)) / 2
                cost_count = cost_count + 1
            else:
                temp = lower_limit - non_zero_ruc_array_copied[l]
                pSum += abs(pow(temp, 2)) * 2
                penalty_count = penalty_count + 1
        # taoSumDiff = abs(costSum - pSum + (cost_count - penalty_count)*upper_limit )  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(costSum - pSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = pow((costSum - pSum), 2)
        # taoSumDiff = abs(costSum - pSum + (i + 1) * 0.177)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        # previous_diff = taoSumDiff
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    return y

def calculate_min_by_gradient_sqr_res_inv_sqr_no_shift(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    # lower_limit = -0.090
    target_t_min = -0.2
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        costFunction =[0 for i in range(len(non_zero_ruc_array))]
        for l in range(len(non_zero_ruc_array)):
            if (non_zero_ruc_array[l] > lower_limit):
                temp = lower_limit - non_zero_ruc_array[l]
                costSum += abs(pow(temp, 2)) / 2
                cost_count = cost_count+ 1
            else:
                temp = lower_limit - non_zero_ruc_array[l]
                pSum += abs(pow(temp, 2)) * 2
                penalty_count = penalty_count + 1
            # costFunction[l] =  abs(costSum-pSum + (cost_count-penalty_count)*(lower_limit))
            # costFunction[l] =  abs(costSum-pSum)
            costFunction[l] =  pow((costSum - pSum), 2)
        print("org Cost Count: ", cost_count," Penalty Count: ",penalty_count)
        index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,non_zero_ruc_array) #gradient by cost array
        # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
        # index_of_sorted_list,gradients = return_gradients_for_two_function(non_zero_ruc_array)  # Gradient for a functiom
        # index_of_sorted_list,gradients = gradient_min_by_manual(non_zero_ruc_array)  # Gradient by manual process
        non_zero_ruc_array_copied = non_zero_ruc_array.copy()
        for i in range(len(index_of_sorted_list)):
            if(gradients[index_of_sorted_list[i]]<0):
                sign = (-1)*1
            else:sign = 1
            print(sign)
            costSum = 0
            pSum = 0
            cost_count = 0
            penalty_count = 0
            non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + 0.03 * sign
            # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + 0.03
            # print(non_zero_ruc_array_copied)11
            # tmin = return_new_tao_min(non_zero_ruc_array_copied)
            # if(tmin<target_t_min):
            #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            #     break
            for l in range(len(non_zero_ruc_array_copied)):
                if (non_zero_ruc_array_copied[l] > lower_limit):
                    temp = lower_limit - non_zero_ruc_array_copied[l]
                    costSum += abs(pow(temp, 2)) / 2
                    cost_count = cost_count + 1
                else:
                    temp = lower_limit - non_zero_ruc_array_copied[l]
                    pSum += abs(pow(temp, 2)) * 2
                    penalty_count = penalty_count + 1
            print("Cost Count: ", cost_count, " Penalty Count: ", penalty_count)
            # taoSumDiff = abs(pSum - costSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
            #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
            taoSumDiff = pow((costSum - pSum), 2)
            difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)

        lists = (difference.items())  # sorted by key, return a list of tuples
        x, y = zip(*lists)  # unpack a list of pairs into two tuples
        return y

def calculate_min_by_gradient_sqr_res_inv_sqr(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    target_ruc_num = 0
    # lower_limit = -0.090
    target_t_min = -0.17
    is_visited = False
    rucFrame_array = []
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array = rucFrame_array[0].tolist()
    for x in rucFrame_array[1]:
        merged_array.append(x)
    print("length merged: ",len(merged_array)," lenght 1: ",len(rucFrame_array[0])," length 2: ",len(rucFrame_array[1]))
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(merged_array))]
    for l in range(len(merged_array)):
        if (merged_array[l] > lower_limit):
            temp = lower_limit - merged_array[l]
            costSum += abs(pow(temp, 2)) / 2
            cost_count = cost_count+ 1
        else:
            temp = lower_limit - merged_array[l]
            pSum += abs(pow(temp, 2)) * 2
            penalty_count = penalty_count + 1
        # costFunction[l] =  abs(costSum-pSum + (cost_count-penalty_count)*(lower_limit))
        # costFunction[l] =  abs(costSum-pSum)
        # costFunction[l] =  pow((costSum - pSum), 2)
        costFunction[l] =  abs(costSum - pSum)
    # print("org Cost Count: ", cost_count," Penalty Count: ",penalty_count)
    index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,merged_array) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = return_gradients_for_two_function(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_min_by_manual(non_zero_ruc_array)  # Gradient by manual process
    non_zero_ruc_array_copied = merged_array.copy()
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]<0):
            sign = (-1)*1
        else:sign = 1
        # print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  +\
        #                                                      (abs(lower_limit -non_zero_ruc_array_copied[index_of_sorted_list[i]] ) +0.0875)
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + \
        #                                                      (abs(lower_limit - non_zero_ruc_array_copied[index_of_sorted_list[i]]) + 0.03)*sign
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.15 * sign
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.0875
        # print(non_zero_ruc_array_copied)
        tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # print("Tao min", tmin)
        if ((tmin < target_t_min) and (is_visited == False)):
            is_visited = True
            target_ruc_num = i
            print("For altering: ", i, "Number of RUCs from ", len(non_zero_ruc_array_copied),
                  " we can reach the targeted max")
            # break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] > lower_limit):
                temp = lower_limit - non_zero_ruc_array_copied[l]
                costSum += abs(pow(temp, 2)) / 2
                cost_count = cost_count + 1
            else:
                temp = lower_limit - non_zero_ruc_array_copied[l]
                pSum += abs(pow(temp, 2)) * 2
                penalty_count = penalty_count + 1
        print("Cost Count: ", cost_count, " Penalty Count: ", penalty_count)
        # taoSumDiff = abs(pSum - costSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        # taoSumDiff = pow((costSum - pSum), 2)
        taoSumDiff = abs(costSum - pSum)
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)

    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    # return non_zero_ruc_array_copied
    return y,target_ruc_num

def calculate_optimal_min_threshold_sqr_res_inv_sqr(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    # lower_limit = -0.090
    target_t_min = -0.2
    previous_diff = 0
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc[["ruc" + key, "day"]].values
        print("Lenth of Non zero ruc foe key  ", key, " is: ", len(non_zero_ruc_array))
        non_zero_ruc_array_copied = non_zero_ruc_array
        for i in range(len(non_zero_ruc_array_copied)):
            costSum = 0
            pSum = 0
            cost_count = 0
            penalty_count = 0
            non_zero_ruc_array_copied[i][0] = non_zero_ruc_array_copied[i][0] - 0.0875
            # tmin = return_new_tao_min(non_zero_ruc_array_copied)
            # if(tmin<target_t_min):
            #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            #     break
            for l in range(len(non_zero_ruc_array_copied)):
                if (non_zero_ruc_array_copied[l][0] > lower_limit):
                    temp = lower_limit - non_zero_ruc_array_copied[l][0]
                    costSum += abs(pow(temp, 2))  / 2
                    cost_count = cost_count + 1
                else:
                    temp = lower_limit - non_zero_ruc_array_copied[l][0]
                    pSum += abs(pow(temp, 2)) * 2
                    penalty_count = penalty_count +1
            # taoSumDiff = abs(costSum - pSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # taoSumDiff = abs(costSum - pSum )  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            taoSumDiff = pow((costSum - pSum), 2)
            # taoSumDiff = abs(costSum - pSum + (i + 1) * 0.177)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        lists = (difference.items())  # sorted by key, return a list of tuples
        x, y = zip(*lists)  # unpack a list of pairs into two tuples
        return y
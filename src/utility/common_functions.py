import csv
from functools import reduce
from random import random, randint, sample
import numpy
import pandas as pd
from scipy import stats
from sklearn.model_selection import GroupShuffleSplit

from src.loss_minimization import loss_minimization
from src.utility.constant import *
from scipy.stats.mstats import winsorize
# import statsmodels.api as sm
import sys
import pylab as py
from math import exp, log
import matplotlib.pyplot as plt
#-0.1275 or -0.1025 0.0975 #hyper parameter 6
#-0.13 or -0.1075  0.0975 #hyper parameter 8
#-0.12 or -0.09  0.09 #hyper parameter 4
#l1(-0.09 or -065 0.065) l2(-0.08 0.08) #hyper parameter 2
upper_limit = 0.01
lower_limit = -0.01

def box_cox_transformation(data):
    for key in data.keys():
        # print("before transformation")
        # print(data[key].head(3))
        data[key].use = stats.boxcox(data[key].use, lmbda=LAMBDA, alpha=None)
        # print(data[key][data[key]['use']<0])
    return data


def log_transformation(data):
    for key in data.keys():
        # print("before transformation")
        # print(data[key].head(3))
        data[key]['use'] = data[key].apply(lambda x: numpy.log(x['use'] + 2), axis=1)
        # print("after transformation")
        # print(data[key].head(3))
    return data


def read_data(filePath, year):
    file = pd.read_csv(filePath)  # read file from the abs dir
    file['localminute'] = pd.to_datetime(file['localminute'].str.slice(0, 19))
    file['use'] = file['use'] * 1000
    file = file[(file[['use']] > 50).all(axis=1)]
    # mask_less_50 = (file['use'] <= 50)
    # df = file.loc[mask_less_50]
    # # replacing all values less than 50 by a random number 10-50
    # file.loc[mask_less_50, 'use'] = df['use'].apply(lambda x: randint(20, 50))
    file = file[file['localminute'].dt.year == int(year)]
    # print("length file: ", len(file))
    return file


def winsorize_data(file_data_transformed):
    for key in file_data_transformed.keys():
        file_data_transformed[key].use = winsorize(file_data_transformed[key].use, limits=[0.05, 0.05])
    return file_data_transformed


# def calculate_AM_HM_ratio(data):
#     frame_array = []
#     for key in data.keys():
#         ratio_array = []
#         yearly_data = data[key] #yearly data
#         month_wise_group_data = yearly_data.groupby(pd.Grouper(key='localminute', freq='1M')) #monthwise grouping the data
#         for key, monthly_data in month_wise_group_data: # iterate each month data
#             if len(monthly_data)>0: #check if there is data for the month
#                 day_wise_group_data = monthly_data.groupby(pd.Grouper(key='localminute', freq='1D')) #group the montly data by day
#                 for key, daily_data in day_wise_group_data: #iterate each day of the month
#                     if len(daily_data)>0: #check if the day wise data count is greater than zero
#                         hour_wise_group_Data = daily_data.groupby(pd.Grouper(key='localminute', freq='1H')) # group the daily data into hourlwise
#                         AM_per_day= 0
#                         HM_per_day = 0
#                         for key, hourly_data in hour_wise_group_Data: #iterate each hourly data of the day
#                             if len(hourly_data)>0: #check if the hour has data count greater than zero
#                                 arithmatic_sum = 0
#                                 harmonic_sum = 0
#                                 for row in hourly_data.itertuples():
#                                     if (row.use > 0):
#                                         arithmatic_sum += row.use  # calculate arithmatic sum
#                                         harmonic_sum += 1 / row.use
#                                 N = len(hourly_data.dataid.unique())
#                                 harmonic_mean = N / harmonic_sum  # harmonic mean hourly
#                                 arithmatic_mean = arithmatic_sum / N  # arithmatic mean hourly
#                                 AM_per_day +=arithmatic_mean
#                                 HM_per_day +=harmonic_mean
#                         day = (key.month-1)*30+key.day
#                         hm_am_ratio_per_day = HM_per_day / AM_per_day # use the ratio means of 24 hours of a day to calculate day wise ratio means
#                         temp = {'day': day,'month': key.month,'year': key.year, 'ratio'+str(key.year): hm_am_ratio_per_day}
#                         ratio_array.append(temp)
#
#         ratio_frame = pd.DataFrame(ratio_array)
#         frame_array.append(ratio_frame)
#     return frame_array

def calculateAMAndHM(data):  # the below code contains the slicing of the data by month day and hour wise
    # i have calculated ratio means for each of the time frame or window like hour,day,monthly but havent stored it yer
    # print(" Started Calculating the arithmatic and harmonic mean for hourly data")
    frame_array = []
    for key in data.keys():
        dateRatioArray = []
        # print("data after transformation")
        # print(data[key].head(3))
        day = 1
        yearlyData = data[key]  # yearly data
        monthwisegroupData = yearlyData.groupby(pd.Grouper(key='localminute', freq='1M'))  # monthwise grouping the data
        for key, monthlydata in monthwisegroupData:  # iterate each month data
            if len(monthlydata) > 0:  # check if there is data for the month
                daywisegroupData = monthlydata.groupby(
                    pd.Grouper(key='localminute', freq='1D'))  # group the montly data by day
                for key, dailyData in daywisegroupData:  # iterate each day of the month
                    if len(daywisegroupData) > 0:  # check if the day wise data count is greater than zero
                        hourwiseGroupData = dailyData.groupby(
                            pd.Grouper(key='localminute', freq='1H'))  # group the daily data into hourlwise
                        hourwiseSumAM = 0
                        hourwiseSumHM = 0
                        for key, hourlyData in hourwiseGroupData:  # iterate each hourly data of the day
                            if len(hourlyData) > 0:  # check if the hour has data count greater than zero
                                am_sum = 0
                                hm_sum = 0
                                for row in hourlyData.itertuples():
                                    if (getattr(row, "use") > 0):
                                        am_sum += getattr(row, "use")  # calculate arithmatic sum
                                        hm_sum += 1 / getattr(row, "use")  # calculate harmonic sum
                                N = len(hourlyData.dataid.unique())
                                # print("Unique Meters",N)
                                hourlyAttackHM = N / hm_sum  # harmonic mean hourly
                                hourlyAttackAM = am_sum / N  # arithmatic mean hourly
                                # print("AMsum: ", hourlyAttackAM, " HMSum: ", hourlyAttackHM)
                                hourwiseSumAM += hourlyAttackAM
                                hourwiseSumHM += hourlyAttackHM
                        year = int(key.year)
                        month = key.month
                        daywiseRatioMean = hourwiseSumHM / hourwiseSumAM  # use the ratio means of 24 hours of a day to calculate day wise ratio means
                        # print("daywiseRatioMean: ",daywiseRatioMean)
                        # day = (month-1) * MONTH_DAY_MAP[month] + key.day
                        temp = {'year': year, 'month': month, 'day': day, 'ratio' + str(key.year): daywiseRatioMean}
                        day = day + 1
                        # print(temp)
                        dateRatioArray.append(temp)
        dateRatioFrame = pd.DataFrame(dateRatioArray)
        # print(dateRatioFrame.head(2))
        frame_array.append(dateRatioFrame)
        # region write into file
        # print(dateRatioFrame.head(2))
        # filename = key.strftime("%Y")
        # f = open(filename, "a")
        # f.write(dateRatioFrame)
        # dateRatioFrame.to_csv(r'../../resource/'+filename, index=None, header=True)
        # endregion
        # commented 08/17
        # plot = dateRatioFrame.plot(kind="line", x="day", y="ratio"+str(key.year), color="blue",
        #                            xlim=(START, END),
        #                            ylim =(.85, 1.00),
        #                            title =str(key.year)+ ' ratio plot vs day')#08/17
    #
    # plt.plot(frame_array[0]["day"], frame_array[0]["ratio2014"], color='r', label='2014 ratio')  # r - red colour
    # plt.plot(frame_array[1]["day"], frame_array[1]["ratio2015"], color='g', label='2015 ratio')  # r - red colour
    # plt.xlabel("Number of Day ")
    # plt.ylabel("Ratio")
    # plt.legend()
    # plt.grid()
    # plt.show()
    return frame_array


def calculate_safe_margin(data_frame, mad, threshold):
    data_frame['margin_high'] = data_frame['mean_ratio'] + threshold * mad
    data_frame['margin_low'] = data_frame['mean_ratio'] - threshold * mad
    return data_frame

def perform_additive_attack(file_data, romal, delavg, START_DATE, END_DATE):
    file_data_copied = {}
    for key in file_data.keys():
        file_data_copied[key] = file_data[key].copy()
    attack_window_data = file_data_copied[str(ATTACK_YEAR)]
    allMeters = attack_window_data.dataid.unique()
    # print("Number of meteres", len(allMeters))
    numberofitems = (len(allMeters) * romal)
    # print("number of items to sample", numberofitems)
    compromisedMeters = sample(allMeters.tolist(), int(numberofitems))
    # print("Number of compromised meteres", len(compromisedMeters))
    mask = (attack_window_data['localminute'] >= START_DATE) & \
           (attack_window_data['localminute'] <= END_DATE) & \
           (attack_window_data['dataid'].isin(compromisedMeters))
    df = attack_window_data.loc[mask]
    s = delavg - 50
    e = delavg + 50
    attack_window_data.loc[mask, 'use'] = df['use'].apply(lambda x: x + randint(s, e))
    return file_data_copied, numberofitems

def perform_deductive_attack(file_data, romal, delavg, START_DATE, END_DATE):
    file_data_copied = {}
    for key in file_data.keys():
        file_data_copied[key] = file_data[key].copy()
    attack_window_data = file_data_copied[str(ATTACK_YEAR)]
    allMeters = attack_window_data.dataid.unique()
    # print("Number of meteres", len(allMeters))
    numberofitems = (len(allMeters) * romal)
    # print("number of items to sample", numberofitems)
    compromisedMeters = sample(allMeters.tolist(), int(numberofitems))
    # print("Number of compromised meteres", len(compromisedMeters))
    mask = (attack_window_data['localminute'] >= START_DATE) & \
           (attack_window_data['localminute'] <= END_DATE) & \
           (attack_window_data['dataid'].isin(compromisedMeters))
    df = attack_window_data.loc[mask]
    # s = delavg - 10
    # e = delavg + 10
    # attack_window_data.loc[mask, 'use'] = df['use'].apply(lambda x: x - randint(s, e))
    # #this does not create strict pattern
    attack_window_data.loc[mask, 'use'] = df['use'].apply(lambda x: x - delavg)
    return file_data_copied, numberofitems


# half of the meters selected for adversarial attack will take deductive_RUC attack
# romal/2 will be in deductive_RUC and romal/2 will be in addtive
# while other half of the meters will introduce the additive_RUC attack for same del avg
def perform_camouflage_attack(file_data, romal, delavg, START_DATE, END_DATE):
    file_data_copied = {}
    for key in file_data.keys():
        file_data_copied[key] = file_data[key].copy()
    attack_window_data = file_data_copied[str(ATTACK_YEAR)]
    allMeters = attack_window_data.dataid.unique()
    # print("Number of meteres", len(allMeters))
    numberofitems = (len(allMeters) * romal)
    # print("number of items to sample", numberofitems)
    compromisedMeters = sample(allMeters.tolist(), int(numberofitems))
    # print("Number of compromised meteres", len(compromisedMeters))
    # START_DATE = '2014-01-01'
    # END_DATE = '2014-06-01'
    mask = (attack_window_data['localminute'] >= START_DATE) & \
           (attack_window_data['localminute'] < END_DATE) & \
           (attack_window_data['dataid'].isin(compromisedMeters))
    # mask2 = (attack_window_data['localminute'] >= '2014-04-01') & \
    #         (attack_window_data['localminute'] <= END_DATE) & \
    #         (attack_window_data['dataid'].isin(compromisedMeters))
    df = attack_window_data.loc[mask]
    gss = GroupShuffleSplit(n_splits=1, test_size=0.5)
    idx1, idx2 = next(gss.split(df, groups=df.dataid))
    df1, df2 = df.iloc[idx1], df.iloc[idx2]
    s = delavg - 50
    e = delavg + 50
    df1['use'] = df1['use'].apply(lambda x: x - randint(s, e))
    df2['use'] = df2['use'].apply(lambda x: x + randint(s, e))
    df.iloc[idx1] = df1
    df.iloc[idx2] = df2
    attack_window_data.loc[mask, 'use'] = df['use']
    # df2 = attack_window_data.loc[mask2]
    # attack_window_data.loc[mask2, 'use'] = df2['use'].apply(lambda x: x - randint(100, 150))
    return file_data_copied, numberofitems


def calculate_nabla(ratio_frame, keys):
    nabla = []
    for row in ratio_frame.itertuples():
        # print(row)
        temp = {}
        for key in keys:
            # print("key: ",key)
            # if(getattr(row,'ratio'+key)<.92):
            #     print("day: ",row.day," ratio:",getattr(row,'ratio'+key))
            if (getattr(row, 'ratio' + key) > getattr(row, 'margin_high')):
                # print("day: ", row.day, "nabla: ",key, " ratio:",getattr(row, 'ratio'+key), 'margin_high:',getattr(row, 'margin_high')," magnitude:",(getattr(row, 'ratio' + key) - getattr(row, 'margin_low')))
                # ,"margin_high",getattr(row, 'margin_high'),"difference",
                #       (getattr(row, 'ratio'+key) - getattr(row, 'margin_high')))
                temp.update({'nabla' + key: (getattr(row, 'ratio' + key) - getattr(row, 'margin_high'))})
            elif (getattr(row, 'ratio' + key) < getattr(row, 'margin_low')):
                # print("day: ", row.day, "nabla: ", key, " ratio:", getattr(row, 'ratio' + key),'margin_low:',getattr(row, 'margin_low')," magnitude:",(getattr(row, 'ratio' + key) - getattr(row, 'margin_low')))
                #       , "margin_low", getattr(row, 'margin_low'),"difference",
                #       (getattr(row, 'ratio' + key) - getattr(row, 'margin_low')))
                temp.update({'nabla' + key: (getattr(row, 'ratio' + key) - getattr(row, 'margin_low'))})
            else:
                temp.update({'nabla' + key: 0})
        # print(row)
        # temp.update({'day': row.day, 'month': row.month})
        temp.update({'day': row.day})
        nabla.append(temp)
    nabla_frame = pd.DataFrame(nabla)
    # print("nabla_frame",nabla_frame)
    return nabla_frame


def calculate_ruc(nabla_frame, keys):
    ruc_arry = []
    # print("nabla vlaues")
    for row in nabla_frame.itertuples():
        # print("row Index:",row.Index)
        temp = {}
        if (row.day <= SLIDING_FRAME):
            for key in keys:
                temp.update({'ruc' + key: getattr(row, 'nabla' + key)})
                # if(getattr(row,'nabla'+key)!=0):
                #     print("ruc: ","key: ",key,getattr(row,'nabla'+key)," day:", str(row.day))
            # temp.update({'day': row.day, 'month': row.month})
            temp.update({'day': row.day})
            ruc_arry.append(temp)
        else:
            for key in keys:
                sliding_frame_rows = nabla_frame.loc[(nabla_frame['day'] <= row.day)
                                                     & (nabla_frame['day'] > (row.day - SLIDING_FRAME))]
                ruc = 0
                count = 0
                # print(len(sliding_frame_rows))
                for row1 in sliding_frame_rows.itertuples():
                    # print(getattr(row1, 'nabla' + key) ," +")
                    ruc = ruc + getattr(row1, 'nabla' + key)
                    count = count +1
                # print("key: ",key,' day: ',row.day," ruc: ",ruc)
                # nabla_frame.at[row.Index, 'nabla'+key] = ruc
                temp.update({'ruc' + key: ruc})
                # if(ruc!=0):
                #     print("day:",row.day," key:",key," ruc:",ruc)
            # temp.update({'day': row.day, 'month': row.month})
            temp.update({'day': row.day})
            ruc_arry.append(temp)
    ruc_frame = pd.DataFrame(ruc_arry)
    # print("ruc_frame values that are less than zero")
    # print(ruc_frame.loc[ruc_frame["ruc2014"]<0])
    return ruc_frame


# calculate ruc by using inverse power law
def inversePowerLaw(nablaTFrame, keys):
    # print(nablaTFrame)
    rucArray = []  # we have fs days = 7 means see 7 days ahead
    for row in nablaTFrame.itertuples():
        temp = {}
        if (row.day <= SLIDING_FRAME):
            for key in keys:
                temp.update({'ruc' + key: getattr(row, 'nabla' + key)})
            temp.update({'day': row.day, 'month': row.month})
            rucArray.append(temp)
            # temp = {'month': row.month, 'day': row.day, 'ruc2014': row.nabla2014, 'ruc2015': row.nabla2015}
            # rucArray.append(temp)
            # continue
        else:
            for key in keys:
                sliding_frame_rows = nablaTFrame.loc[(nablaTFrame['day'] <= row.day)
                                                     & (nablaTFrame['day'] > (row.day - SLIDING_FRAME))]
                ruc = 0
                for row1 in sliding_frame_rows.itertuples():
                    # ruc = ruc + getattr(row1,'nabla'+key)
                    ruc += getattr(row1, 'nabla' + key) * (1 / pow(SLIDING_FRAME, ITA)) * \
                           pow((row1.day - (row.day - SLIDING_FRAME)), ITA)
                temp.update({'ruc' + key: ruc})
            temp.update({'day': row.day, 'month': row.month})
            rucArray.append(temp)
        # tempFSFrame = nablaTFrame[
        #     (nablaTFrame.day >= (row.day - SLIDING_FRAME + 1)) & (nablaTFrame.day <= row.day)]
        # # print("Lenght of a frame: ",len(tempFSFrame))
        # # here the number of frames should be 7 I am getting 1364  and 1024
        # nablaSum2014 = 0
        # nablaSum2015 = 0
        # for index, row1 in tempFSFrame.iterrows():
        #     # row1.nabla2014*(1/pow(Constant.SLIDING_FRAME,Constant.itaParam))*pow((row1.day -(row.day - Constant.SLIDING_FRAME)),Constant.itaParam)
        #     # nablaSum2014 += row1.nabla2014
        #     #ITA Means the greek letter ita
        #     nablaSum2014 += row1.nabla2014 * (1 / pow(SLIDING_FRAME, ITA)) * \
        #                     pow((row1.day - (row.day - SLIDING_FRAME)), ITA)
        #     nablaSum2015 += row1.nabla2015 * (1 / pow(SLIDING_FRAME, ITA)) * \
        #                     pow((row1.day - (row.day - SLIDING_FRAME)), ITA)
        # temp1 = {'month': row.month, 'day': row.day, 'ruc2014': nablaSum2014, 'ruc2015': nablaSum2015}
        # rucArray.append(temp1)
    rucFrame = pd.DataFrame(rucArray)
    return rucFrame


# this is the weight for the ruc calculated by inverse power law
def weightForRuc(rucByIPL, keys):
    weightRucs = []
    for row in rucByIPL.itertuples():
        # print("row.ruc2014: ",row.ruc2014)
        # logRuc2014 = log(abs(row.ruc2014)) *(-1)
        # here now we are calculating for one alpha later implementation might need for all alpha
        temp = {}
        for key in keys:
            if (getattr(row, 'ruc' + key) == 0):
                expon = 0
            else:
                expon = ((-1) * BETA) * pow(((-1) * log(abs(getattr(row, 'ruc' + key)))), ALPHA)
            w = exp(expon)
            temp.update({'w' + key: w})
            # temp1 = {'month': row.month, 'day': row.day, 'w2014': w2014, 'w2015': w2015}
        temp.update({'day': row.day, 'month': row.month})
        # # if(row.ruc2015 == 0):
        # #     expon2015 = 0
        # # else:
        # #     expon2015 = ((-1) * BETA) * pow(((-1) * log(abs(row.ruc2014))), ALPHA)
        #
        # w2015 = exp(expon2015)
        # temp1 = {'month': row.month, 'day': row.day, 'w2014': w2014, 'w2015': w2015}
        weightRucs.append(temp)
    wFrame = pd.DataFrame(weightRucs)
    return wFrame


# below calculate modified robust ruc by the weights and ruc by inverse power law
def newModifiedRUC(ruc, weightRuc, keys):
    # print(nablaTFrame)
    weightAndRuc = pd.merge(ruc, weightRuc, on=['month', 'day'])
    rucArray = []  # we have fs days = 7 means see 7 days ahead
    for row in weightAndRuc.itertuples():
        temp = {}
        if (row.day <= SLIDING_FRAME):
            for key in keys:
                temp.update({'mruc' + key: getattr(row, 'ruc' + key)})
            temp.update({'day': row.day, 'month': row.month})
            rucArray.append(temp)
            # temp = {'month': row.month, 'day': row.day,
            #         'ruc2014': row.ruc2014, 'ruc2015': row.ruc2015,
            #         'w2014': row.w2014, 'w2015': row.w2015}
            # rucArray.append(temp)
            # continue
        else:
            for key in keys:
                sliding_frame_rows = weightAndRuc.loc[(weightAndRuc['day'] <= row.day)
                                                      & (weightAndRuc['day'] > (row.day - SLIDING_FRAME))]
                mruc = 0
                for row1 in sliding_frame_rows.itertuples():
                    mruc += getattr(row1, 'ruc' + key) * getattr(row1, 'w' + key)
                temp.update({'mruc' + key: mruc})
            temp.update({'day': row.day, 'month': row.month})
            rucArray.append(temp)
        # tempFSFrame = weightAndRuc[
        #     (weightAndRuc.day >= (row.day - SLIDING_FRAME + 1)) & (weightAndRuc.day <= row.day)]
        # # print("Lenght of a frame: ",len(tempFSFrame))
        # # here the number of frames should be 7 I am getting 1364  and 1024
        # modifiedRuc2014 = 0
        # modifiedRuc2015 = 0
        # for index, row1 in tempFSFrame.iterrows():
        #     modifiedRuc2014 += row1.ruc2014 * row1.w2014
        #     modifiedRuc2015 += row1.ruc2015 * row1.w2015
        # temp1 = {'month': row.month, 'day': row.day,
        #          'mruc2014': modifiedRuc2014, 'mruc2015': modifiedRuc2015,
        #          }
        # rucArray.append(temp1)
    rucFrame = pd.DataFrame(rucArray)
    return rucFrame

def calculate_max_by_gradient_no_shift(rucFrame, keys):
    target_t_max = 0.0675
    difference = dict()
    target_num_ruc = 0
    difference = dict()
    rucFrame_array = []
    sorted_frame = dict()
    target_ruc_num = 0
    is_visited = False
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key],ascending=False)
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] > 0]
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array_1 = rucFrame_array[0].tolist()
    for x in rucFrame_array[1]:
        merged_array_1.append(x)
    # print("length merged: ", len(merged_array_1), " lenght 1: ", len(rucFrame_array[0]), " length 2: ",
    #       len(rucFrame_array[1]))
    # fig, ax = plt.subplots()
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(merged_array_1))]
    for l in range(len(merged_array_1)):
        if (merged_array_1[l] < upper_limit):
            costSum += abs(upper_limit - merged_array_1[l]) / 2
            # costFunction[l] = abs(upper_limit - merged_array_1[l]) / 2
            cost_count = cost_count + 1
        else:
            pSum += abs(upper_limit - merged_array_1[l]) * 2
            # costFunction[l] = abs(upper_limit - merged_array_1[l]) * 2
            penalty_count = penalty_count + 1
        cost_norm = costSum
        penalty_norm = pSum
        if (cost_count > 0):
            cost_norm = costSum / cost_count
        if (penalty_count > 0):
            penalty_norm = pSum / penalty_count
        costFunction[l] = abs(cost_norm - penalty_norm)
        # costFunction[l] =  abs(costSum-pSum)
        # costFunction[l] =  abs(costSum-pSum)/(cost_count+penalty_count)
    index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,merged_array_1) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_max_by_manual(non_zero_ruc_array)  # Gradient by manual process
    non_zero_ruc_array_copied = merged_array_1.copy()
    max = numpy.max(non_zero_ruc_array_copied)
    print("MAX: ",max)
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]< 0):
            sign = (-1)*1
        else:sign = 1
        # print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        if (max < (non_zero_ruc_array_copied[index_of_sorted_list[i]] + 0.0025)):
            continue
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]+0.0025
        # print(non_zero_ruc_array_copied)
        print(i,"th gradient: ",gradients[index_of_sorted_list[i]]," residual: ",non_zero_ruc_array_copied[index_of_sorted_list[i]])

        tmax = return_new_tao_max(non_zero_ruc_array_copied)
        # print("Tao max", tmax)
        if ((tmax > target_t_max) and (is_visited == False)):
            is_visited = True
            target_num_ruc = i
            # print("For altering: ", i, "Number of RUCs from ", len(non_zero_ruc_array_copied),
            #       " we can reach the targeted max")
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                costSum += abs(upper_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                pSum += abs(upper_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        # print("cost: ",costSum," penalty: ",pSum)
        # taoSumDiff = abs(pSum - costSum + (cost_count - penalty_count)* upper_limit)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        # print("Cost count:", cost_count, " penalty count: ", penalty_count)
    mean = numpy.mean(non_zero_ruc_array_copied)
    print(mean)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    # print("Loss When points are taken according to the order of gradient")
    print("Target number of ruc: ",target_num_ruc)
    return y,target_num_ruc

def calculate_max_by_RUC_order(rucFrame, keys):
    target_t_max = 0.0675
    difference = dict()
    target_num_ruc = 0
    difference = dict()
    rucFrame_array = []
    sorted_frame = dict()
    target_ruc_num = 0
    is_visited = False
    day_frame_array = []
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key],ascending=False)
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] > 0]
        day_values = sorted_non_zero_ruc['day'].values
        day_frame_array.append(day_values)
        # print(sorted_non_zero_ruc)
        # print("\n")
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array_1 = rucFrame_array[0].tolist()
    day_array = day_frame_array[0].tolist()
    for x in day_frame_array[1]:
        day_array.append(x)
    for x in rucFrame_array[1]:
        merged_array_1.append(x)
    print("length merged: ", len(merged_array_1), " lenght 1: ", len(rucFrame_array[0]), " length 2: ",
          len(rucFrame_array[1]))
    non_zero_ruc_array_copied = merged_array_1.copy()
    max = numpy.max(non_zero_ruc_array_copied)
    print("Max:",max)
    for i in range(len(non_zero_ruc_array_copied)):
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        if (max < (non_zero_ruc_array_copied[i] + 0.0025)):
            continue
        non_zero_ruc_array_copied[i] = non_zero_ruc_array_copied[i]  + 0.0025
        print(i,"th residual: ",non_zero_ruc_array_copied[i])
        tmax = return_new_tao_max(non_zero_ruc_array_copied)
        if(tmax>target_t_max and is_visited == False):
            target_ruc_num = i
            is_visited = True
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                costSum += abs(upper_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                pSum += abs(upper_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    print("Target ruc number is ", target_ruc_num)
    mean = numpy.mean(non_zero_ruc_array_copied)
    print(mean)
    return y,target_ruc_num

def calculate_max_by_gradient(rucFrame, keys):
    target_t_max = 0.0675
    difference = dict()
    target_num_ruc = 0
    difference = dict()
    rucFrame_array = []
    sorted_frame = dict()
    target_ruc_num = 0
    is_visited = False
    day_frame_array = []
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key],ascending=False)
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] > 0]
        day_values = sorted_non_zero_ruc['day'].values
        day_frame_array.append(day_values)
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array_1 = rucFrame_array[0].tolist()
    day_array = day_frame_array[0].tolist()
    for x in day_frame_array[1]:
        day_array.append(x)
    for x in rucFrame_array[1]:
        merged_array_1.append(x)
    print("length merged: ", len(merged_array_1), " lenght 1: ", len(rucFrame_array[0]), " length 2: ",
          len(rucFrame_array[1]))
    # mean = numpy.mean(merged_array_1)
    # std = numpy.std(merged_array_1)
    # print("Mean: ",mean," std:",std)
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(merged_array_1))]
    for l in range(len(merged_array_1)):
        if (merged_array_1[l] < upper_limit):
            costSum += abs(upper_limit - merged_array_1[l]) / 2
            # costFunction[l] = abs(upper_limit - merged_array_1[l]) / 2
            cost_count = cost_count + 1
        else:
            pSum += abs(upper_limit - merged_array_1[l]) * 2
            # costFunction[l] = abs(upper_limit - merged_array_1[l]) * 2
            penalty_count = penalty_count + 1
        cost_norm = costSum
        penalty_norm = pSum
        if(cost_count>0):
            cost_norm  = costSum / cost_count
        if(penalty_count>0):
            penalty_norm = pSum/penalty_count
        costFunction[l] =  abs(cost_norm-penalty_norm)
        # costFunction[l] =  abs(costSum-pSum)
        # costFunction[l] =  abs(costSum-pSum)/(cost_count+penalty_count)
    # print("org Cost count:",cost_count," penalty count: ",penalty_count)
    # print(costFunction)
    index_of_sorted_list,gradients = return_gradients_by_cost_function_signed(costFunction,merged_array_1) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_max_by_manual(non_zero_ruc_array)  # Gradient by manual process
    non_zero_ruc_array_copied = merged_array_1.copy()
    max = numpy.max(non_zero_ruc_array_copied)
    # max  = numpy.mean(non_zero_ruc_array_copied)
    print("Max: ",max)
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]< 0):
            sign = (-1)*1
        else:sign = 1
        # print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        if(max< (non_zero_ruc_array_copied[index_of_sorted_list[i]]  + 0.0025)):
            continue
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  + 0.0025
        print(i,"th gradient: ",gradients[index_of_sorted_list[i]]," residual: ",non_zero_ruc_array_copied[index_of_sorted_list[i]])
        tmax = return_new_tao_max(non_zero_ruc_array_copied)
        # print("Tmax:",tmax," target is : ",target_t_max)
        if(tmax>target_t_max and is_visited == False):
            # print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            target_ruc_num = i
            is_visited = True
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                costSum += abs(upper_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                pSum += abs(upper_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        # print("cost: ",costSum," penalty: ",pSum)
        # taoSumDiff = abs(pSum - costSum + (cost_count - penalty_count)* upper_limit)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        # print("Cost count:", cost_count, " penalty count: ", penalty_count)

    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    # print("Loss When points are taken according to the order of gradient")
    mean = numpy.mean(non_zero_ruc_array_copied)
    print(mean)
    print("Target ruc number is ", target_ruc_num)
    return y,target_ruc_num
    # region normalization
    # max_y  = max(y)
    # min_y = min(y)
    # print(max_y,min_y)
    # print("before")
    # print(y)
    # y_bar = [(i-min_y)/(max_y-min_y) for i in y if len(y)>1]
    # print ("after")
    # print(y_bar)
    # endregion
    # fig = plt.figure()
    # plt.scatter(x, y, marker='o')
    # fig.suptitle('TAO_MIN Loss = Cost - Penalty Plotting', fontsize=20)
    # plt.xlabel('Total Ruc Change')
    # plt.ylabel('Loss = Cost - Penalty')
    # # fig.savefig('test.jpg')
    # plt.show()

def calculate_optimal_max_threshold(rucFrame, keys):
    # difference = dict()
    # upper_limit = 0.065
    target_t_min = 0.0675
    difference = dict()
    sorted_frame = rucFrame.sort_values(by=["ruc2015"],ascending=False)
    sorted_non_zero_ruc = sorted_frame[sorted_frame['ruc2015'] > 0]
    non_zero_ruc_array = sorted_non_zero_ruc[["ruc2015"]].values
    # print("Lenth of Non zero ruc foe key  ", 2015, " is: ", len(non_zero_ruc_array))
    # print(non_zero_ruc_array)
    # print(sorted_non_zero_ruc)
    non_zero_ruc_array_copied = non_zero_ruc_array.copy()
    for i in range(len(non_zero_ruc_array_copied)):
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        non_zero_ruc_array_copied[i] = non_zero_ruc_array_copied[i] + 0.03
        # tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # if(tmin<target_t_min):
        #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
        #     break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] < upper_limit):
                costSum += abs(upper_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                pSum += abs(upper_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        # print("cost: ", costSum, " penalty: ", pSum)
        # taoSumDiff = abs(costSum - pSum + (cost_count - penalty_count)*upper_limit )  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = abs(costSum - pSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(costSum - pSum + (i + 1) * 0.177)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("Cost-penalty: ",taoSumDiff)
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        # previous_diff = taoSumDiff
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    # print("Loss When Points are taken according to RUC value")
    # print(y)
    return y
    # region normalization
    # max_y  = max(y)
    # min_y = min(y)
    # print(max_y,min_y)
    # print("before")
    # print(y)
    # y_bar = [(i-min_y)/(max_y-min_y) for i in y if len(y)>1]
    # print ("after")
    # print(y_bar)
    # endregion
    # fig = plt.figure()
    # plt.scatter(x, y, marker='o')
    # fig.suptitle('Tao min Cost - Penalty Plotting', fontsize=20)
    # plt.xlabel('Total Ruc Change')
    # plt.ylabel('Cost - Penalty')
    # # fig.savefig('test.jpg')
    # plt.show()

def return_new_tao_min(non_zero_ruc_array_copied):
    minSum = sys.float_info.max
    minThreshold = non_zero_ruc_array_copied[0]
    # print("minThreshold: ",minThreshold)
    # print(non_zero_ruc_array_copied)
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        pSum = 0
        for row in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[row] > ((-1) * taoThreshold)):
                costSum += abs(taoThreshold*(-1) - non_zero_ruc_array_copied[row]) / 2
                # costSum += pow(abs(taoThreshold*(-1) - non_zero_ruc_array_copied[row]) / 2,2)
            else:
                pSum += abs(taoThreshold*(-1) - non_zero_ruc_array_copied[row]) * 2
                # pSum +=pow(abs(taoThreshold*(-1) - non_zero_ruc_array_copied[row]) * 2,2)
        # if (costSum != 0 or pSum != 0):
            # print("Both are not zero at least once")
        taoSumDiff = abs(costSum - pSum)
        difference[taoThreshold] = taoSumDiff
        if (minSum > taoSumDiff):
            minSum = taoSumDiff
            minThreshold = taoThreshold * (-1)
    return minThreshold


def return_new_tao_max(non_zero_ruc_array_copied):
    maxSum = sys.float_info.max
    maxThreshold = non_zero_ruc_array_copied[0]
    # print("minThreshold: ",minThreshold)
    # print(non_zero_ruc_array_copied)
    difference = dict()
    for taoThreshold in numpy.arange(0.0025, .500, 0.0025):
        costSum = 0
        pSum = 0
        for row in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[row] < taoThreshold):
                costSum += abs(taoThreshold - non_zero_ruc_array_copied[row]) / 2
            else:
                pSum += abs(taoThreshold - non_zero_ruc_array_copied[row]) * 2
        if (costSum != 0 and pSum != 0):
            # print("Both are not zero at least once")
            taoSumDiff = abs(costSum - pSum)
            difference[taoThreshold] = taoSumDiff
            if (maxSum > taoSumDiff):
                maxSum = taoSumDiff
                maxThreshold = taoThreshold
    return maxThreshold


def return_gradients(non_zero_ruc_array):
    # print(non_zero_ruc_array)
    x = non_zero_ruc_array
    offset = abs(numpy.sum(x) / 2 - numpy.sum(x) * 2 - lower_limit * len(x) / 2 + lower_limit * len(x) * 2)
    dx = numpy.gradient(x)
    # print(dx) #x+d
    f = ((x-lower_limit)*(x-lower_limit)/2 - (x-lower_limit)*(x-lower_limit)*2)# offset - cost_For x|| offset + penalty for x
    gradients = numpy.gradient(f,dx)
    # print(gradients)
    index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: gradients[k], reverse= True)
    # index_of_ruc = [i[0] for i in sorted(enumerate(gradients), key=lambda k: k[1], reverse=True)] #it does same as above line
    print(index_of_sorted_list)
    index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: abs(gradients[k]), reverse=True)
    # index_of_ruc = [i[0] for i in sorted(enumerate(gradients), key=lambda k: k[1], reverse=True)] #it does same as above line
    print(index_of_sorted_list)
    return index_of_sorted_list,gradients


def return_a_list_from_index(index_array, non_zero_ruc_array):
    x=[0 for i in range(len(index_array))]
    for i in range(len(index_array)):
        x[i] =  non_zero_ruc_array[index_array[i]]
    return x


def return_gradients_for_two_function(index_of_ruc_less_than_min_array, inex_of_ruc_greater_than_min_array,non_zero_ruc_array):
    # print(non_zero_ruc_array)
    inside_Points_arry = return_a_list_from_index(index_of_ruc_less_than_min_array,non_zero_ruc_array)
    outside_Points_array2 = return_a_list_from_index(inex_of_ruc_greater_than_min_array,non_zero_ruc_array)
    x1 = numpy.array(inside_Points_arry)
    x2 = numpy.array(outside_Points_array2)
    offset1 = abs(numpy.sum(x1) / 2 - lower_limit * len(x1) / 2)
    offset2 = abs(numpy.sum(x2) * 2 - lower_limit * len(x1) * 2)
    dx1 = numpy.gradient(x1)
    dx2 = numpy.gradient(x2)
    f1 = offset1 + abs((x1 - lower_limit)/2)  # offset - cost_For x|| offset + penalty for x
    f2 = offset2 + abs((x2 - lower_limit)*2)  # offset - cost_For x|| offset + penalty for x
    gradients1 = numpy.gradient(f1, dx1,edge_order=1)
    gradients2 = numpy.gradient(f2, dx2,edge_order=1)
    gradient_merged = [0 for i in range(len(gradients1)+len(gradients2))]
    for i in range(len(gradients1)):
        gradient_merged[index_of_ruc_less_than_min_array[i]] = gradients1[i]
    for i in range(len(gradients2)):
        gradient_merged[inex_of_ruc_greater_than_min_array[i]] = gradients2[i]
    print(gradient_merged)
    index_of_sorted_list_merged = sorted(range(len(gradient_merged)), key=lambda k: gradient_merged[k], reverse=True)
    print(index_of_sorted_list_merged)
    return index_of_sorted_list_merged,gradient_merged

def gradient_min_by_manual(non_zero_ruc_array):
    org_output = function_value_min(non_zero_ruc_array)
    print(non_zero_ruc_array)
    # print("Org Output: ",org_output)
    gradient = []
    for l in range(len(non_zero_ruc_array)):
        arr2 = [0] * len(non_zero_ruc_array)
        for i in range(0, len(non_zero_ruc_array)):
            arr2[i] = non_zero_ruc_array[i]
        # non_zero_ruc_array_copy = non_zero_ruc_array.copy()
        t = arr2 [l]
        arr2[l] = arr2[l] - .0025
        # print("After: ",non_zero_ruc_array_copy[l])
        changed_output = function_value_min(arr2)
        # print("changed output:",changed_output," diff: ",(changed_output - org_output))
        # gradient.append(((changed_output - org_output) / (-1)*float(1e-6)))
        gradient.append(((changed_output - org_output) / (arr2[l] - t)))
    # print(gradient)
    index_of_sorted_list = sorted(range(len(gradient)), key=lambda k: gradient[k], reverse=True)
    # print(index_of_sorted_list)
    # index_of_sorted_list = sorted(range(len(gradient)), key=lambda k: abs(gradient[k]), reverse=True)
    # print(index_of_sorted_list)
    # for i in range(len(gradient)):
    #     if (numpy.isnan(gradient[i])):
    #         gradient[i] = gradient[0]
    # indices = numpy.argsort(gradient)
    # index_of_sorted_list = indices[::-1]
    return index_of_sorted_list,gradient

def gradient_max_by_manual(non_zero_ruc_array):
    org_output = function_value_max(non_zero_ruc_array)
    # print(org_output)
    gradient = []
    for l in range(len(non_zero_ruc_array)):
        non_zero_ruc_array_copy = non_zero_ruc_array.copy()
        # print(non_zero_ruc_array_copy[l])
        non_zero_ruc_array_copy[l] = non_zero_ruc_array_copy[l] + float(1e-4)
        # print(non_zero_ruc_array_copy[l])
        changed_output = function_value_max(non_zero_ruc_array_copy)
        # print(org_output)
        gradient.append(((changed_output - org_output) /float(1e-4)))
    print(gradient)
    # index_of_sorted_list = sorted(range(len(gradient)), key=lambda k: gradient[k], reverse=True)
    # print(index_of_sorted_list)
    index_of_sorted_list = sorted(range(len(gradient)), key=lambda k: abs(gradient[k]), reverse=True)
    print(index_of_sorted_list)
    # for i in range(len(gradient)):
    #     if(numpy.isnan(gradient[i])):
    #         gradient[i] = gradient[0]
    # indices = numpy.argsort(gradient)
    # index_of_sorted_list = indices[::-1]
    return index_of_sorted_list,gradient

def return_gradients_by_cost_function_signed(costFunction, non_zero_ruc_array):
    f = numpy.array(costFunction, dtype = float)
    x = numpy.array(non_zero_ruc_array,dtype=float)
    dx = numpy.gradient(x)
    gradients = numpy.gradient(f,x)
    for i in range(len(gradients)):
        if(numpy.isnan(gradients[i])):
            gradients[i] = gradients[0]
            # print(gradients[i])
    # print(gradients)
    index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: gradients[k], reverse=True)
    print(index_of_sorted_list)
    # index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: abs(gradients[k]), reverse=True)
    # print(index_of_sorted_list)
    # indices = numpy.argsort(gradients)
    # index_of_sorted_list = indices[::-1]
    # print(index_of_sorted_list)
    # print(index_of_sorted_list)
    # for i in range(len(index_of_sorted_list)):
    #     print(gradients[index_of_sorted_list[i]])

    return index_of_sorted_list,gradients



def return_gradients_by_cost_function(costFunction, non_zero_ruc_array):
    f = numpy.array(costFunction, dtype = float)
    x = numpy.array(non_zero_ruc_array,dtype=float)
    dx = numpy.gradient(x)
    gradients = numpy.gradient(f,x)
    for i in range(len(gradients)):
        if(numpy.isnan(gradients[i])):
            gradients[i] = gradients[0]
            # print(gradients[i])
    # print(gradients)
    # index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: gradients[k], reverse=True)
    # print(index_of_sorted_list)
    index_of_sorted_list = sorted(range(len(gradients)), key=lambda k: abs(gradients[k]), reverse=True)
    # print(index_of_sorted_list)
    # indices = numpy.argsort(gradients)
    # index_of_sorted_list = indices[::-1]
    # print(index_of_sorted_list)
    # print(index_of_sorted_list)
    # for i in range(len(index_of_sorted_list)):
    #     print(gradients[index_of_sorted_list[i]])

    return index_of_sorted_list,gradients

def function_value_min(input):
    costSum = 0
    pSum =0
    cost_count = 0
    penalty_count = 0
    for l in range(len(input)):
        if (input[l] > lower_limit):
            # temp = lower_limit - input[l]
            # costSum += abs(pow(temp, 2)) / 2
            costSum += abs(lower_limit - input[l]) / 2
            cost_count = cost_count + 1
        else:
            # temp = lower_limit - input[l]
            # pSum += abs(pow(temp, 2)) * 2
            pSum += abs(lower_limit - input[l]) * 2
            penalty_count = penalty_count + 1
    # return  abs(costSum - pSum  + (cost_count-penalty_count)*(lower_limit))
    return  abs(costSum - pSum)
def function_value_max(input):
    costSum = 0
    pSum =0
    cost_count = 0
    penalty_count = 0
    for l in range(len(input)):
        if (input[l] < upper_limit):
            # temp = lower_limit - input[l]
            # costSum += abs(pow(temp, 2)) / 2
            costSum += abs(upper_limit - input[l]) / 2
            cost_count = cost_count + 1

        else:
            # temp = lower_limit - input[l]
            # pSum += abs(pow(temp, 2)) * 2
            pSum += abs(upper_limit - input[l]) * 2
            penalty_count = penalty_count + 1

    # return  abs(costSum - pSum  + (cost_count-penalty_count)*(lower_limit))
    return  abs(costSum - pSum)

def calculate_min_by_gradient_no_shift(rucFrame, keys):
    difference = dict()
    rucFrame_array = []
    sorted_frame = dict()
    # lower_limit = -0.090
    target_ruc_num = 0
    is_visited = False
    target_t_min = -0.10
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array_1 = rucFrame_array[0].tolist()
    for x in rucFrame_array[1]:
        merged_array_1.append(x)
    # print("length merged: ",len(merged_array_1)," lenght 1: ",len(rucFrame_array[0])," length 2: ",len(rucFrame_array[1]))
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(merged_array_1))]
    for l in range(len(merged_array_1)):
        if (merged_array_1[l] > lower_limit):
            # temp = lower_limit - non_zero_ruc_array[l]
            # costSum += abs(pow(temp, 2)) / 2
            costSum += abs(lower_limit - merged_array_1[l]) / 2
            costFunction[l] = abs(lower_limit - merged_array_1[l]) / 2
            # costFunction[l] = abs(pow(temp, 2)) / 2
            cost_count = cost_count+ 1
        else:
            # temp = lower_limit - non_zero_ruc_array[l]
            # pSum += abs(pow(temp, 2)) * 2
            pSum += abs(lower_limit - merged_array_1[l]) * 2
            costFunction[l] = abs(lower_limit - merged_array_1[l]) * 2
            # costFunction[l] = abs(pow(temp, 2)) * 2
            penalty_count = penalty_count + 1
        cost_norm = costSum
        penalty_norm = pSum
        if (cost_count > 0):
            cost_norm = costSum / cost_count
        if (penalty_count > 0):
            penalty_norm = pSum / penalty_count
        costFunction[l] = abs(cost_norm - penalty_norm)
        # costFunction[l] =  abs(costSum-pSum + (cost_count-penalty_count)*(lower_limit))
        # costFunction[l] =  abs(costSum-pSum)
        # costFunction[l] =  abs(costSum-pSum)/(cost_count+penalty_count)
    # print("org Cost Count: ", cost_count," Penalty Count: ",penalty_count)
    index_of_sorted_list,gradients = return_gradients_by_cost_function(costFunction,merged_array_1) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = return_gradients_for_two_function(non_zero_ruc_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = gradient_min_by_manual(non_zero_ruc_array)  # Gradient by manual process
    # for i in range(len(index_of_sorted_list)):
    #     print(non_zero_ruc_array[i]," ",non_zero_ruc_array[index_of_sorted_list[i]])
    non_zero_ruc_array_copied = merged_array_1.copy()
    min = numpy.min(non_zero_ruc_array_copied)
    print("Min: ",min)
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]<0):
            sign = (-1)*1
        else:sign = 1
        # print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] + 0.03 * sign
        if(non_zero_ruc_array_copied[index_of_sorted_list[i]]>float(0)):continue
        if (min > (non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.0025)):
            continue
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.0025
        print(i,"th gradient: ",gradients[index_of_sorted_list[i]]," residual: ",non_zero_ruc_array_copied[index_of_sorted_list[i]])
        tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # print("Tao min", tmin)
        if ((tmin <= target_t_min) and (is_visited == False)):
            is_visited = True
            target_ruc_num = i
            print("For altering: ", i, "Number of RUCs from ", len(non_zero_ruc_array_copied),
                  " we can reach the targeted max")
            # break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] > lower_limit):
                costSum += abs(lower_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                pSum += abs(lower_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        # print("Cost Count: ", cost_count, " Penalty Count: ", penalty_count)
        # print("cost: ",costSum," penalty: ",pSum)
        # taoSumDiff = abs(pSum - costSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples

    print("taget ruc number: ", target_ruc_num)
    # rucFrame_array.append(non_zero_ruc_array_copied)
    # return non_zero_ruc_array_copied
    # print("taget ruc number: ", target_ruc_num)
    return y,target_ruc_num

def calculate_min_by_RUC_order(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    std_limit_array = []
    loss_array = []
    rucFrame_array = []
    target_t_min = -0.10
    target_ruc_num = 0
    is_visited = False
    day_frame_array = []
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < float(0)]
        # data_frame_array.append(sorted_non_zero_ruc)
        day_values = sorted_non_zero_ruc['day'].values
        day_frame_array.append(day_values)
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array = rucFrame_array[0].tolist()
    # data_frame_array_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), data_frame_array) #merging the dataframes
    day_array = day_frame_array[0].tolist()
    for x in day_frame_array[1]:
        day_array.append(x)
    # print("Days")
    # print(day_array)
    for x in rucFrame_array[1]:
        merged_array.append(x)
    non_zero_ruc_array_copied = merged_array.copy()
    min = numpy.min(non_zero_ruc_array_copied)
    print("Min: ",min)
    for i in range(len(non_zero_ruc_array_copied)):
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        if (min > (non_zero_ruc_array_copied[i] - 0.0025)):
            continue
        non_zero_ruc_array_copied[i] = non_zero_ruc_array_copied[i] - 0.0025
        print(i,"th residual: ",non_zero_ruc_array_copied[i])
        tmin = return_new_tao_min(non_zero_ruc_array_copied)
        std_limit, minSum  = loss_minimization.calculateTmin_ruc(non_zero_ruc_array_copied)
        std_limit_array.append(std_limit*(-1))
        loss_array.append(minSum)
        if((tmin < target_t_min) and (is_visited == False)):
            is_visited = True
            target_ruc_num = i
            print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            # break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] > lower_limit):
                # temp = lower_limit - non_zero_ruc_array_copied[l]
                # costSum += abs(pow(temp, 2)) / 2
                costSum += abs(lower_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                # temp = lower_limit - non_zero_ruc_array_copied[l]
                # pSum += abs(pow(temp, 2)) * 2
                pSum += abs(lower_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    print("taget ruc number: ",target_ruc_num)
    df = pd.DataFrame(std_limit_array,columns=['std_limit'])
    df1 = pd.DataFrame(loss_array,columns=['loss'])
    merged_fr = pd.concat([df['std_limit'],df1['loss']],axis = 1, keys = ['std_limit','loss'])
    return y,target_ruc_num

def calculate_min_by_gradient(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    std_limit_array = []
    loss_array = []
    rucFrame_array = []
    target_t_min = -0.10
    target_ruc_num = 0
    is_visited = False
    day_frame_array = []
    # data_frame_array =  []  # to keep the dataframes here
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < float(0)]
        # data_frame_array.append(sorted_non_zero_ruc)
        day_values = sorted_non_zero_ruc['day'].values
        day_frame_array.append(day_values)
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        rucFrame_array.append(non_zero_ruc_array)
    merged_array = rucFrame_array[0].tolist()
    # data_frame_array_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), data_frame_array) #merging the dataframes
    day_array = day_frame_array[0].tolist()
    for x in day_frame_array[1]:
        day_array.append(x)
    # print("Days")
    # print(day_array)
    for x in rucFrame_array[1]:
        merged_array.append(x)
    # df = pd.DataFrame(merged_array_1, columns=['RUC'])
    # df.to_csv("RUC_min.csv")
    # print("length merged: ",len(merged_array)," lenght 1: ",len(rucFrame_array[0])," length 2: ",len(rucFrame_array[1]))
    # sm.qqplot(numpy.array(merged_array_1),dist=stats.norm,line="s")
    # py.show()
    # sm.qqplot(numpy.array(merged_array_1),dist=stats.uniform,line="s")
    # py.show()
    # sm.qqplot(numpy.array(merged_array_1),dist=stats.lognorm,fit=True,line="s")
    # py.show()
    # sm.qqplot(numpy.array(merged_array_1),dist=stats.expon,line="s")
    # py.show()
    costSum = 0
    pSum = 0
    cost_count = 0
    penalty_count = 0
    costFunction =[0 for i in range(len(merged_array))]
    for l in range(len(merged_array)):
        if (merged_array[l] > lower_limit):
            # temp = lower_limit - non_zero_ruc_array[l]
            # costSum += abs(pow(temp, 2)) / 2
            costSum += abs(lower_limit - merged_array[l]) / 2
            # costFunction[l] = abs(lower_limit - merged_array[l]) / 2
            # costFunction[l] = abs(pow(temp, 2)) / 2
            cost_count = cost_count+ 1
        else:
            # temp = lower_limit - non_zero_ruc_array[l]
            # pSum += abs(pow(temp, 2)) * 2
            pSum += abs(lower_limit - merged_array[l]) * 2
            # costFunction[l] = abs(lower_limit - merged_array[l]) * 2
            # costFunction[l] = abs(pow(temp, 2)) * 2
            penalty_count = penalty_count + 1
        cost_norm = costSum
        penalty_norm = pSum
        if (cost_count > 0):
            cost_norm = costSum / cost_count
        if (penalty_count > 0):
            penalty_norm = pSum / penalty_count
        costFunction[l] = abs(cost_norm - penalty_norm)
        # costFunction[l] =  abs(costSum-pSum)
        # costFunction[l] =  abs(costSum-pSum)/(cost_count+penalty_count)
    # print("org Cost Count: ", cost_count," Penalty Count: ",penalty_count)
    # print(costFunction[len(costFunction)-1])
    index_of_sorted_list,gradients = return_gradients_by_cost_function_signed(costFunction,merged_array) #gradient by cost array
    # index_of_sorted_list,gradients = return_gradients(merged_array)  # Gradient for a functiom
    # index_of_sorted_list,gradients = return_gradients_for_two_function(non_zero_ruc_array)  # Gradient for a functiom
    non_zero_ruc_array_copied = merged_array.copy()
    # index_of_sorted_list,gradients = gradient_min_by_manual(non_zero_ruc_array_copied)  # Gradient by manual process
    min = numpy.min(non_zero_ruc_array_copied)
    print("Maximum is ",min)
    for i in range(len(index_of_sorted_list)):
        if(gradients[index_of_sorted_list[i]]<0):
            sign = (-1)*1
        else:sign = 1
        # print(sign)
        costSum = 0
        pSum = 0
        cost_count = 0
        penalty_count = 0
        # non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  -\
        #                                                      (abs(lower_limit -non_zero_ruc_array_copied[index_of_sorted_list[i]] ) +0.0875)
        if(min>(non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.0025)):
            continue
        non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]] - 0.0025
        print(i,"th gradient: ",gradients[index_of_sorted_list[i]]," residual: ",non_zero_ruc_array_copied[index_of_sorted_list[i]])
        tmin = return_new_tao_min(non_zero_ruc_array_copied)
        # print("Tao min", tmin)
        std_limit, minSum  = loss_minimization.calculateTmin_ruc(non_zero_ruc_array_copied)
        std_limit_array.append(std_limit*(-1))
        loss_array.append(minSum)
        if((tmin < target_t_min) and (is_visited == False)):
            is_visited = True
            target_ruc_num = i
            print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            # break
        for l in range(len(non_zero_ruc_array_copied)):
            if (non_zero_ruc_array_copied[l] > lower_limit):
                # temp = lower_limit - non_zero_ruc_array_copied[l]
                # costSum += abs(pow(temp, 2)) / 2
                costSum += abs(lower_limit - non_zero_ruc_array_copied[l]) / 2
                cost_count = cost_count + 1
            else:
                # temp = lower_limit - non_zero_ruc_array_copied[l]
                # pSum += abs(pow(temp, 2)) * 2
                pSum += abs(lower_limit - non_zero_ruc_array_copied[l]) * 2
                penalty_count = penalty_count + 1
        # print("Cost Count: ", cost_count, " Penalty Count: ", penalty_count)
        # print("cost: ",costSum," penalty: ",pSum)
        # taoSumDiff = abs(pSum - costSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
        # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
        #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
        difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        # print(difference[i])
    # calculateTmin_ruc(non_zero_ruc_array_copied)
    lists = (difference.items())  # sorted by key, return a list of tuples
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    # print("Loss When points are taken according to the order of gradient")
    print("taget ruc number: ",target_ruc_num)
    df = pd.DataFrame(std_limit_array,columns=['std_limit'])
    df1 = pd.DataFrame(loss_array,columns=['loss'])
    merged_fr = pd.concat([df['std_limit'],df1['loss']],axis = 1, keys = ['std_limit','loss'])
    return y,target_ruc_num

def calculate_optimal_by_gradient(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    lower_limit = -0.090
    target_t_min = -0.2
    previous_diff = 0
    for key in keys:
        difference = dict()
        # print(rucFrame['ruc'+key].head(5))
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        # print(sorted_frame[key].head(5))
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc["ruc" + key].values
        # ruc_less_than_min = sorted_non_zero_ruc[sorted_non_zero_ruc['ruc' + key] < lower_limit]
        index_ruc_less_than_min = [i for i,v in enumerate(non_zero_ruc_array) if v < lower_limit]
        # ruc_greater_than_min =   sorted_non_zero_ruc[sorted_non_zero_ruc['ruc' + key] >= lower_limit]
        index_ruc_greater_than_min =   [i for i,v in enumerate(non_zero_ruc_array) if v > lower_limit]
        # ruc_less_than_min_array = ruc_less_than_min["ruc" + key].values
        # ruc_greater_than_min_array = ruc_greater_than_min["ruc" + key].values

        # index_of_sorted_list = return_gradients(non_zero_ruc_array)
        index_of_sorted_list,gradients = return_gradients_for_two_function(index_ruc_less_than_min,index_ruc_greater_than_min,non_zero_ruc_array)
        non_zero_ruc_array_copied = non_zero_ruc_array.copy()
        for i in range(len(index_of_sorted_list)):
            if(gradients[index_of_sorted_list[i]]<0):
                sign = -1
            else:sign = 1
            costSum = 0
            pSum = 0
            # print(sign)
            non_zero_ruc_array_copied[index_of_sorted_list[i]] = non_zero_ruc_array_copied[index_of_sorted_list[i]]  + 0.0875*sign
            # print(non_zero_ruc_array_copied)
            # tmin = return_new_tao_min(non_zero_ruc_array_copied)
            # if(tmin<target_t_min):
            #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            #     break
            for l in range(len(non_zero_ruc_array_copied)):
                if (non_zero_ruc_array_copied[l] > lower_limit):
                    costSum += abs(lower_limit - non_zero_ruc_array_copied[l]) / 2
                else:
                    pSum += abs(lower_limit - non_zero_ruc_array_copied[l]) * 2
            # print("cost: ",costSum," penalty: ",pSum)
            taoSumDiff = abs(pSum - costSum)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # taoSumDiff = abs(pSum - costSum - (i + 1) * 0.177 * 1.5)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # print("cost: ",costSum," penalty: ",pSum," Cost-penalty: ",taoSumDiff,
            #       " non_zero_ruc_array_copied[i][1]:",non_zero_ruc_array_copied[i][1])
            difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
        lists = (difference.items())  # sorted by key, return a list of tuples
        x, y = zip(*lists)  # unpack a list of pairs into two tuples
        return y

def calculate_optimal_min_threshold(rucFrame, keys):
    difference = dict()
    sorted_frame = dict()
    # lower_limit = -0.090
    target_t_min = -0.2
    previous_diff = 0
    for key in keys:
        difference = dict()
        sorted_frame[key] = rucFrame.sort_values(by=["ruc" + key])
        sorted_non_zero_ruc = sorted_frame[key][sorted_frame[key]['ruc' + key] < 0]
        non_zero_ruc_array = sorted_non_zero_ruc[["ruc" + key, "day"]].values
        print("Lenth of Non zero ruc foe key  ", key, " is: ", len(non_zero_ruc_array))
        # print(non_zero_ruc_array)
        # print(sorted_non_zero_ruc)
        non_zero_ruc_array_copied = non_zero_ruc_array
        for i in range(len(non_zero_ruc_array_copied)):
            costSum = 0
            pSum = 0
            cost_count = 0
            penalty_count = 0
            non_zero_ruc_array_copied[i][0] = non_zero_ruc_array_copied[i][0] - 0.0875
            # tmin = return_new_tao_min(non_zero_ruc_array_copied)
            # if(tmin<target_t_min):
            #     print("For altering: ", i ,"Number of RUCs from ",len(non_zero_ruc_array_copied)," we can reach the targeted max")
            #     break
            for l in range(len(non_zero_ruc_array_copied)):
                if (non_zero_ruc_array_copied[l][0] > lower_limit):
                    # temp = lower_limit - non_zero_ruc_array_copied[l][0]
                    # costSum += abs(pow(temp, 2))  / 2
                    costSum += abs(lower_limit - non_zero_ruc_array_copied[l][0]) / 2
                    cost_count = cost_count + 1
                else:
                    # temp = lower_limit - non_zero_ruc_array_copied[l][0]
                    # pSum += abs(pow(temp, 2)) * 2
                    pSum += abs(lower_limit - non_zero_ruc_array_copied[l][0]) * 2
                    penalty_count = penalty_count +1
            # print("cost: ", costSum, " penalty: ", pSum)
            # taoSumDiff = abs(costSum - pSum + (cost_count-penalty_count)*(lower_limit))  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            taoSumDiff = abs(costSum - pSum )  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # taoSumDiff = abs(costSum - pSum + (i + 1) * 0.177)  # + i*(i+1)*0.177*.5 #+(i+1)*0.177
            # print("Cost-penalty: ",taoSumDiff)
            difference[i] = taoSumDiff  # - previous_diff/((i+1)*0.177)
            # previous_diff = taoSumDiff
        lists = (difference.items())  # sorted by key, return a list of tuples
        x, y = zip(*lists)  # unpack a list of pairs into two tuples
        return y

def calculateTmax_ruc(rucFrame, keys):
    maxSum = sys.float_info.max
    maxRuc2014 = rucFrame['ruc2014'].max()
    maxRuc2015 = rucFrame['ruc2015'].max()
    print(maxRuc2014, maxRuc2015)
    max_candidate = max(maxRuc2015, maxRuc2014)
    global maxThreshold
    print("maxThreshold: ", max_candidate)
    difference = dict()
    for taoThreshold in numpy.arange(0.0000, max_candidate, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "ruc" + key) > 0):  # previously It was greater equal
                    # tdsc says to iterate only non zero items
                    if (getattr(row, "ruc" + key) < taoThreshold):
                        # temp = abs(taoThreshold - getattr(row, "ruc" + key))/2
                        # costSum += pow(temp,2)
                        costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                        costCount = costCount + 1
                    else:
                        # temp = abs(taoThreshold - getattr(row, "ruc" + key))*2
                        # pSum += pow(temp,2)
                        pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                        penaltyCount = penaltyCount + 1
        if (pSum != 0 and costSum != 0):
            # taoSumDiff = abs(costSum - pSum+ (costCount-penaltyCount)*taoThreshold)
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            # print("Original cost: ", costSum, " penalty: ", pSum," taothreshold: ",taoThreshold)
            # print("Cost Sum: ", costSum," psum: ",pSum," maxSum: ",maxSum," taoDifference: ",taoSumDiff)
            if (maxSum > taoSumDiff):
                # print("this is executing")
                maxSum = taoSumDiff
                # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
                maxThreshold = taoThreshold
    lists = sorted(difference.items())  # sorted by key, return a list of tuples
    # w = csv.writer(open("loss_vs_tmax_l1.csv", "w"))
    # for key, val in difference.items():
    #     w.writerow([key, val])
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    fig = plt.figure()
    plt.plot(x, y)
    # fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Cost - Penalty')
    # fig.savefig('test.jpg')
    plt.show()
    return maxThreshold


def calculateTmin_ruc(rucFrame, keys):
    minSum = sys.float_info.max
    minRuc2014 = rucFrame['ruc2014'].min()
    minRuc2015 = rucFrame['ruc2015'].min()
    print(minRuc2014, minRuc2015)
    min_candidate = min(minRuc2015, minRuc2014)
    global minThreshold1
    difference  = dict()
    print("min: ", min_candidate)
    for taoThreshold in numpy.arange(min_candidate, 0, 0.0025):
        costSum = 0
        pSum = 0
        costCount = 0
        penaltyCount = 0
        for row in rucFrame.itertuples():
            # print(row)
            for key in keys:
                if (getattr(row, "ruc" + key) < 0):
                    if (getattr(row, "ruc" + key) > ( taoThreshold)):
                        # temp = abs(taoThreshold*(-1) - getattr(row, "ruc" + key))/2
                        # costSum += pow(temp, 2)
                        costSum += abs(taoThreshold - getattr(row, "ruc" + key)) / 2
                        costCount = costCount + 1
                    else:
                        # temp = abs(taoThreshold*(-1) - getattr(row, "ruc" + key))*2
                        # pSum += pow(temp, 2)
                        pSum += abs(taoThreshold - getattr(row, "ruc" + key)) * 2
                        penaltyCount = penaltyCount + 1
        if (costSum != 0 and pSum != 0):
            # print("Both are not zero at least once")
            # taoSumDiff = abs(costSum - pSum + (costCount-penaltyCount)*taoThreshold)
            taoSumDiff = abs(costSum - pSum)
            # taoSumDiff = pow(abs(costSum - pSum),2)
            difference[taoThreshold] = taoSumDiff
            if (minSum > taoSumDiff):
                minSum = taoSumDiff
                minThreshold = taoThreshold
                # print("Cost Count: ",costCount," penalty count: ",penaltyCount)
    lists = (difference.items())  # sorted by key, return a list of tuples
    # w = csv.writer(open("free_of_attack_loss_old.csv", "w"))
    # for key, val in difference.items():
    #     w.writerow([key, val])
    x, y = zip(*lists)  # unpack a list of pairs into two tuples
    #
    fig = plt.figure()
    plt.plot(x, y)
    # fig.suptitle('Cost - Penalty Plotting', fontsize=20)
    plt.xlabel('Standard Limit')
    plt.ylabel('Loss')
    # fig.savefig('test.jpg')
    plt.show()
    return minThreshold


def calculateTmax_mruc(rucFrame, keys):
    # maxSum = (-1)*sys.float_info.max  #commented puposefully as the algorithm says if
    # the difference between cost and penalty is min
    maxSum = sys.float_info.max
    # print("maxSum: ",maxSum)
    maxRuc2014 = rucFrame['mruc2014'].max()
    maxRuc2015 = rucFrame['mruc2015'].max()
    maxThreshold = max(maxRuc2015, maxRuc2014)
    # print("maxThreshold: ",maxThreshold)

    for taoThreshold in numpy.arange(0.0000, .100, 0.0025):
        costSum = 0
        pSum = 0
        for row in rucFrame.itertuples():
            for key in keys:
                if (getattr(row, "mruc" + key) >= 0):
                    if (getattr(row, "mruc" + key) < taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "mruc" + key)) / 2
                        # costSum +=(taoThreshold -  getattr(row, "mruc"+key))/2
                    else:
                        pSum += abs(taoThreshold - getattr(row, "mruc" + key)) * 2
                        # pSum += (getattr(row, "mruc"+key)-taoThreshold)*2
        if (pSum != 0 and costSum != 0):
            taoSumDiff = abs(costSum - pSum)
            # print("Cost Sum: ", costSum," psum: ",pSum)
            if (maxSum > taoSumDiff):
                # print("this is executing")
                maxSum = taoSumDiff
                maxThreshold = taoThreshold

    return maxThreshold


def calculateTmin_mruc(rucFrame, keys):
    minSum = sys.float_info.max
    minRuc2014 = rucFrame['mruc2014'].min()
    minRuc2015 = rucFrame['mruc2015'].min()
    minThreshold = min(minRuc2015, minRuc2014)
    # print("minThreshold: ",minThreshold)
    for taoThreshold in numpy.arange(-.100, -0.0025, 0.0025):
        costSum = 0
        pSum = 0
        for row in rucFrame.itertuples():
            # print(row)
            for key in keys:
                if (getattr(row, "mruc" + key) < 0):
                    if (getattr(row, "mruc" + key) > taoThreshold):
                        costSum += abs(taoThreshold - getattr(row, "mruc" + key)) / 2
                        # costSum += (taoThreshold - getattr(row, "mruc" + key)) / 2

                    else:
                        pSum += abs(taoThreshold - getattr(row, "mruc" + key)) * 2
                        # pSum += (getattr(row, "mruc" + key)-taoThreshold) * 2
        if (costSum != 0 and pSum != 0):
            # print("Both are not zero at least once")
            taoSumDiff = abs(costSum - pSum)
            if (minSum > taoSumDiff):
                minSum = taoSumDiff
                minThreshold = taoThreshold
    return minThreshold

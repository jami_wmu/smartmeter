from src.utility.common_functions import *
import os as os
import numpy as np
file_relative_path = "../../resource/2016.csv"
file_path = os.path.abspath(file_relative_path)  # relative path of the data file
file_data = read_data(file_path, "2016")  # call the reader function of datareader object
# file_data_filtered = file_data[(file_data['use'] <= 6000) & (file_data['use'] > 50)]
file_data_filtered = file_data[(file_data['use'] <= 6000)]
randomlist_hour =[10,11,12,13,14,15,16,17,18] #random.sample(range(0, 23), 6)
print(randomlist_hour)
randomlist_day =[1,2,3,4,5,6] #random.sample(range(0, 6), 2)
print(randomlist_day)
considered_window = file_data_filtered[(file_data_filtered['localminute'] >= '2016-03-01') &  (file_data_filtered['localminute'] <=  '2016-03-06')]
allMeters = set(considered_window.dataid.unique())
# print("Number of meteres", len(allMeters))
numberofitems = (len(allMeters) * .30)
# print(numberofitems)
if(int(numberofitems)%2 !=0):
    numberofitems = numberofitems - 1
compromised_meters = set(sample(allMeters, int(numberofitems)))
# print(len(compromised_meters))
# print(compromised_meters)
count = 0
additive_meters = []
deductive_meters = []
for i in compromised_meters:
    if(count < (len(compromised_meters)/2)):
        additive_meters.append(i)
        count = count + 1
    else:
        deductive_meters.append(i)
        count = count + 1
# print(additive_meters)
# print(deductive_meters)
del_avg = 400
mean_perturb = []
daywisegroupData = considered_window.groupby(pd.Grouper(key='localminute', freq='1D'))  # group the montly data by day
daywise_updated_data = {}
for key_D, dailyData in daywisegroupData:
    if ((len(daywisegroupData) > 0) & (int(key_D.day) in randomlist_day)):
        hourwiseGroupData = dailyData.groupby(pd.Grouper(key='localminute', freq='1H'))  # group the daily data into hourlwise
        hourwise_updated_data = {}
        for key_H, hourlyData in hourwiseGroupData:  # iterate each hourly data of the day
            if ((len(hourlyData) > 0) & (int(key_H.hour) in randomlist_hour)):  # check if the hour has data count greater than zero
                # print("********** hourly Data ***********")
                # print(hourlyData)
                for_additve  = hourlyData[hourlyData['dataid'].isin(additive_meters)]
                for_deductive = hourlyData[hourlyData['dataid'].isin(deductive_meters)]
                not_touched =  hourlyData[~ hourlyData['dataid'].isin(compromised_meters)] # this is for unperturbed set
                max_usage = for_additve['use'].max()  # max power
                min_usage = for_additve['use'].min()  # min power
                mean_usage = for_additve['use'].mean()
                # print("Items greater than mean", for_additve[for_additve['use'] > mean_usage].count(),
                #       "Less than mean:", for_additve[for_additve['use'] < mean_usage].count())
                # print("max usage: ",max_usage," min_usage: ",min_usage," mean usage: ",(mean_usage+min_usage)/2)
                iteration_cout = 0
                if(len(for_deductive)>=len(for_additve)):
                    iteration_cout  = len(for_additve)
                else:
                    iteration_cout = len(for_deductive)
                #generate iteration count number of random number with avg = delavg
                # print("length of the iteration count",iteration_cout)
                perturbation_list = []
                # print("****************** Deductive ***************")
                # print(for_deductive)
                i = 0
                delta = 8
                # start = 200
                for_deductive_sorted = for_deductive.sort_values(by=["use"])
                for index, row in for_deductive_sorted.iterrows():
                    # print(row)
                    # step = start+i*delta
                    if (i == iteration_cout): break
                    if for_deductive_sorted.loc[index, 'use'] > 0:
                        # update(row,"use", (getattr(row, "use") - del_avg))
                        temp = for_deductive_sorted.loc[index, 'use']
                        for_deductive_sorted.loc[index, 'use'] = for_deductive_sorted.loc[index, 'use'] - (i+1)*delta
                        # for_deductive_sorted.loc[index, 'use'] = for_deductive_sorted.loc[index, 'use'] - step
                        if(for_deductive_sorted.loc[index, 'use']<0):
                            # print("Negative ")
                            # print(temp)
                            for_deductive_sorted.loc[index, 'use'] = temp - (i + 1) * delta/8
                            # for_deductive_sorted.loc[index, 'use'] = temp - step/8
                            if (for_deductive_sorted.loc[index, 'use'] < 0):
                                # for_deductive_sorted.loc[index, 'use'] = temp - step / (8*8)
                                for_deductive_sorted.loc[index, 'use'] = temp - (i + 1) * delta / (8*8)
                                # print("Still Negative")
                                # perturbation_list.append(step/(8*8))
                                perturbation_list.append((i + 1) * delta / (8*8))
                            else:
                                perturbation_list.append((i + 1) * delta/8)
                                # perturbation_list.append(step/8)
                        else:
                            # perturbation_list.append(step)
                            perturbation_list.append((i + 1) * delta)
                    i = i + 1
                # print(for_deductive)
                mean_perturb.append(np.mean(perturbation_list))
                # print(np.mean(perturbation_list))
                # print((perturbation_list))
                i = 0
                #to bring in the data order aware attack
                # random_list = random.sample(range(1, 800), iteration_cout) # generating a list of values
                #random is not a smart way
                # less randomness is better
                # try to make it deterministic considering max and min in the records
                # RL_mean = np.mean(random_list)
                # print("current mean is: ",RL_mean)
                # random_list_sorted = np.sort(random_list)
                # print(random_list_sorted)
                #sort
                for_additve_sorted = for_additve.sort_values(by=['use'], ascending=False)
                # print(for_additve_sorted)
                perturbation_list_sorted = np.sort(perturbation_list)
                for index, row in for_additve_sorted.iterrows():
                    # print(row)
                    if(i == iteration_cout):break
                    if  for_additve_sorted.loc[index,'use']  > 0:
                        # for_additve.loc[index,'use']  = for_additve.loc[index,'use'] + del_avg
                        for_additve_sorted.loc[index,'use']  = for_additve_sorted.loc[index,'use'] + perturbation_list_sorted[i]
                        # print("executing")
                    i = i +1
                # print(for_additve)
                hourwise_updated_data[key_H] = pd.concat([for_additve_sorted,for_deductive_sorted,not_touched])
            else:
                hourwise_updated_data[key_H] = hourlyData
        merged_list = []
        for k in hourwise_updated_data:
            merged_list.append(hourwise_updated_data[k])
        # print(merged_list)
        out = pd.concat(merged_list)
        daywise_updated_data[key_D] = out
    else:
        daywise_updated_data[key_D] = dailyData
#region test updates
print("Mean Perturb: ",np.mean(mean_perturb))
for key,group in daywisegroupData: #
    # if((key.day in randomlist_day) & (key.hour in randomlist_day)):
    if((key.day in randomlist_day)):
        # print("******Unperturbed set******")
        # print(group)
        # print(group[group['dataid']==9121])
        break
for key in daywise_updated_data: #
    # if((key.day in randomlist_day) & (key.hour in randomlist_day)):
    if((key.day in randomlist_day)):
        # print("******Perturbed set******")
        # print(daywise_updated_data[key])
        # print(daywise_updated_data[key][daywise_updated_data[key]['dataid']==9121])
        break
#endregion
merged_list_org = []
for k,g in daywisegroupData:
    # merged = pd.merge(categories, g[1], how='outer', on='cat')
    merged_list_org.append(g)
# print(merged_list)
out_org = pd.concat(merged_list_org)
# print(out_org)
out_org.use =  stats.boxcox(out_org.use, lmbda=LAMBDA, alpha=None)
# print(out_org)
# print(len(out_org))
merged_list_updated = []
for k in daywise_updated_data:
    # merged = pd.merge(categories, g[1], how='outer', on='cat')
    merged_list_updated.append(daywise_updated_data[k])
# print(merged_list)
out_updated = pd.concat(merged_list_updated)
mask_neg = (out_updated['use'] <= 0)
df = out_updated.loc[mask_neg]
# replacing all values less than 50 by a random number 10-50
out_updated.loc[mask_neg, 'use'] = df['use'].apply(lambda x: randint(5, 10))
out_updated.use =  stats.boxcox(out_updated.use, lmbda=LAMBDA, alpha=None)
# print(len(out_updated))
#region ratio test original
dateRatioArray = {}
day = 60
day_grouped = out_org.groupby(pd.Grouper(key='localminute', freq='1D'))
for key, dailyData in day_grouped:
    if len(dailyData) > 0:
        hourwiseGroupData = dailyData.groupby(pd.Grouper(key='localminute', freq='1H'))
        hourwiseSumAM = 0
        hourwiseSumHM = 0
        for key_H, hourlyData in hourwiseGroupData:  # iterate each hourly data of the day
            if len(hourlyData) > 0:  # check if the hour has data count greater than zero
                am_sum = 0
                hm_sum = 0
                for row in hourlyData.itertuples():
                    if (getattr(row, "use") > 0):
                        am_sum += getattr(row, "use")  # calculate arithmatic sum
                        hm_sum += 1 / getattr(row, "use")  # calculate harmonic sum
                N = len(hourlyData.dataid.unique())
                hourlyAttackHM = N / hm_sum  # harmonic mean hourly
                hourlyAttackAM = am_sum / N  # arithmatic mean hourly
                hourwiseSumAM += hourlyAttackAM
                hourwiseSumHM += hourlyAttackHM
        daywiseRatioMean = hourwiseSumHM / hourwiseSumAM  # use the ratio means of 24 hours of a day to calculate day wise ratio mean
        # temp = {'day': key.day, 'ratio': daywiseRatioMean}
        dateRatioArray[day] = daywiseRatioMean
        day = day + 1
#endregion ratio test

#region ratio test perturbed
dateRatioArray_updated = {}
day = 60
day_grouped_updated = out_updated.groupby(pd.Grouper(key='localminute', freq='1D'))
for key,dailyData in day_grouped_updated:
    # dailyData = daywise_updated_data[key]
    if len(dailyData) > 0:
        hourwiseGroupData = dailyData.groupby(pd.Grouper(key='localminute', freq='1H'))
        hourwiseSumAM = 0
        hourwiseSumHM = 0
        for key, hourlyData in hourwiseGroupData:  # iterate each hourly data of the day
            if len(hourlyData) > 0:  # check if the hour has data count greater than zero
                am_sum = 0
                hm_sum = 0
                for row in hourlyData.itertuples():
                    if (getattr(row, "use") > 0):
                        am_sum += getattr(row, "use")  # calculate arithmatic sum
                        hm_sum += 1 / getattr(row, "use")  # calculate harmonic sum
                N = len(hourlyData.dataid.unique())
                hourlyAttackHM = N / hm_sum  # harmonic mean hourly
                hourlyAttackAM = am_sum / N  # arithmatic mean hourly
                hourwiseSumAM += hourlyAttackAM
                hourwiseSumHM += hourlyAttackHM
        daywiseRatioMean = hourwiseSumHM / hourwiseSumAM  # use the ratio means of 24 hours of a day to calculate day wise ratio means
        # temp = {'day': key.day, 'ratio': daywiseRatioMean}
        dateRatioArray_updated[day] = daywiseRatioMean
        day = day + 1
#endregion
# print("Orginal Ratio")
# print(dateRatioArray)
# print("Updated Ratio")
# print(dateRatioArray_updated)
df1 = pd.DataFrame(list(dateRatioArray.items()),columns = ['day','ratio'])
df2 = pd.DataFrame(list(dateRatioArray_updated.items()),columns = ['day','ratio'])
#region read the safe margin
safe_margin = pd.read_csv("safe_margin_new_mad2.0.csv")
# print(safe_margin_kappa_varied)
safe_margin_within_day_range = safe_margin[(safe_margin['day']>59) & (safe_margin['day']<=65)]
# print(safe_margin_within_day_range)
#endregion
# merge_with_org = pd.merge(df1,safe_margin_within_day_range,on='day')
# merge_with_update = pd.merge(df2,safe_margin_within_day_range,on='day')
# print(merge_with_org)
# print(merge_with_update)
def testing_ratio():
    file_data = {}
    file_data_filtered = {}
    file_relative_path = TESTING_DATA.get('2016')  # get the file path by year as key
    file_path = os.path.abspath(file_relative_path)  # relative path of the data file
    file_data['2016'] = read_data(file_path, '2016')  # call the reader function of datareader object
    file_data_filtered['2016'] = file_data['2016'][file_data['2016']['use'] <= 6000]
    # print("file_data",file_data_filtered)
    file_data_transformed = box_cox_transformation(file_data_filtered)
    hm_am_ratio_frame = calculateAMAndHM(file_data_transformed)
    df_merged = reduce(lambda left, right: pd.merge(left, right, on=['day']), hm_am_ratio_frame)
    return df_merged

testing_ratio_frame = testing_ratio()
testing_ratio_updated = testing_ratio_frame.copy()
merged_with_safe_margin = pd.merge(testing_ratio_frame, safe_margin, on=['day'])
nabla_frame = calculate_nabla(merged_with_safe_margin, TESTING_DATA.keys())
residual_frame = calculate_ruc(nabla_frame, TESTING_DATA.keys())
ruc_with_safe_margin = pd.merge(merged_with_safe_margin, residual_frame, on=['day'])
print(ruc_with_safe_margin[(ruc_with_safe_margin['day']>59) & (ruc_with_safe_margin['day']<= 65)])
testing_ratio_updated.set_index('day', inplace=True)
df2.set_index('day', inplace=True)
df_temp = pd.DataFrame()
df_temp['ratio2016'] = testing_ratio_updated['ratio2016']
testing_ratio_dict = df_temp.to_dict(orient="index")
merge_with_update_dict = df2.to_dict(orient="index")
# print(testing_ratio_dict)
# print(merge_with_update_dict)
for key in testing_ratio_dict.keys():
    testing_ratio_dict[key] = testing_ratio_dict[key]['ratio2016']
# print(testing_ratio_dict)
for key in merge_with_update_dict.keys():
    testing_ratio_dict[key] = merge_with_update_dict[key]['ratio']
# print(testing_ratio_dict)

testing_ratio_dict_frame = pd.DataFrame(list(testing_ratio_dict.items()),columns = ['day','ratio2016'])
merged_with_safe_margin_updated = pd.merge(testing_ratio_dict_frame, safe_margin, on=['day'])
nabla_frame_updated = calculate_nabla(merged_with_safe_margin_updated, TESTING_DATA.keys())
residual_frame_updated = calculate_ruc(nabla_frame_updated, TESTING_DATA.keys())
ruc_with_safe_margin_updated = pd.merge(merged_with_safe_margin_updated, residual_frame_updated, on=['day'])
print(ruc_with_safe_margin_updated[(ruc_with_safe_margin_updated['day']>59) & (ruc_with_safe_margin_updated['day']<= 65)])

import matplotlib.ticker

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 30


plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)
locator = matplotlib.ticker.MultipleLocator(2)
plt.gca().xaxis.set_major_locator(locator)
day_array = ruc_with_safe_margin[(ruc_with_safe_margin['day']>50)&(ruc_with_safe_margin['day']<=70)]['day'].tolist()
# print(day_array)
plt.plot(day_array,ruc_with_safe_margin[(ruc_with_safe_margin['day']>50)&(ruc_with_safe_margin['day']<=70)]['ratio2016'],
         marker="<", markevery=5,         label = "Ratio",color='b')
plt.plot(day_array, ruc_with_safe_margin_updated[(ruc_with_safe_margin['day']>50)&(ruc_with_safe_margin['day']<=70)]['ratio2016'],
         marker=">", markevery=5,         label = "Ratio Perturbed",color='m')
plt.plot(day_array, ruc_with_safe_margin[(ruc_with_safe_margin['day']>50)&(ruc_with_safe_margin['day']<=70)]['margin_low'],
         marker = "X",markevery=5,label = "Safe Margin Low",color='g')
plt.plot(day_array, ruc_with_safe_margin[(ruc_with_safe_margin['day']>50)&(ruc_with_safe_margin['day']<=70)]['margin_high'],
         marker = "D",markevery=5,label = "Safe Margin High",color='g')
plt.axvline(x=60, color='r', linestyle='--')
plt.axvline(x=65, color='r', linestyle=':')
plt.text(59,.93,'Attack Start',rotation=0)
plt.text(64,.93,'Attack End',rotation=0)

plt.xlabel('Day')
plt.ylabel('Ratio')
plt.legend()
plt.show()

def testing_attacked_threshold(residual_frame):
    first_detected = 0
    tier1_anomaly = 0
    tier2_for_att = 0
    false_alarm_day = []
    for row in residual_frame.itertuples():
        if not(getattr(row,"ratio2016") <= getattr(row,'margin_high') and
               getattr(row,'ratio2016') >= getattr(row,'margin_low')):
            tier1_anomaly = tier1_anomaly + 1
            if float(getattr(row,"ruc2016"))!= float(0):
                if (float(getattr(row,"ruc2016")) > float(0.065) or float(getattr(row,"ruc2016")) < float(-0.065)):
                    if (getattr(row,"day") > int(59) and getattr(row,"day")<int(66)):
                        if int(tier2_for_att) == int(0) :
                            first_detected = getattr(row, "day")
                        tier2_for_att = tier2_for_att +1
                    else:
                        false_alarm_day.append(getattr(row,'day'))
    return  first_detected,tier2_for_att,false_alarm_day

# first_detected,tier2_for_att,false_alarm_day =
# first_detected_u,tier2_for_att_u,false_alarm_day_u =